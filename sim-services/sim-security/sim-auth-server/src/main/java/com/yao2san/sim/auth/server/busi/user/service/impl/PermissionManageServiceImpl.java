package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.common.auth.util.UserUtil;
import com.yao2san.sim.auth.server.busi.user.service.PermissionManageService;
import com.yao2san.sim.framework.cache.utils.CacheUtil;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.PurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simsecurity.request.RolePermissionReq;
import com.yao2san.simservice.entity.simsecurity.Purview;
import com.yao2san.simservice.mapper.simsecurity.PurviewMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.yao2san.sim.framework.common.auth.Constants.CACHED_ALL_PERMISSION_KEY;
import static com.yao2san.sim.framework.common.auth.Constants.CACHED_INTERFACE_PERMISSION_KEY;

@Log4j2
@Service
public class PermissionManageServiceImpl extends ServiceBaseImpl<PurviewMapper, Purview> implements PermissionManageService {
    @Autowired
    private PurviewMapper purviewMapper;

    @Override
    public ResponsePageData<Permission> qryPermissionList(Permission permission) {
        Page<Permission> page = convertPage(permission);
        List<Permission> list = purviewMapper.qryPermissionList(page, permission);
        //PageInfo<Permission> list = this.qryList("permissionManage.qryPermissionList", permission);
        return buildPageResult(page, list);
    }

    @Override
    public ResponseData<List<Map<String, Object>>> qryPermissionTree(Permission permission) {
        Page<Permission> page = convertPage(permission);
        page.setSize(Integer.MAX_VALUE);
        List<Permission> list = purviewMapper.qryPermissionList(page, permission);
        //List<Permission> list = this.sqlSession.selectList("permissionManage.qryPermissionList", permission);
        List<Map<String, Object>> tree = this.buildTree(list);
        return ResponseData.success(tree);
    }

    @Override
    public ResponseData<Permission> qryPermissionDetail(Long purviewId) {
        Permission permission = purviewMapper.qryPermissionDetail(purviewId);
        //Permission data = this.sqlSession.selectOne("permissionManage.qryPermissionDetail", purviewId);
        return ResponseData.success(permission);
    }

    @Override
    public ResponseData<?> addOrUpdatePermission(PurviewAddOrUpdateReq req) {
        req.setCreateUser(UserUtil.getCurrUserId());
        if (req.getId() == null) {
            if (this.checkCodeExist(req.getPurviewCode())) {
                return ResponseData.businessError("权限编码" + req.getPurviewCode() + "已存在");
            }
            req.setStatus("1000");
            this.save(req);
            //this.sqlSession.insert("permissionManage.addPermission", permission);

            req.setPath(this.getPath(req.getId()));
            //this.sqlSession.update("permissionManage.updatePermission", permission);
            this.updateById(req);
        } else {
            req.setUpdateUser(UserUtil.getCurrUserId());
            req.setPath(this.getPath(req.getId()));
            this.updateById(req);
            //permission.setUpdateUser(UserUtil.getCurrUserId());
            //permission.setPath(this.getPath(permission.getPurviewId()));
            //this.sqlSession.update("permissionManage.updatePermission", permission);
        }

        cachePermission();

        return ResponseData.success(req);
    }

    @Override
    public void cachePermission() {
        //缓存所有权限
        List<Permission> allPermission = purviewMapper.qryPermissionList(null, new Permission());
        //List<Permission> allPermission = this.sqlSession.selectList("permissionManage.qryPermissionList");
        //缓存接口权限
        List<Permission> interfacePermission = purviewMapper.getInterfacePermissions(null);
        //List<Permission> interfacePermission = this.sqlSession.selectList("permission.getInterfacePermissions");
        List<com.yao2san.sim.framework.common.auth.Permission> pa = new ArrayList<>();
        List<com.yao2san.sim.framework.common.auth.Permission> pi = new ArrayList<>();

        for (Permission permission : allPermission) {
            com.yao2san.sim.framework.common.auth.Permission p = new com.yao2san.sim.framework.common.auth.Permission();
            BeanUtils.copyProperties(permission, p);
            pa.add(p);
        }
        for (Permission permission : interfacePermission) {
            com.yao2san.sim.framework.common.auth.Permission p = new com.yao2san.sim.framework.common.auth.Permission();
            BeanUtils.copyProperties(permission, p);
            pi.add(p);
        }
        CacheUtil.set(CACHED_ALL_PERMISSION_KEY, pa);
        CacheUtil.set(CACHED_INTERFACE_PERMISSION_KEY, pi);

        log.info("Cache permissions success!");
    }

    private String getPath(Long id) {
        Permission curr = purviewMapper.qryPermissionDetail(id);
        //Permission curr = this.sqlSession.selectOne("permissionManage.qryPermissionDetail", id);
        if (curr.getParentId() == null || curr.getParentId() <= 0) {
            return "/" + id;
        } else {
            Permission parent = purviewMapper.qryPermissionDetail(curr.getParentId());
            //Permission parent = this.sqlSession.selectOne("permissionManage.qryPermissionDetail", curr.getParentId());
            return parent.getPath() + "/" + id;
        }
    }

    private boolean checkCodeExist(String purviewCode) {
        int count = purviewMapper.countPermissionByCode(purviewCode);
        //Integer count = this.sqlSession.selectOne("permissionManage.countPermissionByCode", permission);
        return count > 0;
    }

    private List<Map<String, Object>> buildTree(List<Permission> permissions) {
        Map<String, Map<String, Object>> map = new HashMap<>();
        List<Map<String, Object>> tree = new ArrayList<>();
        permissions.forEach(permission -> {
            Map<String, Object> m = JSONObject.parseObject(JSONObject.toJSONString(permission), new TypeReference<Map<String, Object>>() {
            });
            m.put("children", new ArrayList<>());
            map.put(String.valueOf(permission.getId()), m);
        });
        permissions.forEach(permission -> {
            Map<String, Object> m = map.get(String.valueOf(permission.getId()));
            if (permission.getParentId() != null && permission.getParentId() > 0) {
                String pid = String.valueOf(permission.getParentId());
                Map<String, Object> pm = map.get(pid);
                @SuppressWarnings("unchecked")
                List<Map<String, Object>> children = (List<Map<String, Object>>) pm.get("children");
                children.add(m);
            } else {
                tree.add(m);
            }
        });
        return tree;
    }

    public ResponsePageData<Map<String, Object>> qryPermissionRoles(Permission permission) {
        List<Map<String, Object>> list;
        Page<Map<String, Object>> page = convertPage(permission);
        if ("1".equals(permission.getIsPermissions())) {
            list = purviewMapper.qryPermissionRoles(page, permission.getId(), permission.getFilterText());
            //list = this.qryList("permissionManage.qryPermissionRoles", permission);
        } else if ("0".equals(permission.getIsPermissions())) {
            list = purviewMapper.qryNotPermissionRoles(page, permission.getId(), permission.getFilterText());
            //res = this.qryList("permissionManage.qryNotPermissionRoles", permission);
        } else {
            list = purviewMapper.qryAllPermissionRoles(page, permission.getId(), permission.getFilterText());
            //res = this.qryList("permissionManage.qryAllPermissionRoles", permission);
        }
        return buildPageResult(page, list);
    }

    @Override
    public ResponsePageData<Map<String, Object>> qryPermissionUsers(Permission permission) {
        List<Map<String, Object>> list;
        Page<Map<String, Object>> page = convertPage(permission);
        if ("1".equals(permission.getIsPermissions())) {
            list = purviewMapper.qryPermissionUsers(page, permission.getId(), permission.getFilterText());
            //list = this.qryList("permissionManage.qryPermissionUsers", permission);
        } else if ("0".equals(permission.getIsPermissions())) {
            list = purviewMapper.qryNotPermissionUsers(page, permission.getId(), permission.getFilterText());
            //list = this.qryList("permissionManage.qryNotPermissionUsers", permission);
        } else {
            list = purviewMapper.qryAllPermissionUsers(page, permission.getId(), permission.getFilterText());
            //list = this.qryList("permissionManage.qryAllPermissionUsers", permission);
        }
        return buildPageResult(page, list);
    }

    @Override
    public ResponseData<?> purviewForRole(Map<String, Object> params) {
        JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(params));
        JSONArray roles = object.getJSONArray("roles");
        JSONArray permissions = object.getJSONArray("permissions");
        //操作类型: 1 增加授权 2 取消授权
        String option = object.getString("option");
        if (roles == null
                || permissions == null
                || roles.size() == 0
                || permissions.size() == 0
                || roles.get(0) == null
                || permissions.get(0) == null) {
            return ResponseData.businessError("权限和角色不能为空");
        }
        List<Map<String, Long>> list = new ArrayList<>();
        Long currUserId = UserUtil.getCurrUserId();
        for (Object role : roles) {
            Long roleId = Long.valueOf(String.valueOf(role));
            for (Object permission : permissions) {
                Long purviewId = Long.valueOf(String.valueOf(permission));
                Map<String, Long> map = new HashMap<>(2);
                map.put("roleId", roleId);
                map.put("purviewId", purviewId);
                map.put("createUser", currUserId);
                list.add(map);
            }
        }
        log.info("权限授权角色变更(option={}):{}", option, list);
        //增加授权
        if ("1".equals(option)) {
            //先取消授权
            purviewMapper.delPurviewRoleRel(list);
            //this.sqlSession.delete("permissionManage.delPurviewRoleRel", list);
            //重新授权
            purviewMapper.addPurviewRoleRel(list);
            //this.sqlSession.insert("permissionManage.addPurviewRoleRel", list);
        } else if ("2".equals(option)) {
            //取消授权
            purviewMapper.delPurviewRoleRel(list);
            //this.sqlSession.delete("permissionManage.delPurviewRoleRel", list);
        } else {
            return ResponseData.businessError("不支持的操作类型: option=" + option);
        }

        return ResponseData.success(null, "操作成功");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deletePermission(Long purviewId) {
        this.delPermission(purviewId);
    }

    @Override
    public ResponsePageData<Permission> qryRolePermissions(RolePermissionReq req) {
        List<Permission> list;
        Page<Permission> page = convertPage(req);
        if ("1".equals(req.getIsPermissions())) {
            list = purviewMapper.qryRolePermissions(page, req);
            //pageInfo = this.qryList("permissionManage.qryRolePermissions", req);
        } else if ("0".equals(req.getIsPermissions())) {
            list = purviewMapper.qryRoleNotPermissions(page, req);
            //pageInfo = this.qryList("permissionManage.qryRoleNotPermissions", req);
        } else {
            list = purviewMapper.qryRoleAllPermissions(page, req);
            //pageInfo = this.qryList("permissionManage.qryRoleAllPermissions", req);
        }

        return buildPageResult(page, list);
    }

    private void delPermission(Long purviewId) {
        Permission permission = purviewMapper.qryPermissionDetail(purviewId);
        //Permission permission = this.sqlSession.selectOne("permissionManage.qryPermissionDetail", purviewId);
        if (permission != null) {
            //子节点
            List<Permission> childPermissions = purviewMapper.qryPermissionList(null, new Permission().setParentId(permission.getId()));
            //List<Permission> childPermissions = this.sqlSession.selectList(
            //        "permissionManage.qryPermissionList",
            //        new Permission().setParentId(permission.getPurviewId());
            if (childPermissions.size() > 0) {
                for (Permission childPermission : childPermissions) {
                    this.delPermission(childPermission.getId());
                }
            }
            log.debug("remove permission:{}", permission.getId());
            this.removeById(permission.getId());
            //this.sqlSession.delete("permissionManage.deletePermission", permission.getPurviewId());
        }
    }
}
