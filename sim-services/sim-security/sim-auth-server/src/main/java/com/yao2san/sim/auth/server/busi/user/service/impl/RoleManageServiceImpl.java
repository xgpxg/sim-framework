package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.common.auth.util.UserUtil;
import com.yao2san.sim.auth.server.busi.user.service.RoleManageService;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.RoleAddReq;
import com.yao2san.simservice.bean.simsecurity.request.RoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.RoleUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.user.RoleListRes;
import com.yao2san.simservice.entity.simsecurity.Role;
import com.yao2san.simservice.enums.common.YesOrNo;
import com.yao2san.simservice.mapper.simsecurity.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class RoleManageServiceImpl extends ServiceBaseImpl<RoleMapper, Role> implements RoleManageService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public ResponsePageData<RoleListRes> qryRoleList(RoleListReq req) {
        Page<RoleListRes> page = convertPage(req);
        List<RoleListRes> list = roleMapper.qryRoleList(page, req);
        return buildPageResult(page, list);
    }

    @Override
    @Transactional
    public ResponseData<?> addRole(RoleAddReq req) {
        if (this.isRoleCodeExist(req.getRoleCode())) {
            return ResponseData.businessError("角色编码[" + req.getRoleCode() + "]已存在");
        }
        Long userId = UserUtil.getCurrUserId();
        req.setCreateUser(userId);
        req.setUpdateUser(userId);
        req.setStatus("1000");
        this.save(req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<?> updateRole(RoleUpdateReq req) {
        if (req.getId() == null) {
            return ResponseData.businessError("角色标识不能为空");
        }
        Long userId = UserUtil.getCurrUserId();
        req.setUpdateUser(userId);
        req.setUpdateDate(LocalDateTime.now());
        this.updateById(req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<?> deleteRole(Long roleId) {
        if (roleId == null) {
            return ResponseData.businessError("角色标识不能为空");
        }
        this.updateById(new Role().setId(roleId).setIsDelete(YesOrNo.YES));
        return ResponseData.success();
    }

    private boolean isRoleCodeExist(String roleCode) {
        long count = this.count(Wrappers.lambdaQuery(Role.class).eq(Role::getRoleCode, roleCode));
        return count > 0;
    }
}
