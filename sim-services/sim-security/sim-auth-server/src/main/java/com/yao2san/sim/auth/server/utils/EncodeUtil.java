package com.yao2san.sim.auth.server.utils;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.util.ByteSource;

public class EncodeUtil {
    private final static String SALT = "nDpvNobVa4Ot1w3mgAL8xSXI90JQHdGE";

    public static String encodePwd(String pwd) {
        ByteSource salt = getSalt(SALT);
        Object result = new Sha256Hash(pwd, salt, 1024);
        return result.toString();
    }

    public static ByteSource getSalt(String salt) {
        return ByteSource.Util.bytes(salt);
    }

    public static void main(String[] args) {
        System.out.println(EncodeUtil.encodePwd("123456"));
    }
}
