package com.yao2san.sim.auth.server.busi.oauth;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Oauth2授权接口实现(待完成)
 */
@RequestMapping("oauth")
public class OauthController {
    /**
     * 获取授权码
     */
    @GetMapping("authorize")
    public void authorize(@RequestParam(name = "response_type") String responseType,
                          @RequestParam(name = "response_type", required = false) String clientId,
                          @RequestParam(name = "redirect_uri", required = false) String redirectUri,
                          @RequestParam(name = "scope", required = false) String scope,
                          @RequestParam(name = "state", required = false) String state,
                          HttpServletResponse response) throws IOException, URISyntaxException {

        String code = "";
        String url = redirectUri;
        URI uri = new URI(redirectUri);
        if (StringUtils.isNotEmpty(uri.getQuery())) {
            url += "&code=" + code;
        } else {
            url = url.endsWith("?") ? (url += "code=" + code) : (url += "?code=" + code);
        }
        response.sendRedirect(url);
    }

    /**
     * code换token
     */
    @PostMapping("token")
    @ResponseBody
    public Map<String, Object> token(@RequestParam(name = "grant_type") String grantType,
                                     @RequestParam(name = "code") String code,
                                     @RequestParam(name = "redirect_uri") String redirectUri,
                                     @RequestParam(name = "client_id") String clientId
    ) {
        Map<String, Object> result = new HashMap<>();
        result.put("access_token", "");
        result.put("token_type", "");
        result.put("expires_in", 3600);
        result.put("refresh_token", "");
        result.put("scope", "");
        return result;
    }

    /**
     * 获取AccessToken
     *
     * @param appId     应用ID
     * @param appSecret 应用密钥
     * @return access token
     */
    @GetMapping("getAccessToken")
    @ResponseBody
    public Map<String, Object> getAccessToken(@RequestParam(name = "appId") String appId,
                                              @RequestParam(name = "appSecret") String appSecret) {

        Map<String, Object> result = new HashMap<>();
        return result;
    }
}
