package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.simservice.bean.simsecurity.request.user.AccessTokenResponse;
import com.yao2san.simservice.bean.simsecurity.request.user.UserLoginReq;
import com.yao2san.simservice.bean.simsecurity.request.user.UserSignInReq;
import com.yao2san.simservice.bean.simsecurity.request.user.UserSignUpReq;
import com.yao2san.simservice.enums.user.LoginType;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.simservice.bean.simsecurity.common.UserAuth;

import java.util.Map;

/**
 * @author wxg
 */
public interface SecurityService {

   /* *//**
     * 用户登录
     *//*
    @Deprecated
    ResponseData<Map<String, Object>> login(UserLoginReq req);*/

    /**
     * 用户登录(新)
     */
    AccessTokenResponse signIn(UserSignInReq req);

    /**
     * 用户注册(新)
     */
    AccessTokenResponse signUp(UserSignUpReq req);
}
