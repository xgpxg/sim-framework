package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.user.service.TenantManagerService;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceAuthorizationReq;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceListReq;
import com.yao2san.simservice.bean.simsecurity.response.TenantServiceListRes;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author wxg
 * <p>
 * 租户管理接口
 */
@RestController
@RequestMapping("/tenantManager")
public class TenantManagerController {
    private final TenantManagerService tenantManagerService;

    public TenantManagerController(TenantManagerService tenantManagerService) {
        this.tenantManagerService = tenantManagerService;
    }

    /**
     * 查询租户服务列表
     */
    @GetMapping("/service")
    public ResponseData<ResponsePageData<TenantServiceListRes>> services(@Validated TenantServiceListReq req) {
        return ResponseData.success(tenantManagerService.services(req));
    }

    /**
     * 服务授权or解除授权
     */
    @PostMapping("/service")
    public ResponseData<?> serviceAuthorization(@RequestBody TenantServiceAuthorizationReq req) {
        return tenantManagerService.serviceAuthorization(req);
    }


    @DeleteMapping("/service")
    public ResponseData<?> deleteServiceAuthorization() {
        return null;
    }
}
