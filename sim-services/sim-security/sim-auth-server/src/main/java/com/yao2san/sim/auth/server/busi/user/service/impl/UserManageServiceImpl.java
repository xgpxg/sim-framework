package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.common.auth.util.UserUtil;
import com.yao2san.sim.auth.server.busi.constant.SystemConstant;
import com.yao2san.sim.auth.server.busi.user.service.UserManageService;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.core.uploader.Uploader;
import com.yao2san.sim.storage.client.exception.UploadErrorException;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.UserRoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.user.RestPasswordReq;
import com.yao2san.simservice.bean.simsecurity.response.UserRoleRes;
import com.yao2san.simservice.bean.simsecurity.request.UserAddReq;
import com.yao2san.simservice.bean.simsecurity.request.UserListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.user.UserDetailRes;
import com.yao2san.simservice.bean.simsecurity.response.user.UserListRes;
import com.yao2san.simservice.common.IdUtil;
import com.yao2san.simservice.entity.simsecurity.User;
import com.yao2san.simservice.entity.simsecurity.UserAuth;
import com.yao2san.simservice.enums.user.IdentityType;
import com.yao2san.simservice.enums.user.RegisterSource;
import com.yao2san.simservice.enums.user.UserStatus;
import com.yao2san.simservice.mapper.simsecurity.RoleMapper;
import com.yao2san.simservice.mapper.simsecurity.UserAuthMapper;
import com.yao2san.simservice.mapper.simsecurity.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserManageServiceImpl extends ServiceBaseImpl<UserMapper, User> implements UserManageService {

    @Autowired
    private Uploader uploader;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private UserAuthMapper userAuthMapper;

    @Override
    public ResponsePageData<UserListRes> qryUserList(UserListReq req) {
        //用户信息
        Page<UserListRes> page = convertPage(req);
        List<UserListRes> list = userMapper.qryUserList(page, req);
        //角色
        list.forEach(u -> {
            List<UserRoleRes> userRoleRes = roleMapper.getUserRoles(u.getId());
            u.setRoles(userRoleRes.stream().map(UserRoleRes::getRoleName).collect(Collectors.joining(",")));
        });
        return buildPageResult(page, list);
    }

    @Override
    public ResponsePageData<Permission> qryUserPermissions(Permission req) {
        List<Permission> list;
        Page<Permission> page = convertPage(req);
        if ("1".equals(req.getIsPermissions())) {
            list = userMapper.qryUserPermissions(page, req);
        } else {
            list = userMapper.qryUserNotPermissions(page, req);
        }
        return buildPageResult(page, list);
    }

    @Override
    @Transactional
    public void addOrUpdateUserPermissions(Map<String, Object> params) {
        JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(params));
        JSONArray userIds = object.getJSONArray("userIds");
        JSONArray permissions = object.getJSONArray("permissions");
        //操作类型: 1 增加授权 2 取消授权
        String option = object.getString("option");
        if (userIds == null
                || permissions == null
                || userIds.size() == 0
                || permissions.size() == 0
                || userIds.get(0) == null
                || permissions.get(0) == null) {
            throw new BusiException("权限和用户标识不能为空");
        }
        List<Map<String, Long>> list = new ArrayList<>();
        Long currUserId = UserUtil.getCurrUserId();
        for (Object userId : userIds) {
            Long uid = Long.valueOf(String.valueOf(userId));
            for (Object permission : permissions) {
                Long purviewId = Long.valueOf(String.valueOf(permission));
                Map<String, Long> map = new HashMap<>(2);
                map.put("userId", uid);
                map.put("purviewId", purviewId);
                map.put("createUser", currUserId);
                list.add(map);
            }
        }
        log.info("用户特殊授权变更(option={}):{}", option, list);
        //增加授权
        if ("1".equals(option)) {
            //先取消授权
            userMapper.delPurviewUserRel(list);
            //重新授权
            userMapper.addPurviewUserRel(list);
        } else if ("2".equals(option)) {
            //取消授权
            userMapper.delPurviewUserRel(list);
        } else {
            throw new BusiException("不支持的操作类型: option=" + option);
        }

        throw new BusiException("操作成功");
    }

    public ResponsePageData<Map<String, Object>> qryUserRoles(UserRoleListReq req) {
        Page<Map<String, Object>> page = convertPage(req);
        List<Map<String, Object>> list = userMapper.qryUserRoles(page, req);
        return buildPageResult(page, list);
    }

    @Override
    @Transactional
    public void addOrUpdateUserRoles(Map<String, Object> params) {
        JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(params));
        JSONArray userIds = object.getJSONArray("userIds");
        JSONArray permissions = object.getJSONArray("roleIds");
        //操作类型: 1 增加授权 2 取消授权
        String option = object.getString("option");
        if (userIds == null
                || permissions == null
                || userIds.size() == 0
                || permissions.size() == 0
                || userIds.get(0) == null
                || permissions.get(0) == null) {
            throw new BusiException("角色和用户标识不能为空");
        }
        List<Map<String, Long>> list = new ArrayList<>();
        Long currUserId = UserUtil.getCurrUserId();
        for (Object userId : userIds) {
            Long uid = Long.valueOf(String.valueOf(userId));
            for (Object role : permissions) {
                Long roleId = Long.valueOf(String.valueOf(role));
                Map<String, Long> map = new HashMap<>(2);
                map.put("userId", uid);
                map.put("roleId", roleId);
                map.put("createUser", currUserId);
                list.add(map);
            }
        }
        log.info("用户角色变更(option={}):{}", option, list);
        //增加授权
        if ("1".equals(option)) {
            //先取消授权
            userMapper.delUserRoleRel(list);
            //重新授权
            userMapper.addUserRoleRel(list);
        } else if ("2".equals(option)) {
            //取消授权
            userMapper.delUserRoleRel(list);
        } else {
            throw new BusiException("不支持的操作类型: option=" + option);
        }
    }

    @Override
    @Transactional
    public ResponseData<Void> addUser(UserAddReq req) {
        String encodePwd = EncodeUtil.encodePwd(req.getPhone().substring(5, 11));
        req.setStatus(UserStatus.OK.getCode());
        req.setSource(RegisterSource.SYSTEM_CREATE.getCode());
        this.save(req);
        userAuthMapper.insert(new UserAuth()
                .setId(IdUtil.nextId())
                .setUserId(req.getId())
                .setType(IdentityType.USERNAME.getType())
                .setIdentity(req.getUserName())
                .setSecret(encodePwd)
                .setCreateDate(LocalDateTime.now())
                .setCreateUser(UserUtil.getCurrUserId()));
        addUserOrgRel(req.getId(), req.getOrgId());
        return ResponseData.success();
    }

    private void addUserOrgRel(Long userId, Long orgId) {
        if (orgId != null) {
            userMapper.delUserOrgRel(userId);
            userMapper.addUserOrgRel(userId, orgId);
        }
    }

    @Override
    @Transactional
    public void updateUser(UserUpdateReq req) {
        this.updateById(req);
        addUserOrgRel(req.getId(), req.getOrgId());
        userAuthMapper.update(null, Wrappers.lambdaUpdate(UserAuth.class)
                .eq(UserAuth::getUserId, req.getId())
                .eq(UserAuth::getType, IdentityType.USERNAME.getType())
                .set(UserAuth::getIdentity, req.getUserName())
                .set(UserAuth::getUpdateDate, LocalDateTime.now())
                .set(UserAuth::getUpdateUser, UserUtil.getCurrUserId()));
    }

    @Override
    public UserDetailRes detail(Long id) {
        return userMapper.qryUser(id);
    }

    @Override
    public ResponseData<Void> resetPwd(RestPasswordReq req) {
        String encodePwd = EncodeUtil.encodePwd(req.getPassword());
        userAuthMapper.update(null, Wrappers.lambdaUpdate(UserAuth.class)
                .eq(UserAuth::getUserId, req.getId())
                .eq(UserAuth::getType, IdentityType.USERNAME.getType())
                .set(UserAuth::getSecret, encodePwd));
        return ResponseData.success();
    }

    @Override
    public ResponseData<String> uploadAvatar(MultipartFile file) {
        try {
            UploadResult uploadResult = uploader.upload(file, SystemConstant.USER_HEAD_IMAGE_PATH + UUID.randomUUID() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
            return ResponseData.success(uploadResult.getUrl());
        } catch (IOException | UploadErrorException e) {
            throw new BusiException("头像上传失败");
        }

    }
}
