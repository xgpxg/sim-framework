package com.yao2san.sim.auth.server.shiro.token;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author wxg
 */
@NoArgsConstructor
@AllArgsConstructor
public class PhoneToken implements AuthenticationToken {
    @Getter
    private String phone;
    @Getter
    private String verifyCode;

    @Override
    public Object getPrincipal() {
        return this.phone;
    }

    @Override
    public Object getCredentials() {
        return this.verifyCode;
    }
}
