package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.UserAddReq;
import com.yao2san.simservice.bean.simsecurity.request.UserListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserRoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserUpdateReq;
import com.yao2san.simservice.bean.simsecurity.request.user.RestPasswordReq;
import com.yao2san.simservice.bean.simsecurity.response.user.UserDetailRes;
import com.yao2san.simservice.bean.simsecurity.response.user.UserListRes;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 用户管理
 */
public interface UserManageService {
    /**
     * 查询用户列表
     */
    ResponsePageData<UserListRes> qryUserList(UserListReq user);

    ResponsePageData<Permission> qryUserPermissions(Permission permission);

    void addOrUpdateUserPermissions(Map<String, Object> params);

    ResponsePageData<Map<String, Object>> qryUserRoles(UserRoleListReq req);

    void addOrUpdateUserRoles(Map<String, Object> params);

    ResponseData<Void> addUser(UserAddReq user);

    void updateUser(UserUpdateReq user);

    UserDetailRes detail(Long userId);

    ResponseData<Void> resetPwd(RestPasswordReq req);

    ResponseData<String> uploadAvatar(MultipartFile file);
}
