package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceAuthorizationReq;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceListReq;
import com.yao2san.simservice.bean.simsecurity.response.TenantServiceListRes;

public interface TenantManagerService {
    ResponsePageData<TenantServiceListRes> services(TenantServiceListReq req);
    ResponseData<?> serviceAuthorization(TenantServiceAuthorizationReq req);

}
