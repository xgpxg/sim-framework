package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.RoleAddReq;
import com.yao2san.simservice.bean.simsecurity.request.RoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.RoleUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.UserRoleRes;
import com.yao2san.simservice.bean.simsecurity.response.user.RoleListRes;

import java.util.Map;

public interface RoleManageService {
    /**
     * 查询角色列表
     */
    ResponsePageData<RoleListRes> qryRoleList(RoleListReq req);

    /**
     * 新增角色
     */
    ResponseData<?> addRole(RoleAddReq req);

    /**
     * 更新角色
     */
    ResponseData<?> updateRole(RoleUpdateReq req);

    /**
     * 删除角色
     */
    ResponseData<?> deleteRole(Long roleId);
}
