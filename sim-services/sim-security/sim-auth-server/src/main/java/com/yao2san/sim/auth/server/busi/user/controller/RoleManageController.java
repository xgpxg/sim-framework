package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.user.service.RoleManageService;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.RoleAddReq;
import com.yao2san.simservice.bean.simsecurity.request.RoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.RoleUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.user.RoleListRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 角色管理
 */
@RestController
@RequestMapping("role")
public class RoleManageController {
    private final RoleManageService roleManageService;

    public RoleManageController(RoleManageService roleManageService) {
        this.roleManageService = roleManageService;
    }

    /**
     * 查询角色列表
     *
     * @param req 查询参数
     * @return 角色列表
     */
    @GetMapping
    public ResponseData<ResponsePageData<RoleListRes>> roleList(RoleListReq req) {
        return ResponseData.success(roleManageService.qryRoleList(req));
    }

    /**
     * 新增角色
     *
     * @param req 参数
     * @return result
     */
    @PostMapping
    public ResponseData<?> addRole(@RequestBody RoleAddReq req) {
        return roleManageService.addRole(req);
    }

    /**
     * 修改角色
     *
     * @param req 参数
     * @return result
     */
    @PatchMapping
    public ResponseData<?> updateRole(@RequestBody RoleUpdateReq req) {
        return roleManageService.updateRole(req);
    }

    /**
     * 删除角色
     *
     * @param roleId 角色ID
     * @return result
     */
    @DeleteMapping("{roleId}")
    public ResponseData<?> deleteRole(@PathVariable Long roleId) {
        return roleManageService.deleteRole(roleId);
    }
}
