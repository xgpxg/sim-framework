package com.yao2san.sim.auth.server.busi.org.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.lang.tree.parser.NodeParser;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.OrgAddReq;
import com.yao2san.simservice.bean.simsecurity.request.OrgListReq;
import com.yao2san.simservice.bean.simsecurity.request.OrgUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.OrgDetailRes;
import com.yao2san.simservice.bean.simsecurity.response.OrgListRes;
import com.yao2san.simservice.bean.simsecurity.response.OrgTreeRes;
import com.yao2san.sim.auth.server.busi.org.service.OrgService;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.simservice.entity.simsecurity.Org;
import com.yao2san.simservice.mapper.simsecurity.OrgMapper;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wxg
 */
@Service
public class OrgServiceImpl extends ServiceBaseImpl<OrgMapper, Org> implements OrgService {

    @Autowired
    private OrgMapper orgMapper;

    private static final TreeNodeConfig TREE_NODE_CONFIG;

    static {
        TREE_NODE_CONFIG = new TreeNodeConfig();
        TREE_NODE_CONFIG.setIdKey("orgId");
        TREE_NODE_CONFIG.setParentIdKey("parentId");
    }

    @Override
    public ResponsePageData<OrgListRes> list(OrgListReq req) {
        Page<OrgListRes> page = convertPage(req);
        List<OrgListRes> list = orgMapper.list(page, req);
        //PageInfo<OrgListRes> list = this.qryList("org.list", req);
        return buildPageResult(page, list);
    }

    @Override
    public ResponseData<List<Tree<Long>>> tree(OrgListReq req) {
        List<OrgListRes> list = orgMapper.list(null, req);
        //List<OrgListRes> list = this.sqlSession.selectList("org.list", req);
        List<OrgTreeRes> treeRes = new ArrayList<>();
        for (OrgListRes res : list) {
            OrgTreeRes orgTreeRes = new OrgTreeRes();
            BeanUtils.copyProperties(res, orgTreeRes);
            treeRes.add(orgTreeRes);
        }
        List<Tree<Long>> tree = TreeUtil.build(treeRes, -1L, TREE_NODE_CONFIG, new NodeParser<OrgTreeRes, Long>() {
            @SneakyThrows
            @Override
            public void parse(OrgTreeRes treeNode, Tree<Long> tree) {
                tree.setId(treeNode.getOrgId());
                tree.setParentId(treeNode.getParentId());

                Field[] fields = treeNode.getClass().getDeclaredFields();
                for (Field field : fields) {
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    Object o = field.get(treeNode);
                    tree.put(field.getName(), o);
                    field.setAccessible(accessible);
                }
            }
        });
        return ResponseData.success(tree);
    }

    @Override
    public ResponseData<OrgDetailRes> detail(Long orgId) {
        return null;
    }

    @Override
    @Transactional
    public ResponseData<Void> add(OrgAddReq req) {
        Integer count = orgMapper.isOrgCodeExist(req.getOrgCode());
        //Integer count = sqlSession.selectOne("org.isOrgCodeExist", req.getOrgCode());
        if (count > 0) {
            return ResponseData.error("机构编码已存在");
        }
        count = orgMapper.isOrgNameExist(req.getOrgName());
        //count = sqlSession.selectOne("org.isOrgNameExist", req.getOrgName());
        if (count > 0) {
            return ResponseData.error("机构名称已存在");
        }
        this.save(req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> update(OrgUpdateReq req) {
        this.updateById(req);
        //this.sqlSession.update("org.updateOrg", req);
        return ResponseData.success();
    }

    @Override
    @Transactional
    public ResponseData<Void> delete(List<Long> orgIds) {
        //this.sqlSession.delete("org.deleteOrg", orgIds);
        this.removeByIds(orgIds);
        return ResponseData.success();
    }
}
