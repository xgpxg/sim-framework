package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.user.service.UserManageService;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.UserAddReq;
import com.yao2san.simservice.bean.simsecurity.request.UserListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserRoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserUpdateReq;
import com.yao2san.simservice.bean.simsecurity.request.user.RestPasswordReq;
import com.yao2san.simservice.bean.simsecurity.response.user.UserDetailRes;
import com.yao2san.simservice.bean.simsecurity.response.user.UserListRes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("userManage")
@Api(tags = "用户管理")
public class UserManageController {
    private final UserManageService userManageService;

    public UserManageController(UserManageService userManageService) {
        this.userManageService = userManageService;
    }

    /**
     * 查询用户列表
     *
     * @param user 查询参数
     * @return 用户列表
     */
    @ApiOperation(value = "查询用户列表")
    @GetMapping
    public ResponseData<ResponsePageData<UserListRes>> userList(UserListReq user) {
        return ResponseData.success(userManageService.qryUserList(user));
    }

    /**
     * 查询用户详情
     *
     * @param id 用户ID
     * @return 用户详情
     */
    @GetMapping("{id}")
    public ResponseData<UserDetailRes> detail(@PathVariable Long id) {
        return ResponseData.success(userManageService.detail(id));
    }

    @GetMapping("permissions")
    public ResponseData<ResponsePageData<Permission>> qryUserPermissions(Permission permission) {
        return ResponseData.success(userManageService.qryUserPermissions(permission));
    }

    @PostMapping("permissions")
    public ResponseData<Void> addOrUpdateUserPermissions(@RequestBody Map<String, Object> params) {
        userManageService.addOrUpdateUserPermissions(params);
        return ResponseData.success();
    }

    @GetMapping("roles")
    public ResponseData qryUserRoles(UserRoleListReq req) {
        return ResponseData.success(userManageService.qryUserRoles(req));
    }


    /**
     * 新增或修改用户角色
     *
     * @param params 参数
     * @return result
     */
    @PostMapping("roles")
    public ResponseData<Void> addOrUpdateUserRoles(@RequestBody Map<String, Object> params) {
        userManageService.addOrUpdateUserRoles(params);
        return ResponseData.success();
    }

    /**
     * 新增用户
     */
    @PostMapping
    public ResponseData<Void> addUser(@RequestBody @Validated UserAddReq user) {
        return userManageService.addUser(user);
    }

    /**
     * 修改用户
     */
    @PatchMapping
    public ResponseData<Void> updateUser(@RequestBody @Validated UserUpdateReq user) {
        userManageService.updateUser(user);
        return ResponseData.success();
    }

    /**
     * 上传用户头像
     */
    @PostMapping("avatar")
    public ResponseData<String> uploadAvatar(@RequestPart("file") MultipartFile multipartFile) {
        return userManageService.uploadAvatar(multipartFile);
    }

    /**
     * 重置用户密码
     */
    @PatchMapping("/resetPwd")
    public ResponseData<Void> resetPwd(@RequestBody @Validated RestPasswordReq req) {
        userManageService.resetPwd(req);
        return ResponseData.success();
    }

}
