package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.yao2san.sim.auth.server.busi.user.service.RoleService;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.simservice.bean.simsecurity.response.UserRoleRes;
import com.yao2san.simservice.mapper.simsecurity.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleServiceImpl extends BaseServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<UserRoleRes> getUserRoles(Long userId) {
        return roleMapper.getUserRoles(userId);
    }

    @Override
    public boolean hasRole(Long userId, Long roleId) {
        Integer count = roleMapper.hasRole(userId, roleId);
        return count > 0;
    }
}
