package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yao2san.sim.framework.common.auth.UserPrincipal;
import com.yao2san.sim.auth.server.busi.user.service.AccountService;
import com.yao2san.sim.auth.server.busi.user.service.PermissionService;
import com.yao2san.sim.auth.server.busi.user.service.RoleService;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.response.UserRoleRes;
import com.yao2san.simservice.entity.simsecurity.User;
import com.yao2san.simservice.entity.simsecurity.UserAuth;
import com.yao2san.simservice.enums.user.UserType;
import com.yao2san.simservice.mapper.simsecurity.UserAuthMapper;
import com.yao2san.simservice.mapper.simsecurity.UserMapper;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl extends ServiceBaseImpl<UserMapper, User> implements AccountService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserAuthMapper userAuthMapper;

    @Override
    public UserPrincipal getUserPrincipal(Integer type, String identity, String secret) {
        UserAuth userAuth = getUserAuth(type, identity);
        if (userAuth == null) {
            throw new UnknownAccountException("用户不存在");
        }
        User user = userMapper.selectById(userAuth.getUserId());
        if (user == null) {
            throw new UnknownAccountException("用户不存在");
        }
        Long uid = user.getId();
        String un = user.getUserName();
        String pwd = userAuth.getSecret();
        String avatar = user.getAvatar();
        String encodePwd = EncodeUtil.encodePwd(secret);
        if (!encodePwd.equals(pwd)) {
            throw new IncorrectCredentialsException("用户名或密码不正确");
        }

        //用户基本信息
        UserPrincipal userPrincipal = new UserPrincipal();
        userPrincipal.setId(uid);
        userPrincipal.setNickname(un);
        userPrincipal.setType(user.getUserType());
        userPrincipal.setAvatar(avatar);
        //角色和权限
        RoleService roleService = BeanContextUtil.getBean(RoleService.class);
        PermissionService permissionService = BeanContextUtil.getBean(PermissionService.class);
        List<UserRoleRes> userRoleRes = roleService.getUserRoles(user.getId());
        List<Permission> userPermissions = permissionService.getUserPermissions(user.getId());

        userPrincipal.setRoles(userRoleRes.stream().map(UserRoleRes::getRoleCode).collect(Collectors.toSet()));
        userPrincipal.setPermissions(userPermissions.stream().map(Permission::getPurviewCode).collect(Collectors.toSet()));

        return userPrincipal;
    }


    private UserAuth getUserAuth(Integer type, String identity) {
        return userAuthMapper.selectOne(Wrappers.lambdaQuery(UserAuth.class)
                .eq(UserAuth::getType, type)
                .eq(UserAuth::getIdentity, identity));
    }
}
