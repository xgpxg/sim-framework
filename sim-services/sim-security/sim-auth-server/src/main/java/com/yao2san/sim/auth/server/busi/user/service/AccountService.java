package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.framework.common.auth.UserPrincipal;

public interface AccountService {
    UserPrincipal getUserPrincipal(Integer type,String identity, String secret);
}
