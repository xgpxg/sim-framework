package com.yao2san;

import com.yao2san.sim.storage.client.annotations.SimStorageClient;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author wxg
 */
@SpringBootApplication
@Slf4j
@SimStorageClient
@MapperScan("com.yao2san.simservice.mapper.**")
public class SimAuthServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimAuthServerApplication.class, args);
        log.info("sim-auth-server start success!");
    }
}
