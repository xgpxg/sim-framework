package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.yao2san.sim.auth.server.shiro.token.PhoneToken;
import com.yao2san.sim.framework.common.auth.config.AuthProperties;
import com.yao2san.sim.framework.common.auth.enums.AuthType;
import com.yao2san.sim.framework.common.auth.util.JwtUtil;
import com.yao2san.sim.framework.common.auth.UserPrincipal;
import com.yao2san.simservice.bean.simsecurity.request.user.AccessTokenResponse;
import com.yao2san.simservice.bean.simsecurity.request.user.UserLoginReq;
import com.yao2san.simservice.bean.simsecurity.request.user.UserSignInReq;
import com.yao2san.simservice.bean.simsecurity.request.user.UserSignUpReq;
import com.yao2san.simservice.enums.user.IdentityType;
import com.yao2san.simservice.enums.user.LoginType;
import com.yao2san.sim.auth.server.busi.user.service.*;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.simservice.mapper.simsecurity.UserAuthMapper;
import com.yao2san.simservice.mapper.simsecurity.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wxg
 */
@Slf4j
@Service
public class SecurityServiceImpl extends BaseServiceImpl implements SecurityService {
    private final UserService userService;
    private final AuthProperties properties;
    private final AccountService accountService;
    private final UserMapper userMapper;
    private final UserAuthMapper userAuthMapper;

    public SecurityServiceImpl(UserService userService, AuthProperties properties, AccountService accountService, UserMapper userMapper, UserAuthMapper userAuthMapper) {
        this.userService = userService;
        this.properties = properties;
        this.accountService = accountService;
        this.userMapper = userMapper;
        this.userAuthMapper = userAuthMapper;
    }

/*    @Override
    @Transactional(rollbackFor = Exception.class)
    @Deprecated
    public ResponseData<Map<String, Object>> login(UserLoginReq req) {
        LoginType loginType = LoginType.of(req.getLoginType());
        String token;
        try {
            switch (loginType) {
                //用户名密码登陆
                case UN:
                    token = login(req.getUsername(), req.getPassword(), properties.getAuthType());
                    break;
                default:
                    return ResponseData.error("不支持的登陆方式");
            }

        } catch (UnknownAccountException e) {
            throw e;
        } catch (AccountException e) {
            return ResponseData.businessError(e.getMessage());
        }
        Map<String, Object> tokenMap = new HashMap<>();
        tokenMap.put("token", token);

        return ResponseData.success(tokenMap);
    }

    private String login(String username, String password, AuthType authType) {
        Subject subject = SecurityUtils.getSubject();
        if (authType == AuthType.SESSION) {
            subject.login(new UsernamePasswordToken(username, password));
            return String.valueOf(SecurityUtils.getSubject().getSession().getId());
        }
        *//*if (authType == AuthType.JWT) {
            UserPrincipal user = accountService.getUserPrincipal(username, password);
            String jwt = JwtUtil.create(JSONObject.parseObject(JSONObject.toJSONString(user), new TypeReference<Map<String, ?>>() {
            }));
            subject.login(new BearerToken(jwt));

            return jwt;
        }*//*

        return "";
    }*/


    @Override
    public AccessTokenResponse signIn(UserSignInReq req) {
        IdentityType identityType = IdentityType.of(req.getType());
        Subject subject = SecurityUtils.getSubject();
        switch (identityType) {
            case USERNAME:
                subject.login(new UsernamePasswordToken(req.getIdentity(), req.getSecret()));
                break;
            case PHONE_NUMBER:
                subject.login(new PhoneToken(req.getIdentity(), req.getSecret()));
                break;
            default:
                break;
        }
        return new AccessTokenResponse();
    }

    @Override
    public AccessTokenResponse signUp(UserSignUpReq req) {
        //创建用户信息

        //创建用户授权信息

        //自动登录
        signIn(new UserSignInReq(req.getType(), req.getIdentity(), req.getSecret()));
        return null;
    }

    private void addUser(UserSignUpReq req) {

    }

    private void addUserAuth(UserSignUpReq req) {

    }
}
