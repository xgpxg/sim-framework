package com.yao2san.sim.auth.server.busi.user.service;

import com.yao2san.sim.framework.common.auth.UserPrincipal;

public interface UserAuthService {
    UserPrincipal login(Integer type,String identity,String secret);
}
