package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.constant.SystemConstant;
import com.yao2san.sim.framework.common.auth.UserPrincipal;
import com.yao2san.sim.auth.server.busi.user.service.SecurityService;
import com.yao2san.sim.framework.cache.utils.CacheUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.simservice.bean.simsecurity.request.user.AccessTokenResponse;
import com.yao2san.simservice.bean.simsecurity.request.user.UserSignInReq;
import com.yao2san.simservice.bean.simsecurity.request.user.UserSignUpReq;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.*;


/**
 * @author wxg
 */
@RestController
@RequestMapping("security")
@Slf4j
public class SecurityController {
    private final SecurityService securityService;

    public SecurityController(SecurityService securityService) {
        this.securityService = securityService;
    }

    /**
     * 登出
     */
    @GetMapping("logout")
    public ResponseData<Void> logout() {
        Subject subject = SecurityUtils.getSubject();
        //清除认证信息缓存
        UserPrincipal userPrincipal = (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
        Long id = userPrincipal.getId();
        String key = SystemConstant.SHIRO_AUTH_CACHE_KEY_PREFIX + id;
        CacheUtil.del(key);
        subject.logout();
        return ResponseData.success();
    }

    /**
     * 获取图片验证码
     *
     * @return
     */
    @PostMapping("getImageCode")
    public ResponseData<?> sendImageCode() {
        return null;
    }

    /**
     * 获取短信验证码
     */
    @PostMapping("sendSMSCode")
    public ResponseData<?> sendSMSCode(String phone) {
        return null;
    }

    /**
     * 用户登录(新)
     */
    @PostMapping("signIn")
    public ResponseData<AccessTokenResponse> signIn(@RequestBody UserSignInReq req) {
        return ResponseData.success(securityService.signIn(req));
    }

    /**
     * 用户注册(新)
     */
    @PostMapping("signUp")
    public ResponseData<AccessTokenResponse> signUp(@RequestBody UserSignUpReq req) {
        return ResponseData.success(securityService.signUp(req));
    }
}
