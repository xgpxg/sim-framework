package com.yao2san.sim.auth.server.shiro.realm;

import com.yao2san.sim.auth.server.busi.user.service.AccountService;
import com.yao2san.sim.auth.server.shiro.token.PhoneToken;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.common.auth.UserPrincipal;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import com.yao2san.simservice.enums.user.IdentityType;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

@Component
public class PhoneRealm extends AuthorizingRealm {
    private final static String REALM_NAME = "phone";

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof PhoneToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        UserPrincipal userPrincipal = (UserPrincipal) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(userPrincipal.getRoles());
        authorizationInfo.setStringPermissions(userPrincipal.getPermissions());
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        PhoneToken token = (PhoneToken) authenticationToken;
        String userName = token.getPhone();
        String userPwd = token.getVerifyCode();

        AccountService accountService = BeanContextUtil.getBean(AccountService.class);
        UserPrincipal user = accountService.getUserPrincipal(IdentityType.PHONE_NUMBER.getType(), userName, userPwd);
        return new SimpleAuthenticationInfo(user, userPwd, EncodeUtil.getSalt(userName), getName());
    }


    @Override
    public String getName() {
        return REALM_NAME;
    }

    @Override
    public boolean isCachingEnabled() {
        return true;
    }
}
