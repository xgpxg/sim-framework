package com.yao2san.sim.auth.server.busi.org.controller;

import cn.hutool.core.lang.tree.Tree;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.OrgAddReq;
import com.yao2san.simservice.bean.simsecurity.request.OrgListReq;
import com.yao2san.simservice.bean.simsecurity.request.OrgUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.OrgDetailRes;
import com.yao2san.simservice.bean.simsecurity.response.OrgListRes;
import com.yao2san.sim.auth.server.busi.org.service.OrgService;
import com.yao2san.sim.framework.web.response.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("orgManage")
@Api(tags = "机构管理")
public class OrgController {
    @Autowired
    private OrgService orgService;

    @ApiOperation(value = "查询机构列表")
    @GetMapping
    public ResponseData<ResponsePageData<OrgListRes>> list(OrgListReq req) {
        return ResponseData.success(orgService.list(req));
    }
    @ApiOperation(value = "查询机构列表")
    @GetMapping("tree")
    public ResponseData<List<Tree<Long>>> tree(OrgListReq req) {
        return orgService.tree(req);
    }

    @ApiOperation(value = "查询机构详情")
    @GetMapping("{orgId}")
    public ResponseData<OrgDetailRes> detail(@PathVariable Long orgId) {
        return orgService.detail(orgId);
    }

    @ApiOperation(value = "新增机构")
    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated OrgAddReq req) {
        return orgService.add(req);
    }

    @ApiOperation(value = "更新机构")
    @PatchMapping
    public ResponseData<Void> update(@RequestBody @Validated OrgUpdateReq req) {
        return orgService.update(req);
    }

    @ApiOperation(value = "删除机构")
    @DeleteMapping
    public ResponseData<Void> delete(@PathVariable List<Long> orgIds) {
        return orgService.delete(orgIds);
    }
}
