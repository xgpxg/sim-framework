package com.yao2san.sim.auth.server.busi.user.controller;

import com.yao2san.sim.auth.server.busi.user.service.PermissionManageService;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.PurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simsecurity.request.RolePermissionReq;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("permission")
public class PermissionManageController {
    private final PermissionManageService permissionManageService;

    public PermissionManageController(PermissionManageService permissionManageService) {
        this.permissionManageService = permissionManageService;
    }

    /**
     * 查询权限列表
     */
    @GetMapping
    public ResponseData<ResponsePageData<Permission>> qryPermissionList(Permission permission) {
        return ResponseData.success(permissionManageService.qryPermissionList(permission));
    }

    /**
     * 查询权限树
     */
    @GetMapping("tree")
    public ResponseData qryPermissionTree(Permission permission) {
        return permissionManageService.qryPermissionTree(permission);
    }

    /**
     * 查询权限详情
     */
    @GetMapping("{purviewId}")
    public ResponseData qryPermissionDetail(@PathVariable("purviewId") Long purviewId) {
        return permissionManageService.qryPermissionDetail(purviewId);
    }

    @PostMapping
    public ResponseData addPermission(@Validated @RequestBody PurviewAddOrUpdateReq req) {
        return permissionManageService.addOrUpdatePermission(req);
    }

    /**
     * 查询指定权限已授权/未授权的角色
     */
    @GetMapping("roles")
    public ResponseData<ResponsePageData<Map<String, Object>>> qryPermissionRoles(Permission permission) {
        return ResponseData.success(permissionManageService.qryPermissionRoles(permission));
    }

    @GetMapping("users")
    public ResponseData<ResponsePageData<Map<String, Object>>> qryPermissionUsers(Permission permission) {
        return ResponseData.success(permissionManageService.qryPermissionUsers(permission));
    }

    /**
     * 为角色增加授权
     *
     * @param params 需传递两个对象：角色ID数组和权限ID数组
     */
    @PostMapping("purview")
    public ResponseData<?> purviewForRole(@RequestBody Map<String, Object> params) {
        return permissionManageService.purviewForRole(params);
    }

    @DeleteMapping("{purviewId}")
    public ResponseData<?> deletePermission(@PathVariable Long purviewId) {
        permissionManageService.deletePermission(purviewId);
        return ResponseData.success();
    }

    /**
     * 查询指定角色已授权/未授权的权限
     */
    @GetMapping("rolePermission")
    public ResponseData<ResponsePageData<Permission>> qryRolePermissions(RolePermissionReq req) {
        return ResponseData.success(permissionManageService.qryRolePermissions(req));
    }
}
