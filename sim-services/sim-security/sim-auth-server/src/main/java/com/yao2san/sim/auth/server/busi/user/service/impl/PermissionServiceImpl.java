package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.yao2san.sim.auth.server.busi.user.service.PermissionService;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.mapper.simsecurity.PurviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PermissionServiceImpl  implements PermissionService {

    @Autowired
    private PurviewMapper purviewMapper;
    @Override
    public List<Permission> getUserPermissions(Long userId) {
        Permission permission = new Permission();
        permission.setUserId(userId);
        return this.getUserPermissions(permission);
    }

    @Override
    public List<Permission> getUserPermissions(Permission permission) {
        return purviewMapper.getUserPermissions(permission);
        //return this.sqlSession.selectList("permission.getUserPermissions", permission);
    }

    @Override
    public boolean hasPermissions(Long userId, Long permissionId) {
        Map<String, Object> param = new HashMap<>(2);
        param.put("userId", userId);
        param.put("permissionId", permissionId);
        int count = purviewMapper.hasPermission(param);
        //Integer count = this.sqlSession.selectOne("permission.hasPermission", param);
        return count > 0;
    }

    @Override
    public List<Permission> getInterfacePermissions(String url) {
        return purviewMapper.getInterfacePermissions(url);
        //return this.sqlSession.selectList("permission.getInterfacePermissions", url);
    }

    @Override
    public List<Permission> getInterfacePermissions() {
        return purviewMapper.getInterfacePermissions(null);
        //return this.sqlSession.selectList("permission.getInterfacePermissions");
    }
}
