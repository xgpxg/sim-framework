package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.auth.server.busi.user.service.TenantManagerService;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseCode;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceAuthorizationReq;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceListReq;
import com.yao2san.simservice.bean.simsecurity.response.TenantServiceListRes;
import com.yao2san.simservice.entity.simgateway.ServicePurview;
import com.yao2san.simservice.mapper.simgateway.ServicePurviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TenantManagerServiceImpl extends ServiceBaseImpl<ServicePurviewMapper, ServicePurview> implements TenantManagerService {
    @Autowired
    private ServicePurviewMapper servicePurviewMapper;

    @Override
    public ResponsePageData<TenantServiceListRes> services(TenantServiceListReq req) {
        Page<TenantServiceListRes> page = convertPage(req);
        if ("1".equals(req.getIsAuth())) {
            List<TenantServiceListRes> list = servicePurviewMapper.qryAuthServices(page, req);
            return buildPageResult(page, list);
        } else if ("0".equals(req.getIsAuth())) {
            List<TenantServiceListRes> list = servicePurviewMapper.qryNotAuthServices(page, req);
            return buildPageResult(page, list);
        } else {
            throw new BusiException("不支持的服务授权查询类型(期望值:0 or 1，实际值:" + req.getIsAuth() + ")");
        }
    }

    @Override
    public ResponseData serviceAuthorization(TenantServiceAuthorizationReq req) {
        if (req.getUserIds() == null || req.getUserIds().size() == 0) {
            return ResponseData.error(ResponseCode.ILLEGAL_ARGUMENT, "用户ID不能为空");
        }
        if (req.getServices() == null || req.getServices().size() == 0) {
            return ResponseData.error(ResponseCode.ILLEGAL_ARGUMENT, "服务ID不能为空");
        }
        //新增授权
        if ("1".equals(req.getOption())) {
            req.getUserIds().forEach(userId -> {
                //先取消授权
                servicePurviewMapper.delServicePurview("user", userId, req.getServices());

                req.getServices().forEach(serviceId -> {
                    this.save(new ServicePurview()
                            .setObjectId(userId)
                            .setObjectType("user")
                            .setCreateDate(LocalDateTime.now())
                            .setServiceId(serviceId)
                            .setCreateUser(userId));
                });
            });
            return ResponseData.success(null, "授权成功");
        }
        //解除授权
        else if ("2".equals(req.getOption())) {
            req.getUserIds().forEach(userId -> {
                servicePurviewMapper.delServicePurview("user", userId, req.getServices());
            });
            return ResponseData.success(null, "解除授权成功");
        } else {
            return ResponseData.error("不支持的服务授权查询类型(期望值:1 or 2，实际值:" + req.getOption() + ")");
        }


    }
}
