package com.yao2san.sim.auth.server.busi.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yao2san.sim.auth.server.busi.user.service.UserAuthService;
import com.yao2san.sim.auth.server.utils.EncodeUtil;
import com.yao2san.sim.framework.common.auth.UserPrincipal;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.simservice.entity.simsecurity.UserAuth;
import com.yao2san.simservice.mapper.simsecurity.UserAuthMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAuthServiceImpl implements UserAuthService {
    @Autowired
    private UserAuthMapper userAuthMapper;

    @Override
    public UserPrincipal login(Integer type, String identity, String secret) {
        UserAuth userAuth = userAuthMapper.selectOne(Wrappers.lambdaQuery(UserAuth.class)
                .eq(UserAuth::getType, type)
                .eq(UserAuth::getIdentity, identity));
        if (userAuth == null) {
            throw new BusiException("用户不存在");
        }
        String encodePwd = EncodeUtil.encodePwd(userAuth.getSecret());
        if (StringUtils.equals(encodePwd, secret)) {
            throw new BusiException("密码不正确");
        }
        UserPrincipal user = new UserPrincipal();
        user.setId(userAuth.getUserId());
        user.setNickname("");
        return null;
    }
}
