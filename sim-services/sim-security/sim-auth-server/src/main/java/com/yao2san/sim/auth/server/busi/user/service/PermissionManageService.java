package com.yao2san.sim.auth.server.busi.user.service;


import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.PurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simsecurity.request.RolePermissionReq;

import java.util.List;
import java.util.Map;

public interface PermissionManageService {
    ResponsePageData<Permission> qryPermissionList(Permission permission);

    ResponseData<List<Map<String, Object>>> qryPermissionTree(Permission permission);

    ResponseData<Permission> qryPermissionDetail(Long purviewId);

    ResponseData<?> addOrUpdatePermission(PurviewAddOrUpdateReq req);

    void cachePermission();

    ResponsePageData<Map<String, Object>> qryPermissionRoles(Permission permission);

    ResponsePageData<Map<String, Object>> qryPermissionUsers(Permission permission);

    ResponseData<?> purviewForRole(Map<String,Object> params);

    void deletePermission(Long purviewId);

    ResponsePageData<Permission> qryRolePermissions(RolePermissionReq req);
}