package com.yao2san.sim.auth.client.annotations;

import com.yao2san.sim.auth.client.config.AuthClientConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(AuthClientConfig.class)
public @interface SimAuthClient {
}
