package com.yao2san.sim.auth.client.shiro.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author wxg
 */
@Data
@AllArgsConstructor
public class AccessToken implements AuthenticationToken {
    /**
     * AccessToken
     */
    private String accessToken;

    @Override
    public Object getPrincipal() {
        return accessToken;
    }

    @Override
    public Object getCredentials() {
        return accessToken;
    }
}
