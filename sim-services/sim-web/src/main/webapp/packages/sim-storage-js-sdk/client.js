import axios from 'axios'
import qs from 'qs'

function SimStorageClient(options) {
  this.endpoint = options.endpoint
  this.accessKey = options.accessKey
  this.secretKey = options.secretKey
  this.stsToken = options.stsToken

  return this;
}

const proto = SimStorageClient.prototype;

proto.upload = function (options) {
  //用户应用地址
  const endpoint = this.endpoint
  //文件对象
  const object = options.object
  //文件
  const file = options.file
  //扩展数据
  const ext = options.ext

  const url = endpoint + ('/' + object).replace(/\/+/g, '/')
  let config = {
    url: url,
    headers: {
      'Content-Type': 'multipart/form-data',
      'x-storage-token': this.stsToken
    },
    onUploadProgress: (progressEvent) => {
      if (progressEvent.lengthComputable) {
        if (options.onProgress) {
          options.onProgress(progressEvent);
        }
      }
    }
  }
  const data = new FormData()
  data.append('file', file)
  if (options.ext) {
    //TODO
  }
  return axios.put(url, data, config);
}

proto.signatureUrl = function (options) {
  //用户应用地址
  const endpoint = this.endpoint
  //文件对象
  const object = options.object

  return endpoint + ('/' + object).replace(/\/+/g, '/') + '?' + qs.stringify(options.response)
}

proto.delete = function (options) {
  //用户应用地址
  const endpoint = this.endpoint
  const object = options.object
  const url = endpoint + ('/' + object).replace(/\/+/g, '/')
  let config = {
    url: url,
    headers: {
      'Content-Type': 'multipart/form-data',
      'x-storage-token': this.stsToken
    }
  }
  return axios.delete(url, {}, config);
}
export default SimStorageClient
