import store from '@/store'

//{code:['xxx','yyy'],type:'disabled']} or {code:'xxx',type:'disabled']}
function checkRole(el, binding) {
  const {value} = binding;
  if (!value) {
    return false;
  }
  //const roles = store.getters && store.getters.roles
  const roles = store.getters && store.getters.roles;


  let hasRole = false;
  if (typeof (value) == 'string') {
    hasRole = roles.some(p => p === value);
    if (!hasRole) {
      el.parentNode && el.parentNode.removeChild(el)
    }
  } else if (value instanceof Object) {
    //交集
    let intersection = (value.code || value).filter(v => roles.includes(v))
    let olen = (value.code || value).length
    let len = intersection.length
    if (value instanceof Array || value.code instanceof Array) {
      hasRole = olen > 0 && len > 0 && olen === len
    } else {
      hasRole = roles.some(p => p === value.code);
    }
    if (!hasRole) {
      if (value.type === 'disabled') {
        el.disabled = 'disabled';
        el.className += ' is-disabled';
      } else {
        el.parentNode && el.parentNode.removeChild(el)
      }
    }
  }
}

export default {
  inserted(el, binding) {
    checkRole(el, binding)
  },
  update(el, binding) {
    checkRole(el, binding)
  }
}
