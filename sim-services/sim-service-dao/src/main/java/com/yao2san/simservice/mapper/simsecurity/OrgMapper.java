package com.yao2san.simservice.mapper.simsecurity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsecurity.request.OrgListReq;
import com.yao2san.simservice.bean.simsecurity.response.OrgListRes;
import com.yao2san.simservice.entity.simsecurity.Org;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 组织机构 Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OrgMapper extends BaseMapper<Org> {

    List<OrgListRes> list(Page<OrgListRes> page,@Param("param") OrgListReq param);

    Integer isOrgCodeExist(String orgCode);

    Integer isOrgNameExist(String orgName);
}
