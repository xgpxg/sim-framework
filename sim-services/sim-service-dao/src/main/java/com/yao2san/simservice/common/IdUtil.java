package com.yao2san.simservice.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IdUtil {

    private static CustomerIdGenerator customerIdGenerator;

    @Autowired
    public void setCustomerIdGenerator(CustomerIdGenerator customerIdGenerator) {
        IdUtil.customerIdGenerator = customerIdGenerator;
    }

    public static Long nextId() {
        return customerIdGenerator.nextId();
    }
}
