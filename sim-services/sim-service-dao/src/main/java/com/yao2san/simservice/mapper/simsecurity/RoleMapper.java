package com.yao2san.simservice.mapper.simsecurity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsecurity.request.RoleListReq;
import com.yao2san.simservice.bean.simsecurity.response.UserRoleRes;
import com.yao2san.simservice.bean.simsecurity.response.user.RoleListRes;
import com.yao2san.simservice.entity.simsecurity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 查询用户的角色
     *
     * @param userId 用户ID
     * @return 用户角色列表
     */
    List<UserRoleRes> getUserRoles(Long userId);

    /**
     * 用户是否具有某个角色
     *
     * @param userId 用户ID
     * @param roleId 角色ID
     * @return 0无 1有
     */
    Integer hasRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 查询角色列表
     *
     * @param param 查询参数
     * @return 角色列表
     */
    List<RoleListRes> qryRoleList(Page<RoleListRes> page, @Param("param") RoleListReq param);
}
