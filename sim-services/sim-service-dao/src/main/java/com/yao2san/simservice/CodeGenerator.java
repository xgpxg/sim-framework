/*
package com.yao2san.simservice;




import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

public class CodeGenerator {
    public static void main(String[] args) {
        AutoGenerator mpg = new AutoGenerator();
        GlobalConfig gc = new GlobalConfig();

        String projectPath = "D:\\work\\project\\my\\sim-framework-1.0.2\\gen";

        gc.setOutputDir(projectPath);

        gc.setFileOverride(true);
        gc.setAuthor("wxg");
        gc.setOpen(false);
        gc.setActiveRecord(true);
        gc.setBaseResultMap(false);
        gc.setBaseColumnList(false);
        gc.setDateType(DateType.TIME_PACK);
        mpg.setGlobalConfig(gc);

        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("${SIM_DB_URL}");//oa为我自己的数据库
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("${SIM_DB_UN}");
        dsc.setPassword("${SIM_DB_PW}");
        mpg.setDataSource(dsc);

        PackageConfig pc = new PackageConfig();
        pc.setParent("com.yao2san.simservicedao");
        pc.setServiceImpl("service");
        mpg.setPackageInfo(pc);
        mpg.setTemplate(new TemplateConfig().setXml(null));

        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        strategy.setTablePrefix("");
        strategy.setEntityTableFieldAnnotationEnable(true);
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }


}
*/
