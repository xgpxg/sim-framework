package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yao2san.simservice.entity.simgateway.ServiceGroup;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface ServiceGroupMapper extends BaseMapper<ServiceGroup> {

}
