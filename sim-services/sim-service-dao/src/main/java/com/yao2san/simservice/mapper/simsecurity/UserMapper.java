package com.yao2san.simservice.mapper.simsecurity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.common.UserAuth;
import com.yao2san.simservice.bean.simsecurity.request.UserAddReq;
import com.yao2san.simservice.bean.simsecurity.request.UserListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserRoleListReq;
import com.yao2san.simservice.bean.simsecurity.request.UserUpdateReq;
import com.yao2san.simservice.bean.simsecurity.response.user.UserDetailRes;
import com.yao2san.simservice.bean.simsecurity.response.user.UserListRes;
import com.yao2san.simservice.entity.simsecurity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 根据手机号查询用户
     *
     * @param phone 手机号
     * @return 用户信息
     */
    Map<String, Object> qryUserBaseInfoByPhone(String phone);

    /**
     * 根据用户名、手机号、邮箱等查询用户
     *
     * @param param 用户名、手机号、邮箱
     * @return 用户信息
     */

    Map<String, Object> qryUserBaseInfoByUsername(String param);

    /**
     * 根据用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户信息
     */
    Map<String, Object> qryUserBaseInfoByUserId(Long userId);

    /**
     * 更新用户信息
     *
     * @param param 更新内容
     * @return 受影响行数
     */
    int updateUser(UserUpdateReq param);

    /**
     * 新增用户属性
     *
     * @param param 用户属性内容
     * @return 受影响行数
     */
    int addUserAttr(Map<String, Object> param);

    /**
     * 检查用户属性是否存在
     *
     * @param param 用户属性
     * @return 存在返回1 否则返回0
     */
    int checkUserAttr(Map<String, Object> param);

    /**
     * 更新用户属性
     *
     * @param param 用户属性内容
     * @return 受影响行数
     */
    int updateUserAttr(Map<String, Object> param);

    /**
     * 删除用户属性
     *
     * @param param 用户属性内容
     * @return 受影响行数
     */
    int deleteUserAttr(Map<String, Object> param);

    /**
     * 查询用户属性
     *
     * @param param 包含用户ID和属性编码
     * @return 用户属性列表
     */
    List<Map<String, Object>> qryUserAttr(Map<String, Object> param);

    /**
     * 检查用户授权是否存在
     *
     * @param userAuth 用户授权
     * @return 存在返回1 否则返回0
     */
    int checkAuthExists(UserAuth userAuth);

    List<UserListRes> qryUserList(Page<UserListRes> page, @Param("param") UserListReq param);

    List<Permission> qryUserPermissions(Page<Permission> page, @Param("param") Permission permission);

    List<Permission> qryUserNotPermissions(Page<Permission> page, @Param("param") Permission permission);

    int delPurviewUserRel(List<Map<String, Long>> param);

    int addPurviewUserRel(List<Map<String, Long>> param);

    List<Map<String, Object>> qryUserRoles(Page<Map<String, Object>> page, @Param("param") UserRoleListReq param);

    /**
     * 移除用户角色关系
     *
     * @return
     */
    int delUserRoleRel(List<Map<String, Long>> list);

    /**
     * 新增用户角色关系
     *
     * @return
     */
    int addUserRoleRel(List<Map<String, Long>> list);

    UserDetailRes qryUser(Long id);

    /**
     * @return
     */
    int addUserOrgRel(Long userId,Long orgId);

    int delUserOrgRel(Long userId);
}
