package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simgateway.request.AppListReq;
import com.yao2san.simservice.bean.simgateway.response.AppListRes;
import com.yao2san.simservice.entity.simgateway.RouteApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface RouteAppMapper extends BaseMapper<RouteApp> {

    /**
     * 查询应用列表
     *
     * @param page  分页
     * @param param 查询参数
     * @return 应用列表
     */
    List<AppListRes> list(Page<AppListRes> page, @Param("param") AppListReq param);
}
