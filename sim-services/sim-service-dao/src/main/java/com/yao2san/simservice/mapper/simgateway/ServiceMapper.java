package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simgateway.request.ServiceGroupListReq;
import com.yao2san.simservice.bean.simgateway.request.ServiceListReq;
import com.yao2san.simservice.bean.simgateway.response.ServiceDetailRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceGroupListRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceListRes;
import com.yao2san.simservice.entity.simgateway.Service;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface ServiceMapper extends BaseMapper<Service> {

    /**
     * 查询服务列表
     *
     * @param page  分页
     * @param param 查询参数
     * @return 服务列表
     */
    List<ServiceListRes> list(Page<ServiceListRes> page, @Param("param") ServiceListReq param);

    /**
     * 查询服务详情
     *
     * @param id 服务ID
     * @return 服务详情
     */
    ServiceDetailRes detail(Long id);

    /**
     * 查询服务分组
     *
     * @param param 查询参数
     * @return 分组列表
     */
    List<ServiceGroupListRes> group(ServiceGroupListReq param);
}
