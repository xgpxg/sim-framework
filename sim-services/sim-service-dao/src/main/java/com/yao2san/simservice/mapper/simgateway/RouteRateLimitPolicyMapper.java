package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simgateway.common.RouteRateLimitExtra;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitListReq;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitListRes;
import com.yao2san.simservice.entity.simgateway.RouteRateLimitPolicy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface RouteRateLimitPolicyMapper extends BaseMapper<RouteRateLimitPolicy> {

    List<RouteRateLimitExtra> qryRouteRateLimits(Long routeId);

    List<RouteRateLimitListRes> list(Page<RouteRateLimitListRes> page, @Param("param") RouteRateLimitListReq param);

    RouteRateLimitDetailRes detail(Long id);

}
