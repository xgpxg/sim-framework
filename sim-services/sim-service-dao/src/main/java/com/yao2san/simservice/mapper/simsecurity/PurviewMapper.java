package com.yao2san.simservice.mapper.simsecurity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import com.yao2san.simservice.bean.simsecurity.request.RolePermissionReq;
import com.yao2san.simservice.entity.simsecurity.Purview;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface PurviewMapper extends BaseMapper<Purview> {

    /**
     * 查询用户权限(包括角色授权的和单独给用户授权的)
     *
     * @param permission 查询参数
     * @return 用户权限列表
     */
    List<Permission> getUserPermissions(Permission permission);

    /**
     * 检查用户是否具有某个权限
     *
     * @param param 参数
     * @return 0否 1是
     */
    int hasPermission(Map<String, Object> param);

    /**
     * 查询接口权限
     *
     * @param url 接口地址
     * @return 接口权限列表
     */
    List<Permission> getInterfacePermissions(String url);

    /**
     * 查询权限列表
     *
     * @param page  分页
     * @param param 查询参数
     * @return 权限列表
     */
    List<Permission> qryPermissionList(Page<Permission> page, @Param("param") Permission param);

    /**
     * 查询权限详情
     *
     * @param purviewId 权限ID
     * @return 权限详情
     */
    Permission qryPermissionDetail(Long purviewId);

    /**
     * 根据权限编码查询权限数量（用于判断编码是否重复）
     *
     * @param purviewCode 权限编码
     * @return 0未重复 1重复
     */
    int countPermissionByCode(String purviewCode);

    /**
     * 查询指定权限已授权的角色
     *
     * @param purviewId 权限ID
     * @return 权限已授权的角色
     */
    List<Map<String, Object>> qryPermissionRoles(Page<Map<String, Object>> page,Long purviewId, String filterText);

    /**
     * 查询指定权限未授权的角色
     *
     * @param purviewId 权限ID
     * @return 权限未授权的角色
     */
    List<Map<String, Object>> qryNotPermissionRoles(Page<Map<String, Object>> page,Long purviewId, String filterText);


    /**
     * 查询指定权限已授权/未授权的角色
     *
     * @param purviewId 权限ID
     * @return 权限已授权/未授权的角色
     */
    List<Map<String, Object>> qryAllPermissionRoles(Page<Map<String, Object>> page,Long purviewId, String filterText);

    /**
     * 查询指定权限已授权的用户
     *
     * @param purviewId  权限ID
     * @param filterText
     * @return 权限已授权的用户
     */
    List<Map<String, Object>> qryPermissionUsers(Page<Map<String, Object>> page,Long purviewId, String filterText);

    List<Map<String, Object>> qryNotPermissionUsers(Page<Map<String, Object>> page,Long purviewId, String filterText);

    List<Map<String, Object>> qryAllPermissionUsers(Page<Map<String, Object>> page,Long purviewId, String filterText);

    /**
     * 移除角色授权关系
     *
     * @return 影响行数
     */
    int delPurviewRoleRel(List<Map<String, Long>> list);

    /**
     * 添加角色授权关系
     *
     * @return 影响行数
     */
    int addPurviewRoleRel(List<Map<String, Long>> list);

    /**
     * 查询角色已授权的权限列表
     *
     * @param param 查询参数
     * @return 角色已授权的权限列表
     */
    List<Permission> qryRolePermissions(Page<Permission> page,@Param("param") RolePermissionReq param);

    /**
     * 查询角色未授权的权限列表
     *
     * @param param 查询参数
     * @return 角色未授权的权限列表
     */
    List<Permission> qryRoleNotPermissions(Page<Permission> page,@Param("param")RolePermissionReq param);

    /**
     * 查询角色已授权/未授权的权限列表
     *
     * @param param 查询参数
     * @return 角色已授权/未授权的权限列表
     */
    List<Permission> qryRoleAllPermissions(Page<Permission> page,@Param("param")RolePermissionReq param);
}
