package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteListReq;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteListRes;
import com.yao2san.simservice.entity.simgateway.RouteGray;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface RouteGrayMapper extends BaseMapper<RouteGray> {

    List<GrayRouteListRes> list(Page<GrayRouteListRes> page, @Param("param") GrayRouteListReq param);

    GrayRouteDetailRes detail(Long id);
}
