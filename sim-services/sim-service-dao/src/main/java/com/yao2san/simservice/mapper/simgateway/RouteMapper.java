package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simgateway.common.RouteExtra;
import com.yao2san.simservice.bean.simgateway.request.RouteListReq;
import com.yao2san.simservice.bean.simgateway.response.RouteListRes;
import com.yao2san.simservice.entity.simgateway.Route;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface RouteMapper extends BaseMapper<Route> {

    List<RouteExtra> loadAll();

    List<Map<String, Object>> loadGrayRoutes();


    List<RouteListRes> listPage(Page<RouteListRes> page, @Param("param") RouteListReq param);
}
