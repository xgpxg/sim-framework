package com.yao2san.simservice.common;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义数据库ID主键生成
 */
@Configuration
public class CustomerIdGenerator implements IdentifierGenerator {
    private final static Sequence SEQUENCE = new Sequence(null);

    @Override
    public Long nextId(Object entity) {
        return SEQUENCE.nextId();
    }
    public Long nextId() {
        return SEQUENCE.nextId();
    }
}