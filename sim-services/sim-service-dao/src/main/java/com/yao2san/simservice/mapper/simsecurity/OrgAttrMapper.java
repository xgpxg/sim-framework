package com.yao2san.simservice.mapper.simsecurity;

import com.yao2san.simservice.entity.simsecurity.OrgAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 组织机构属性 Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OrgAttrMapper extends BaseMapper<OrgAttr> {

}
