package com.yao2san.simservice.mapper.simsystem;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsystem.request.DicCodeListReq;
import com.yao2san.simservice.bean.simsystem.response.DicCodeListRes;
import com.yao2san.simservice.entity.simsystem.SysDicCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface SysDicCodeMapper extends BaseMapper<SysDicCode> {

    List<DicCodeListRes> qryDicCode(Page<DicCodeListRes> page, @Param("param") DicCodeListReq param);

}
