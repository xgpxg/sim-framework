package com.yao2san.simservice.mapper.simsystem;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsystem.request.DicItemListReq;
import com.yao2san.simservice.bean.simsystem.response.DicCodeListRes;
import com.yao2san.simservice.bean.simsystem.response.DicItemListRes;
import com.yao2san.simservice.entity.simsystem.SysDicItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典项 Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface SysDicItemMapper extends BaseMapper<SysDicItem> {
    List<DicItemListRes> qryDicItem(Page<DicItemListRes> page, @Param("param") DicItemListReq param);

}
