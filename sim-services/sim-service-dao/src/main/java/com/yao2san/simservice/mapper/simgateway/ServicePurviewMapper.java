package com.yao2san.simservice.mapper.simgateway;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simsecurity.request.TenantServiceListReq;
import com.yao2san.simservice.bean.simsecurity.response.TenantServiceListRes;
import com.yao2san.simservice.entity.simgateway.ServicePurview;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface ServicePurviewMapper extends BaseMapper<ServicePurview> {

    List<TenantServiceListRes> qryAuthServices(Page<TenantServiceListRes> page, @Param("param") TenantServiceListReq param);

    List<TenantServiceListRes> qryNotAuthServices(Page<TenantServiceListRes> page, @Param("param") TenantServiceListReq param);

    int delServicePurview(@Param("objectType") String objectType,@Param("objectId") Long objectId, @Param("services") List<Long> services);
}
