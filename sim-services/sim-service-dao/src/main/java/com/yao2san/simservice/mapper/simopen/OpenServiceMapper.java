package com.yao2san.simservice.mapper.simopen;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yao2san.simservice.entity.simopen.OpenService;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OpenServiceMapper extends BaseMapper<OpenService> {

}
