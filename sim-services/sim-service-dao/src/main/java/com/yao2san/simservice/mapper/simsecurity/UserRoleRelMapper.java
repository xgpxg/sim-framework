package com.yao2san.simservice.mapper.simsecurity;

import com.yao2san.simservice.entity.simsecurity.UserRoleRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface UserRoleRelMapper extends BaseMapper<UserRoleRel> {

}
