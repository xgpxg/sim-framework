package com.yao2san.simservice.mapper.simsystem;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yao2san.simservice.entity.simsystem.SysConfigApp;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface SysConfigAppMapper extends BaseMapper<SysConfigApp> {

}
