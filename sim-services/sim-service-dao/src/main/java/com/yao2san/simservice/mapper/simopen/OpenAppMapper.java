package com.yao2san.simservice.mapper.simopen;

import com.yao2san.simservice.bean.simopen.request.OpenAppListReq;
import com.yao2san.simservice.bean.simopen.response.OpenAppDetailRes;
import com.yao2san.simservice.bean.simopen.response.OpenAppListRes;
import com.yao2san.simservice.entity.simopen.OpenApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OpenAppMapper extends BaseMapper<OpenApp> {

    /**
     * 查询应用列表
     *
     * @param param 查询参数
     * @return 应用列表
     */
    List<OpenAppListRes> list(OpenAppListReq param);

    /**
     * 查询应用详情
     *
     * @param id 应用ID
     * @return 应用详情
     */
    OpenAppDetailRes detail(Long id);
}
