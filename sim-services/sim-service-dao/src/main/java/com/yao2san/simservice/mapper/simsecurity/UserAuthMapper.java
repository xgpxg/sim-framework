package com.yao2san.simservice.mapper.simsecurity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yao2san.simservice.entity.simsecurity.UserAuth;


/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface UserAuthMapper extends BaseMapper<UserAuth> {

}
