package com.yao2san.simservice.mapper.simsecurity;

import com.yao2san.simservice.entity.simsecurity.UserPurviewRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface UserPurviewRelMapper extends BaseMapper<UserPurviewRel> {

}
