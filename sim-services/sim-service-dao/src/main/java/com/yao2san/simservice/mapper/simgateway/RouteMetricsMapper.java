package com.yao2san.simservice.mapper.simgateway;

import com.yao2san.simservice.entity.simgateway.RouteMetrics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface RouteMetricsMapper extends BaseMapper<RouteMetrics> {

}
