package com.yao2san.simservice.mapper.simopen;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yao2san.simservice.entity.simopen.OpenServiceGroup;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OpenServiceGroupMapper extends BaseMapper<OpenServiceGroup> {

}
