package com.yao2san.simservice.mapper.simsecurity;

import com.yao2san.simservice.entity.simsecurity.OrgAttrCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 组织机构属性定义 Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OrgAttrCodeMapper extends BaseMapper<OrgAttrCode> {

}
