package com.yao2san.simservice.mapper.simsystem;

import com.yao2san.simservice.entity.simsystem.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
