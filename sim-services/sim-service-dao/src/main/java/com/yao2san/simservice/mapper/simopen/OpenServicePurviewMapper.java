package com.yao2san.simservice.mapper.simopen;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.simservice.bean.simopen.request.ServiceListReq;
import com.yao2san.simservice.bean.simopen.request.ServicePurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simopen.request.ServicePurviewReq;
import com.yao2san.simservice.bean.simopen.response.ServicePurviewRes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yao2san.simservice.entity.simopen.OpenServicePurview;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface OpenServicePurviewMapper extends BaseMapper<OpenServicePurview> {

    /**
     * 查询已授权的服务
     *
     * @param page  分页参数
     * @param param 查询参数
     * @return 服务列表
     */
    List<ServicePurviewRes> qryAuthServices(Page<ServicePurviewRes> page, @Param("param") ServiceListReq param);

    /**
     * 查询未授权的服务
     *
     * @param page  分页参数
     * @param param 查询参数
     * @return 服务列表
     */
    List<ServicePurviewRes> qryNotAuthServices(Page<ServicePurviewRes> page, @Param("param") ServiceListReq param);

    /**
     * 新增服务授权
     *
     * @param param 授权参数
     * @return 影响行数
     */
    int addServicePurview(ServicePurviewAddOrUpdateReq param);

    /**
     * 取消服务授权
     *
     * @param param 授权参数
     * @return 影响行数
     */
    int delServicePurview(ServicePurviewAddOrUpdateReq param);
}
