package com.yao2san.simservice.mapper.simsecurity;

import com.yao2san.simservice.entity.simsecurity.UserOrgRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户组织机构 Mapper 接口
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
public interface UserOrgRelMapper extends BaseMapper<UserOrgRel> {

}
