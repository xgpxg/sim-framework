package com.yao2san.sim.storage.client.core.uploader;

import com.yao2san.sim.storage.client.core.enums.StorageType;
import com.yao2san.sim.storage.client.core.event.UploadResult;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class FtpUploader extends AbstractUploader {

    @Override
    public UploadResult upload(InputStream stream, String dest, UploadArgs args) throws IOException {
        throw new RuntimeException("Sorry, the ftp uploader and downloader not supported yet");
    }

    @Override
    public String getBasePath() {
        throw new RuntimeException("Sorry, the ftp uploader and downloader not supported yet");
    }

    @Override
    public String getName() {
        return StorageType.FTP.name();
    }
}
