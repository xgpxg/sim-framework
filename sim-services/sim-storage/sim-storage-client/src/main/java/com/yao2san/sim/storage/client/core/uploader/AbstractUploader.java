package com.yao2san.sim.storage.client.core.uploader;


import com.yao2san.sim.storage.client.core.event.UploadResult;
import com.yao2san.sim.storage.client.exception.UploadErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;

@Slf4j
public abstract class AbstractUploader implements Uploader {


    @Override
    public UploadResult upload(String file, String dest) throws IOException, UploadErrorException {
        return upload(file, dest, null);
    }


    @Override
    public UploadResult upload(File file, String dest) throws IOException, UploadErrorException {
        return upload(file, dest, null);
    }


    @Override
    public UploadResult upload(MultipartFile file, String dest) throws IOException, UploadErrorException {
        return upload(file.getInputStream(), dest, null);
    }


    @Override
    public UploadResult upload(InputStream stream, String dest) throws IOException, UploadErrorException {
        return upload(stream, dest, null);
    }


    @Override
    public UploadResult upload(String file, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return upload(new File(file), dest, args);
    }


    @Override
    public UploadResult upload(File file, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return upload(Files.newInputStream(file.toPath()), dest, args);
    }


    @Override
    public UploadResult upload(MultipartFile file, String dest, UploadArgs args) throws IOException, UploadErrorException {
        return upload(file.getInputStream(), dest, args);
    }


    @Override
    public abstract UploadResult upload(InputStream stream, String dest, UploadArgs args) throws IOException, UploadErrorException;

    @Override
    public UploadResult upload(URL url, String dest) throws UploadErrorException {
        try {
            InputStream inputStream = url.openStream();
            return upload(inputStream, dest);
        } catch (IOException | UploadErrorException e) {
            log.error("internet file copy error", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public abstract String getBasePath();
}
