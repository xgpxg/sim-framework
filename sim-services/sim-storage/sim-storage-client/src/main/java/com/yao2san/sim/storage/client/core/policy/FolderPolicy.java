package com.yao2san.sim.storage.client.core.policy;

/**
 * 文件夹生成策略
 **/
public interface FolderPolicy {

    /**
     * 生成文件夹
     */
    String create();

}
