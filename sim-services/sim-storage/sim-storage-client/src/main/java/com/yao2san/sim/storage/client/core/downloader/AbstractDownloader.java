package com.yao2san.sim.storage.client.core.downloader;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class AbstractDownloader implements Downloader {
    @Override
    public void download(String object, String dest) throws IOException {
        download(object, Files.newOutputStream(Paths.get(dest)));
    }


    @Override
    public abstract void download(String object, OutputStream out) throws IOException;
}
