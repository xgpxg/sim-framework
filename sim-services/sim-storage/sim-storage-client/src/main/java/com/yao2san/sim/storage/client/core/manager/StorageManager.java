package com.yao2san.sim.storage.client.core.manager;

import com.yao2san.sim.storage.client.core.auth.Credentials;

import java.io.IOException;
import java.util.List;

public interface StorageManager {
    /**
     * 文件是否存在
     *
     * @param object 唯一名称
     */
    boolean exist(String object);

    /**
     * 删除文件
     *
     * @param object 唯一名称
     */
    void delete(String object) throws IOException;

    /**
     * 重命名文件
     *
     * @param source 原文件名
     * @param target 目标文件名
     */
    void rename(String source, String target) throws IOException;

    /**
     * 复制文件
     *
     * @param source     原文件名
     * @param target     目标文件名
     * @param keepSource 是否保留源文件
     */
    void copy(String source, String target, boolean keepSource) throws IOException;

    /**
     * 列出文件
     *
     * @param prefix 前缀
     */
    List<StorageObject> list(String prefix) throws IOException;

    /**
     * 列出文件
     *
     * @param prefix     前缀
     * @param startAfter 从指定位置开始
     */
    List<StorageObject> list(String prefix, String startAfter) throws IOException;

    /**
     * 获取Manager的唯一名称标识,该名称全局唯一
     */
    String getName();

    /**
     * 生成临时凭证
     *
     * @param durationSeconds 有效时间(秒)
     */
    Credentials credentials(long durationSeconds);
}
