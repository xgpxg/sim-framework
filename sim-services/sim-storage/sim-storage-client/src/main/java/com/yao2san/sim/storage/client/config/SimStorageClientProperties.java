package com.yao2san.sim.storage.client.config;

import com.yao2san.sim.storage.client.core.enums.StorageType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "sim.storage")
@Data
public class SimStorageClientProperties {
    private StorageType type;
    private boolean enable;
    private String customType;


}
