package com.yao2san.sim.storage.client.core.policy;

/**
 * @author wxg
 **/
public class EmptyFileRenamePolicy implements FileRenamePolicy{
    @Override
    public String rename(String name) {
        return name;
    }
}
