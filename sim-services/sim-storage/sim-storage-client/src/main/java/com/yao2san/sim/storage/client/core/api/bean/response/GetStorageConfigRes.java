package com.yao2san.sim.storage.client.core.api.bean.response;

import com.yao2san.sim.storage.client.core.auth.Credentials;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wxg
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class GetStorageConfigRes extends StorageConfig {
    private Credentials credentials;
}
