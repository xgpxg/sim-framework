package com.yao2san.sim.storage.client.annotations;

import com.yao2san.sim.storage.client.config.SimStorageClientConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author wxg
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Documented
@Import(SimStorageClientConfig.class)
public @interface SimStorageClient {
}
