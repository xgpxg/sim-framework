package com.yao2san.sim.storage.client.core.api.bean.response;

import com.yao2san.sim.storage.client.core.enums.StorageType;
import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class StorageConfig {
    private String endpoint;
    private String region;
    private String bucket;
    private String path;
    private StorageType storageType;
    private String openId;
}
