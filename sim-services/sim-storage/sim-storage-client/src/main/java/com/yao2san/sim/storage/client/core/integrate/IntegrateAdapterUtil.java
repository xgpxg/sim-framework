package com.yao2san.sim.storage.client.core.integrate;

import com.aliyun.oss.OSSClientBuilder;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.region.Region;
import com.yao2san.sim.storage.client.config.AliOssProperties;
import com.yao2san.sim.storage.client.config.LocalProperties;
import com.yao2san.sim.storage.client.config.MinioProperties;
import com.yao2san.sim.storage.client.config.TencentCosProperties;
import io.minio.MinioClient;

import static com.yao2san.sim.storage.client.core.enums.StorageType.*;


/**
 * @author wxg
 **/
public class IntegrateAdapterUtil {
    public static Object getClient(IntegrateConfig integrateConfig) {
        //Minio client
        if (integrateConfig.getStorageType().equals(MINIO)) {
            MinioProperties minioProperties = (MinioProperties) getProperties(integrateConfig);
            return MinioClient.builder()
                    .endpoint(minioProperties.getEndpoint())
                    .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
                    .build();
        }
        //Ali OSS client
        if (integrateConfig.getStorageType().equals(ALI_OSS)) {
            AliOssProperties aliOssProperties = (AliOssProperties) getProperties(integrateConfig);
            return new OSSClientBuilder()
                    .build(aliOssProperties.getEndpoint()
                            , aliOssProperties.getAccessKey()
                            , aliOssProperties.getSecretKey());

        }
        if (integrateConfig.getStorageType().equals(TENCENT_COS)) {
            TencentCosProperties tencentCosProperties = (TencentCosProperties)getProperties(integrateConfig);
            COSCredentials cred = new BasicCOSCredentials(tencentCosProperties.getAccessKey(), tencentCosProperties.getSecretKey());
            Region region = new Region(tencentCosProperties.getRegion());
            ClientConfig clientConfig = new ClientConfig(region);
            clientConfig.setHttpProtocol(HttpProtocol.https);
            return new COSClient(cred, clientConfig);
        }
        if (integrateConfig.getStorageType().equals(QI_NIU)) {
            //TODO
        }
        throw new UnsupportedOperationException(integrateConfig.getStorageType().name());
    }

    public static Object getProperties(IntegrateConfig integrateConfig) {
        if (integrateConfig.getStorageType().equals(LOCAL)) {
            LocalProperties localProperties = new LocalProperties();
            localProperties.setEndpoint(integrateConfig.getEndpoint());
            localProperties.setRegion(integrateConfig.getRegion());
            localProperties.setBucket(integrateConfig.getBucket());
            localProperties.setPath(integrateConfig.getPath());
            localProperties.setPrefix(integrateConfig.getPrefix());
            localProperties.setAccessKey(integrateConfig.getAk());
            localProperties.setSecretKey(integrateConfig.getSk());
            return localProperties;
        }
        if (integrateConfig.getStorageType().equals(MINIO)) {
            MinioProperties minioProperties = new MinioProperties();
            minioProperties.setEndpoint(integrateConfig.getEndpoint());
            minioProperties.setBucket(integrateConfig.getBucket());
            minioProperties.setPath(integrateConfig.getPath());
            minioProperties.setAccessKey(integrateConfig.getAk());
            minioProperties.setSecretKey(integrateConfig.getSk());
            minioProperties.setSecure(false);
            return minioProperties;
        }
        if (integrateConfig.getStorageType().equals(ALI_OSS)) {
            AliOssProperties aliOssProperties = new AliOssProperties();
            aliOssProperties.setEndpoint(integrateConfig.getEndpoint());
            aliOssProperties.setBucket(integrateConfig.getBucket());
            aliOssProperties.setPath(integrateConfig.getPath());
            aliOssProperties.setAccessKey(integrateConfig.getAk());
            aliOssProperties.setSecretKey(integrateConfig.getSk());
            aliOssProperties.setRegion(integrateConfig.getRegion());
            aliOssProperties.setRoleArn(integrateConfig.getRoleArn());
            return aliOssProperties;
        }
        if (integrateConfig.getStorageType().equals(TENCENT_COS)) {
            TencentCosProperties tencentCosProperties = new TencentCosProperties();
            tencentCosProperties.setEndpoint(integrateConfig.getEndpoint());
            tencentCosProperties.setBucket(integrateConfig.getBucket());
            tencentCosProperties.setPath(integrateConfig.getPath());
            tencentCosProperties.setAccessKey(integrateConfig.getAk());
            tencentCosProperties.setSecretKey(integrateConfig.getSk());
            tencentCosProperties.setRegion(integrateConfig.getRegion());
            return tencentCosProperties;
        }
        throw new UnsupportedOperationException("Unsupported storage type:" + integrateConfig.getStorageType().name());
    }
}
