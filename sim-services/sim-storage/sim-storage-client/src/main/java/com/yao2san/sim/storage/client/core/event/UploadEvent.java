package com.yao2san.sim.storage.client.core.event;

import org.springframework.context.ApplicationEvent;


public class UploadEvent extends ApplicationEvent {


    public UploadEvent(UploadResult source) {
        super(source);
    }

}