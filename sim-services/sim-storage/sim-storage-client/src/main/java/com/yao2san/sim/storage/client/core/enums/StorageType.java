package com.yao2san.sim.storage.client.core.enums;

/**
 * 存储类型
 */
public enum StorageType {
    /**
     * 本地存储
     */
    LOCAL,
    /**
     * FTP存储
     */
    FTP,
    /**
     * MINIO存储
     */
    MINIO,
    /**
     * 阿里OSS
     */
    ALI_OSS,
    /**
     * 腾讯COS
     */
    TENCENT_COS,
    /**
     * 七牛对象存储
     */
    QI_NIU,
    /**
     * 集成存储
     */
    INTEGRATE,
    /**
     * 自定义存储
     */
    CUSTOM
}