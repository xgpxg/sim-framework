package com.yao2san.sim.storage.client.core.integrate;

import com.yao2san.sim.storage.client.core.enums.StorageType;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author wxg
 * <p>
 * 文件上传通知参数
 **/
@Data
public class UploadNotify {

    /**
     * 是否成功
     */
    private Boolean ok;
    /**
     * 错误信息
     */
    private String message;
    /**
     * 通知内容
     */
    private Content content;
    /**
     * 元数据
     */
    private Meta meta;

    @Data
    public static class Meta {
        /**
         * 通知类型
         */
        private NotifyType notifyType;
        /**
         * 存储类型
         */
        private StorageType storageType;
        /**
         * 存储应用ID
         */
        private String openId;
    }

    @Data
    public static class Content {
        /**
         * 对象名称
         */
        private String object;
        /**
         * 对象ID,文件上传并通知storage-server后返回
         */
        private Long objectId;
        /**
         * 原始文件名称
         */
        private String originalName;
        /**
         * 文件类型
         */
        private String contentType;
        /**
         * 后缀名,文件无后缀时为空
         */
        private String suffix;
        /**
         * 文件大小
         */
        private long size;
        /**
         * 文件ulr,当存储类型为第三方存储时为文件地址,是否可直接访问取决于第三方存储对存储桶的权限配置是否为可公开访问的
         */
        private String url;

        private Timestamp lastModified;
    }

    public enum NotifyType {
        /**
         * 上传通知
         */
        UPLOAD,
        /**
         * 下载通知
         */
        DOWNLOAD,
        /**
         * 删除文件通知
         */
        DELETE
    }
}
