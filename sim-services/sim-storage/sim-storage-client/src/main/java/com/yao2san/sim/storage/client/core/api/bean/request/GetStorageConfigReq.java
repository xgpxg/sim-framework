package com.yao2san.sim.storage.client.core.api.bean.request;

import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class GetStorageConfigReq {
    private String openId;
    private String secretKey;
    private String fileName;

}
