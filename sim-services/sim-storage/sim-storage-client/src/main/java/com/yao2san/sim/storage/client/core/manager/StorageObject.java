package com.yao2san.sim.storage.client.core.manager;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StorageObject {
    private String object;
    private Boolean isDir;
    private String storageType;
    private Long size;
    private Long lastModified;
    private String url;

}
