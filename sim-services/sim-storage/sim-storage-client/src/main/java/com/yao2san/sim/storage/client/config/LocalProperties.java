package com.yao2san.sim.storage.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sim.storage.local")
public class LocalProperties {
    /**
     * 客户端地址,即应用服务地址
     */
    private String endpoint;

    private String region;
    /**
     * 客户端访问前缀, 如配置为file，则可通过http://{endpoint}/file/xxx访问文件
     */
    private String prefix;
    /**
     * 本地存储暂未使用
     */
    private String bucket;
    /**
     * 文件存储跟路径
     */
    private String path;
    /**
     * 自定义key
     */
    private String accessKey;
    /**
     * 自定义密钥
     */
    private String secretKey;
}
