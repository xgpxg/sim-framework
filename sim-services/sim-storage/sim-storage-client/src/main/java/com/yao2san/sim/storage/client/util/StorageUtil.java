package com.yao2san.sim.storage.client.util;

public class StorageUtil {

    private final static String[] SPECIAL_CHARS = {"../", "./", ".\\", ".\\\\"};

    public static void checkObjectName(String... objects) {
        if (objects == null) {
            return;
        }
        for (String object : objects) {
            if (object != null) {
                for (String specialChar : SPECIAL_CHARS) {
                    if (object.contains(specialChar)) {
                        throw new IllegalArgumentException("Illegal parameter:" + specialChar);
                    }
                }
            }
        }
    }
}
