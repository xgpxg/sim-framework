package com.yao2san.sim.storage.client.exception;

public class StorageClientException extends RuntimeException{
    public StorageClientException() {
        super();
    }

    public StorageClientException(String message) {
        super(message);
    }

    public StorageClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageClientException(Throwable cause) {
        super(cause);
    }

    protected StorageClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
