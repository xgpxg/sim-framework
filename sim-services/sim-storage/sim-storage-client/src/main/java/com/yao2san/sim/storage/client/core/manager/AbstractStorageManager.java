package com.yao2san.sim.storage.client.core.manager;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.FIFOCache;
import com.yao2san.sim.storage.client.core.auth.Credentials;

public abstract class AbstractStorageManager implements StorageManager{

    protected static final  FIFOCache<String, Credentials> CREDENTIALS_CACHE = CacheUtil.newFIFOCache(100, 1000 * 60);

    public Credentials credentials(long durationSeconds) {
        return null;
    }
}
