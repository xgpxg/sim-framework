package com.yao2san.sim.storage.client.aspect;

import com.yao2san.sim.storage.client.core.policy.SecurityPolicy;
import com.yao2san.sim.storage.client.exception.NoPermissionException;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author wxg
 **/
@Aspect
@Configuration
@ConditionalOnProperty(prefix = "sim.storage", name = "enable", havingValue = "true")
public class SecurityAspect {
    private final SecurityPolicy securityPolicy;

    public SecurityAspect(SecurityPolicy securityPolicy) {
        this.securityPolicy = securityPolicy;
    }

    @Before("execution(* com.yao2san.sim.storage.client.core.uploader.Uploader..*(..))" +
            "|| execution(* com.yao2san.sim.storage.client.core.downloader.Downloader..*(..)) " +
            "|| execution(* com.yao2san.sim.storage.client.core.manager.StorageManager..*(..)) ")
    public void checkPermission() {
        if (!securityPolicy.pass()) {
            throw new NoPermissionException("No permission");
        }
    }
}
