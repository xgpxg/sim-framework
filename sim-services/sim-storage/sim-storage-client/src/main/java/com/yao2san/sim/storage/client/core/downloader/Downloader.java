package com.yao2san.sim.storage.client.core.downloader;

import java.io.IOException;
import java.io.OutputStream;

public interface Downloader {
    /**
     * 下载文件
     *
     * @param object 文件对象
     * @param dest   要保存的文件(含路径)
     */
    void download(String object, String dest) throws IOException;

    /**
     * 下载文件
     *
     * @param object 文件对象
     * @param out    输出流
     */
    void download(String object, OutputStream out) throws IOException;


    /**
     * 获取文件下载URL
     *
     * @param object 文件对象
     * @return 文件下载URL
     */
    String url(String object);

    /**
     * 获取文件下载URL
     *
     * @param object    文件对象
     * @param preSigned 是否使用预签名
     * @return 文件下载URL
     */
    String url(String object, boolean preSigned);

    String getName();
}
