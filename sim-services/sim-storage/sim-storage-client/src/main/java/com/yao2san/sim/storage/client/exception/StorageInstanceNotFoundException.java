package com.yao2san.sim.storage.client.exception;

/**
 * @author wxg
 **/
public class StorageInstanceNotFoundException extends RuntimeException {
    public StorageInstanceNotFoundException() {
        super();
    }

    public StorageInstanceNotFoundException(String message) {
        super(message);
    }

    public StorageInstanceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageInstanceNotFoundException(Throwable cause) {
        super(cause);
    }

    protected StorageInstanceNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
