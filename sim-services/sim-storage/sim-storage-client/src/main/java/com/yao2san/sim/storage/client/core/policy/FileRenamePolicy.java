package com.yao2san.sim.storage.client.core.policy;

public interface FileRenamePolicy {
    String rename(String name);
}
