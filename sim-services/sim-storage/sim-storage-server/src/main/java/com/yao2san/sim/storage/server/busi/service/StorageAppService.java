package com.yao2san.sim.storage.server.busi.service;

import com.yao2san.sim.storage.server.busi.bean.request.StorageAppAddReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppDeleteReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppUpdateReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageListReq;
import com.yao2san.sim.storage.server.busi.bean.response.StorageAppDetailRes;
import com.yao2san.sim.storage.server.busi.bean.response.StorageAppListRes;

import java.util.List;

/**
 * @author wxg
 **/
public interface StorageAppService {
    List<StorageAppListRes> list(StorageListReq req);

    void add(StorageAppAddReq req);

    void update(StorageAppUpdateReq req);

    void delete(StorageAppDeleteReq req);

    StorageAppDetailRes detail(Long storageAppId);
}
