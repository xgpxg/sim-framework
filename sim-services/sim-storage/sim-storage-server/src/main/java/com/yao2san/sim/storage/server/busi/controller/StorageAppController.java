package com.yao2san.sim.storage.server.busi.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppAddReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppDeleteReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppUpdateReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageListReq;
import com.yao2san.sim.storage.server.busi.bean.response.StorageAppDetailRes;
import com.yao2san.sim.storage.server.busi.bean.response.StorageAppListRes;
import com.yao2san.sim.storage.server.busi.service.StorageAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("storageApp")
public class StorageAppController {
    @Autowired
    private StorageAppService storageAppService;

    /**
     * 添加应用
     */
    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated StorageAppAddReq req) {
        storageAppService.add(req);
        return ResponseData.success();
    }

    @GetMapping
    public ResponseData<List<StorageAppListRes>> list(StorageListReq req) {
        return ResponseData.success(storageAppService.list(req));
    }

    @GetMapping("{storageAppId}")
    public ResponseData<StorageAppDetailRes> detail(@PathVariable("storageAppId") Long storageAppId) {
        return ResponseData.success(storageAppService.detail(storageAppId));
    }

    @DeleteMapping
    public ResponseData<Void> delete(StorageAppDeleteReq req) {
        storageAppService.delete(req);
        return ResponseData.success();
    }

    @PatchMapping
    public ResponseData<Void> update(@RequestBody StorageAppUpdateReq req) {
        storageAppService.update(req);
        return ResponseData.success();
    }
}
