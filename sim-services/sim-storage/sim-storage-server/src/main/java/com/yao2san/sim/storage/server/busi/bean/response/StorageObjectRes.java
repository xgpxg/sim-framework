package com.yao2san.sim.storage.server.busi.bean.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class StorageObjectRes extends BaseBean {
    private Long storageObjectId;
    private String object;
    private String originalName;
    private String size;
    private String contentType;
    private String suffix;
    private String openId;
    private String storageType;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastModified;
}
