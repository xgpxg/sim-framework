package com.yao2san.sim.storage.server.busi.service.impl;

import com.yao2san.sim.framework.utils.CommonUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppAddReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppDeleteReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageAppUpdateReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageListReq;
import com.yao2san.sim.storage.server.busi.bean.response.StorageAppDetailRes;
import com.yao2san.sim.storage.server.busi.bean.response.StorageAppListRes;
import com.yao2san.sim.storage.server.busi.service.StorageAppService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @author wxg
 **/
@Service
public class StorageAppServiceImpl extends BaseServiceImpl implements StorageAppService {
    @Override
    public List<StorageAppListRes> list(StorageListReq req) {
        return this.sqlSession.selectList("storageApp.list", req);
    }

    @Override
    @Transactional
    public void add(StorageAppAddReq req) {
        req.setOpenId(makeKey());
        req.setSecretKey(makeKey());
        this.sqlSession.insert("storageApp.add", req);
    }

    @Override
    @Transactional
    public void update(StorageAppUpdateReq req) {
        this.sqlSession.update("storageApp.update", req);
    }

    @Override
    @Transactional
    public void delete(StorageAppDeleteReq req) {
        int delete = this.sqlSession.delete("storageApp.delete", req);
        if (delete != 1) {
            throw new BusiException("删除失败");
        }
    }

    @Override
    public StorageAppDetailRes detail(Long storageAppId) {
        return this.sqlSession.selectOne("storageApp.detail", storageAppId);
    }

    private String makeKey() {
        return CommonUtil.md5(UUID.randomUUID().toString());
    }
}
