package com.yao2san.sim.storage.server.busi.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.storage.server.busi.bean.request.FileUploadReq;
import com.yao2san.sim.storage.server.busi.bean.request.StorageObjectReq;
import com.yao2san.sim.storage.server.busi.bean.response.StorageObjectRes;
import com.yao2san.sim.storage.client.core.api.bean.response.GetStorageConfigRes;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wxg
 **/
public interface StorageManagerService {

    GetStorageConfigRes getConfig(String openId);

    PageInfo<StorageObjectRes> listObject(StorageObjectReq req);

    void upload(MultipartFile file, FileUploadReq req);
}
