package com.yao2san.sim.storage.server.config;

import com.yao2san.sim.framework.web.mybatis.sharding.ShardingStrategy;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Date;

public class MyTableShardingStrategy implements ShardingStrategy {
    @Override
    public String getTableName(String tableName) {
        return tableName+"_"+ DateFormatUtils.format(new Date(),"yyyyMMdd");
    }
}
