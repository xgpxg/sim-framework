package com.yao2san.sim.storage.server.busi.service.impl;

import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.storage.server.busi.bean.response.StorageConfigRes;
import com.yao2san.sim.storage.server.busi.service.OpenApiService;
import com.yao2san.sim.storage.client.core.integrate.UploadNotify;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wxg
 **/
@Service
@Slf4j
public class OpenApiServiceImpl extends BaseServiceImpl implements OpenApiService {
    @Override
    public StorageConfigRes getConfig(String openId, String secretKey) {
        validate(openId, secretKey);
        StorageConfigRes config = this.sqlSession.selectOne("openApi.qryStorageConfig", openId);
        if (config == null) {
            throw new RuntimeException("Storage app not found,app id:" + openId);
        }
        return config;
    }


    private void validate(String openId, String secretKey) {
        if (StringUtils.isEmpty(openId)) {
            throw new RuntimeException("App id can not be empty");
        }
        //TODO
    }

    @Override
    @Transactional
    public UploadNotify notify(UploadNotify req) {
        if (req.getMeta().getNotifyType() == UploadNotify.NotifyType.UPLOAD) {
            String originalName = req.getContent().getOriginalName();
            req.getContent().setSuffix(FilenameUtils.getExtension(originalName));
            this.sqlSession.insert("openApi.saveObject", req);
            return req;
        } else if (req.getMeta().getNotifyType() == UploadNotify.NotifyType.DELETE) {
            this.sqlSession.delete("openApi.deleteObject", req);
            return null;
        }
        return null;
    }

}
