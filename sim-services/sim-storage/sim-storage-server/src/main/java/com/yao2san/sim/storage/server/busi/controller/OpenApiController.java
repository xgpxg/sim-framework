package com.yao2san.sim.storage.server.busi.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.storage.server.busi.bean.response.StorageConfigRes;
import com.yao2san.sim.storage.server.busi.service.OpenApiService;
import com.yao2san.sim.storage.client.core.integrate.UploadNotify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wxg
 * <p>
 * 客户端调用接口
 **/
@RequestMapping("storage/server")
@RestController
public class OpenApiController {
    @Autowired
    private OpenApiService openApiService;

    /**
     * 获取存储配置,含临时凭证
     */
    @GetMapping("config")
    public ResponseData<StorageConfigRes> getConfig(String openId, String secretKey) {
        return ResponseData.success(openApiService.getConfig(openId, secretKey));
    }

    /**
     * 上传结果通知
     */
    @PostMapping("notify")
    public ResponseData<UploadNotify> notify(@RequestBody UploadNotify req) {
        return ResponseData.success(openApiService.notify(req));
    }
}
