package com.yao2san;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@Slf4j
@RestController
public class SimStorageServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimStorageServerApplication.class, args);
        log.info("sim-storage-server start success!");
    }
}
