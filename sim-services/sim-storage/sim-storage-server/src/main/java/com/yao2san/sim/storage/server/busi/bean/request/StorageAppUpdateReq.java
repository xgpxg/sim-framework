package com.yao2san.sim.storage.server.busi.bean.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
public class StorageAppUpdateReq extends BaseBean {
    @NotNull(message = "storageAppId不能为空")
    private Long storageAppId;
    private String openId;
    private String secretKey;
    private String appName;
    private String storageType;
    private String endpoint;
    private String bucket;
    private String region;
    private String ak;
    private String sk;
    private String path;

}
