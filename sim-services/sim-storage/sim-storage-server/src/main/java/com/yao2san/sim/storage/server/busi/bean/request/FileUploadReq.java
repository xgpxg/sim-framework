package com.yao2san.sim.storage.server.busi.bean.request;

import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class FileUploadReq {
    /**
     * 存储平台应用的openId
     */
    private String openId;
    /**
     * 上传的目录
     */
    private String dir;
}
