package com.yao2san.sim.gateway.server.route.gray;

import com.yao2san.sim.gateway.server.config.GatewayProperties;

import java.util.List;
import java.util.Map;
@Deprecated
public interface IGrayRouteLocator {
    Map<String, GatewayProperties.GrayRoute> locateGrayRoutes();
    List<GrayRoute> getAll();
}
