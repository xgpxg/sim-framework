package com.yao2san.sim.gateway.server.api.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simgateway.request.ServiceAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.ServiceListReq;
import com.yao2san.simservice.bean.simgateway.response.ServiceDetailRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceGroupListRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceListRes;

import java.util.List;

public interface ServiceManagerService {
    ResponsePageData<ServiceListRes> list(ServiceListReq req);
    ResponseData<Void> add(ServiceAddOrUpdateReq req);
    ResponseData<ServiceDetailRes> detail(Long req);
    ResponseData<Void> update(ServiceAddOrUpdateReq req);
    ResponseData<Void> delete(Long serviceId);
    ResponseData<Void> online(Long serviceId);
    ResponseData<Void> offline(Long serviceId);
    ResponseData<List<ServiceGroupListRes>> group();
}
