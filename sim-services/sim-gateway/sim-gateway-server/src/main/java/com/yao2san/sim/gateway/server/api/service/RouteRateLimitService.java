package com.yao2san.sim.gateway.server.api.service;

import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitListReq;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitListRes;

public interface RouteRateLimitService {
    void refresh();

    void refresh(Long routeId);

    RouteRateLimitDetailRes detail(Long routeRateLimitPolicyId);

    ResponsePageData<RouteRateLimitListRes> list(RouteRateLimitListReq req);

    void add(RouteRateLimitAddOrUpdateReq req);

    void delete(Long rateLimitPolicyId);

    void update(RouteRateLimitAddOrUpdateReq req);
    void updateStatus(RouteRateLimitAddOrUpdateReq req);
}
