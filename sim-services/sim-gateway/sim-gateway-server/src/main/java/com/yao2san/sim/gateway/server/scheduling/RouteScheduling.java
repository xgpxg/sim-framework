package com.yao2san.sim.gateway.server.scheduling;

import com.yao2san.sim.gateway.server.route.core.RouteHelper;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RouteScheduling {
    @Scheduled(fixedRateString = "#{${zuul.dynamic-route.refresh-interval-seconds:30}*1000}")
    public void refreshRoute() {
        RouteHelper.refresh();
        log.info("RouteScheduling: refresh route success!");
    }

    @Scheduled(fixedRateString = "#{${zuul.dynamic-route.refresh-interval-seconds:30}*1000}")
    public void refreshGrayRoute() {
        GrayRouteHelper.refresh();
        log.info("RouteScheduling: refresh gray route success!");
    }
}
