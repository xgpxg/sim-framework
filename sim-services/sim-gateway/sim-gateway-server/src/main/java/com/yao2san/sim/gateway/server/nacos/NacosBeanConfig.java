package com.yao2san.sim.gateway.server.nacos;

import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.nacos.api.naming.NamingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NacosBeanConfig {
    private final NacosConfigProperties nacosConfigProperties;

    private final NacosServiceManager nacosServiceManager;

    public NacosBeanConfig(NacosConfigProperties nacosConfigProperties, NacosServiceManager nacosServiceManager) {
        this.nacosConfigProperties = nacosConfigProperties;
        this.nacosServiceManager = nacosServiceManager;
    }

    @Bean
    public NamingService namingService(){
       return nacosServiceManager.getNamingService(nacosConfigProperties.assembleConfigServiceProperties());
    }
}
