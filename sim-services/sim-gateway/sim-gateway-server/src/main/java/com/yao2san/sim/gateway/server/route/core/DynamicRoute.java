package com.yao2san.sim.gateway.server.route.core;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;

import java.security.Signature;

@EqualsAndHashCode(callSuper = true)
@Data
public class DynamicRoute extends ZuulProperties.ZuulRoute {

    private String prefix;

    private String routeName;

    private Long routeAppId;

    private String version;

    private String status;

    private String type;

    private Boolean isPublic = false;

    private String publicPrefix;


}
