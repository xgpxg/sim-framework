package com.yao2san.sim.gateway.server.route.core;

import com.yao2san.sim.gateway.server.config.Constant;
import com.yao2san.simservice.bean.simgateway.common.RouteExtra;
import com.yao2san.simservice.mapper.simgateway.RouteMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.DependsOn;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Load router from database.
 *
 * @author wxg
 */
@Slf4j
@DependsOn("nacosServiceUtil")
public class DatabaseRouteLocator extends AbstractRouteLocator {

    @Autowired
    private RouteMapper routeMapper;

    public DatabaseRouteLocator(String servletPath, ZuulProperties properties) {
        super(servletPath, properties);
    }

    @Override
    synchronized Map<String, ZuulProperties.ZuulRoute> loadDynamicRoute() {
        Map<String, ZuulProperties.ZuulRoute> routeMap = new LinkedHashMap<>();

        List<DynamicRoute> dbRoutes = new ArrayList<>();
        List<RouteExtra> routeExtraList = routeMapper.loadAll();
        for (RouteExtra routeExtra : routeExtraList) {
            DynamicRoute dr = new DynamicRoute();
            BeanUtils.copyProperties(routeExtra, dr);
            dbRoutes.add(dr);
        }
        if (dbRoutes.size() > 0) {
            dbRoutes.forEach(route -> {
                String path = route.getPath();
                String[] split = path.split(",");
                if (split.length > 1) {
                    int n = 1;
                    for (String s : split) {
                        DynamicRoute r = new DynamicRoute();
                        BeanUtils.copyProperties(route, r);
                        r.setId(r.getId() + "_" + n);
                        r.setPath(s);
                        routeMap.put(s, r);
                        n++;
                    }
                } else {
                    routeMap.put(route.getPath(), route);
                }
                DynamicRoute publicRoute = buildPublicRoute(route);
                if (publicRoute != null) {
                    routeMap.put(publicRoute.getPath(), publicRoute);
                }
            });
        } else {
            log.info("Dynamic route not found.");
        }
        return routeMap;
    }

    private DynamicRoute buildPublicRoute(DynamicRoute route) {
        if (route.getIsPublic()) {
            if (route.getPublicPrefix() != null && !route.getPublicPrefix().equals("")) {
                DynamicRoute publicRoute = new DynamicRoute();
                BeanUtils.copyProperties(route, publicRoute);
                String path = /*Constant.OPEN_SERVICE_PREFIX + */route.getPublicPrefix() + Constant.OPEN_SERVICE_SUFFIX;
                publicRoute.setPath(path);
                return publicRoute;
            } else {
                log.error("The route {} is configured with public access, but the access prefix is not configured, which may cause an exception. Please check the route configuration", route.getRouteName());
                return null;
            }
        }
        return null;
    }
}
