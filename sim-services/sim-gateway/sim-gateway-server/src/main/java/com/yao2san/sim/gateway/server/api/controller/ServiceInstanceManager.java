package com.yao2san.sim.gateway.server.api.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.gateway.server.api.request.MetadataUpdateReq;
import com.yao2san.sim.gateway.server.nacos.NacosServiceUtil;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("service-info")
public class ServiceInstanceManager {
    @GetMapping
    public ResponseData list() {
        List<String> services = NacosServiceUtil.getServices();
        return ResponseData.success(services);
    }

    @GetMapping("inst/{groupName}/{serviceId}")
    public ResponseData serviceInfo(@PathVariable String groupName,@PathVariable String serviceName) {
        return ResponseData.success(NacosServiceUtil.getServiceInfo(serviceName,groupName));
    }

    @GetMapping("inst")
    public ResponseData instances() {
        Map<String, List<ServiceInstance>> serviceInstanceInfo = NacosServiceUtil.getServiceInstanceInfo();
        return ResponseData.success(serviceInstanceInfo);
    }


    /**
     * update metadata
     */
    @PostMapping("inst/{serviceId}/metadata")
    public ResponseData updateMetadata(@PathVariable @NotEmpty String serviceId, @RequestBody @Validated MetadataUpdateReq metadataUpdateReq) {
        NacosServiceUtil.upsertMetadata(
                serviceId,
                metadataUpdateReq.getInstanceId(),
                metadataUpdateReq.getKey(),
                metadataUpdateReq.getValue());
        return ResponseData.success(null, "success");
    }
}
