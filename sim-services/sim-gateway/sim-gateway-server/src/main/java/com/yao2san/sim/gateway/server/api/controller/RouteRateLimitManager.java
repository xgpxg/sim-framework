package com.yao2san.sim.gateway.server.api.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.RouteRateLimitService;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitListReq;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitListRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Rate manage api
 *
 * @author wxg
 */
@RestController
@RequestMapping("rateLimit")
public class RouteRateLimitManager {

    @SuppressWarnings("all")
    @Autowired
    private RouteRateLimitService ratelimitServiceRoute;

    @PostMapping("refresh")
    public ResponseData<Void> refresh() {
        ratelimitServiceRoute.refresh();
        return ResponseData.success(null, "success");
    }

    @PostMapping("refresh/{routeId}")
    public ResponseData<Void> refreshByRouteId(@PathVariable Long routeId) {
        ratelimitServiceRoute.refresh(routeId);
        return ResponseData.success(null, "success");
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated RouteRateLimitAddOrUpdateReq req) {
        ratelimitServiceRoute.add(req);
        ratelimitServiceRoute.refresh(req.getRouteId());
        return ResponseData.success();
    }

    @DeleteMapping("{routeRateLimitPolicyId}")
    public ResponseData<Void> delete(@PathVariable("routeRateLimitPolicyId") Long routeRateLimitPolicyId) {
        ratelimitServiceRoute.delete(routeRateLimitPolicyId);
        return ResponseData.success();
    }

    @PatchMapping
    public ResponseData<Void> update(@RequestBody RouteRateLimitAddOrUpdateReq req) {
        ratelimitServiceRoute.update(req);
        ratelimitServiceRoute.refresh(req.getRouteId());
        return ResponseData.success();
    }

    @PatchMapping("status")
    public ResponseData<Void> updateStatus(@RequestBody RouteRateLimitAddOrUpdateReq req) {
        ratelimitServiceRoute.updateStatus(req);
        ratelimitServiceRoute.refresh(req.getRouteId());
        return ResponseData.success();
    }

    @GetMapping
    public ResponseData<ResponsePageData<RouteRateLimitListRes>> list(RouteRateLimitListReq req) {
        return ResponseData.success(ratelimitServiceRoute.list(req));
    }

    @GetMapping("{routeRateLimitPolicyId}")
    public ResponseData<RouteRateLimitDetailRes> detail(@PathVariable("routeRateLimitPolicyId") Long routeRateLimitPolicyId) {
        return ResponseData.success(ratelimitServiceRoute.detail(routeRateLimitPolicyId));
    }

    @GetMapping("test")
    public ResponseData<String> test1(){
        return ResponseData.success("get");
    }
    @PostMapping("test")
    public ResponseData<String> test2(){
        return ResponseData.success("post");
    }
}
