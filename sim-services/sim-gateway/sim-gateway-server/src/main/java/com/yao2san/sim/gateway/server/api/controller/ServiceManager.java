package com.yao2san.sim.gateway.server.api.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.ServiceManagerService;
import com.yao2san.simservice.bean.simgateway.request.ServiceAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.ServiceListReq;
import com.yao2san.simservice.bean.simgateway.response.ServiceDetailRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceGroupListRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceListRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("service-manager")
public class ServiceManager {
    @Autowired
    private ServiceManagerService serviceManagerService;

    @GetMapping
    public ResponseData<ResponsePageData<ServiceListRes>> list(ServiceListReq req) {
        return ResponseData.success(serviceManagerService.list(req));
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated ServiceAddOrUpdateReq req) {
        return serviceManagerService.add(req);
    }

    @PatchMapping
    public ResponseData<Void> update(@RequestBody @Validated ServiceAddOrUpdateReq req) {
        return serviceManagerService.update(req);
    }

    @GetMapping("{serviceId:^[0-9]*$}")
    public ResponseData<ServiceDetailRes> detail(@PathVariable Long serviceId) {
        return serviceManagerService.detail(serviceId);
    }

    @DeleteMapping("{serviceId}")
    public ResponseData<Void> delete(@PathVariable Long serviceId) {
        return serviceManagerService.delete(serviceId);
    }

    @PatchMapping("offline/{serviceId}")
    public ResponseData<Void> offline(@PathVariable("serviceId") Long serviceId) {
        return serviceManagerService.offline(serviceId);
    }

    @PatchMapping("online/{serviceId}")
    public ResponseData<Void> online(@PathVariable("serviceId") Long serviceId) {
        return serviceManagerService.online(serviceId);
    }

    @GetMapping("group")
    public ResponseData<List<ServiceGroupListRes>> group() {
        return serviceManagerService.group();
    }
}
