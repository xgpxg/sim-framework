package com.yao2san.sim.gateway.server.route.rule;

import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.RandomUtil;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.yao2san.sim.gateway.server.config.GatewayProperties;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * Loadbalancer rule for weight
 */
@Slf4j
public class WeightMatchRule extends AbstractMatcher {


    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }

    @Override
    public Server choose(Object key) {
        ILoadBalancer lb = getLoadBalancer();

        Server server;

        if (!this.isMatch()) {
            List<Server> notGrayServers = new ArrayList<>(lb.getReachableServers());
            notGrayServers.removeAll(GrayRouteHelper.getGrayServer(lb));
            server = choose(lb, key, notGrayServers);
            log.debug("Not hit gray service, current instance:{}", server);
        } else {
            server = choose(lb, key, GrayRouteHelper.getGrayServer(lb));
            log.debug("Hit gray service,use rule: WeightMatchRule, current instance:{}", server);
        }
        return server;
    }




    @Override
    public boolean isMatch() {
        String name = getName();
        Map<String, GatewayProperties.GrayRoute> grayRouteMap = GrayRouteHelper.getAll();
        GatewayProperties.GrayRoute grayRoute = grayRouteMap.get(name);
        List<WeightRandom.WeightObj<String>> weightObjs = new ArrayList<>();
        WeightRandom.WeightObj<String> grayWeightObj = new WeightRandom.WeightObj<>(
                "Y",
                grayRoute.getWeight());
        WeightRandom.WeightObj<String> mainWeightObj = new WeightRandom.WeightObj<>(
                "N",
                100.0 - grayRoute.getWeight());
        weightObjs.add(grayWeightObj);
        weightObjs.add(mainWeightObj);
        WeightRandom<String> random = RandomUtil.weightRandom(weightObjs);

        return "Y".equals(random.next());
    }
}
