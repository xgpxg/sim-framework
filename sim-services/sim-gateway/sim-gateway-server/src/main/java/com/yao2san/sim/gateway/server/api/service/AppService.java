package com.yao2san.sim.gateway.server.api.service;

import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simgateway.request.AppAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.AppListReq;
import com.yao2san.simservice.bean.simgateway.response.AppListRes;

public interface AppService {
    void add(AppAddOrUpdateReq req);
    void update(AppAddOrUpdateReq req);
    void remove(Long groupId);
    ResponsePageData<AppListRes> list(AppListReq req);
}
