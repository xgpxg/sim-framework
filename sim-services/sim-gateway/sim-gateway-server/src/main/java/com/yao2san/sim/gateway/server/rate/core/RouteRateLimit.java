package com.yao2san.sim.gateway.server.rate.core;

import com.yao2san.sim.gateway.server.route.core.DynamicRoute;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class RouteRateLimit extends DynamicRoute {

    private Long routeId;

    private Long routeRateLimitPolicyId;

    private Long refreshInterval;

    private Long limit;

    private Long quota;

    private boolean breakOnMatch;

    private String matchType;

    private String matcher;

}
