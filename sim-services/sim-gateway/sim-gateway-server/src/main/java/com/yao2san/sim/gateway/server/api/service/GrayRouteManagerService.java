package com.yao2san.sim.gateway.server.api.service;

import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteListReq;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteListRes;

public interface GrayRouteManagerService {

    GrayRouteDetailRes detail(Long routeGrayId);

    ResponsePageData<GrayRouteListRes> list(GrayRouteListReq req);

    void add(GrayRouteAddOrUpdateReq req);

    void delete(Long routeGrayId);

    void update(GrayRouteAddOrUpdateReq grayRouteReq);

    void online(Long routeGrayId);

    void offline(Long routeGrayId);

    /**
     * gray to formal
     */
    void formal(Long routeGrayId);

    void copy(Long routeGrayId);
}
