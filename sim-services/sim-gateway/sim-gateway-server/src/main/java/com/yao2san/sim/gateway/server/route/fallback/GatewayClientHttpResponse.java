package com.yao2san.sim.gateway.server.route.fallback;

import com.alibaba.fastjson.JSONObject;
import com.yao2san.sim.framework.web.response.ResponseCode;
import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author wxg
 **/
public class GatewayClientHttpResponse implements ClientHttpResponse {
    private final HttpStatus status;

    private final String msg;

    public GatewayClientHttpResponse(HttpStatus status, String msg) {

        this.status = status;
        this.msg = msg;
    }

    @Override
    public HttpStatus getStatusCode() throws IOException {
        return status;
    }

    @Override
    public int getRawStatusCode() throws IOException {
        return status.value();
    }

    @Override
    public String getStatusText() throws IOException {
        return status.getReasonPhrase();
    }

    @Override
    public void close() {
    }

    @Override
    public InputStream getBody() {
        ResponseData<Object> error = ResponseData.error(ResponseCode.SERVICE_UNAVAILABLE, msg);
        return new ByteArrayInputStream(JSONObject.toJSONString(error).getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
