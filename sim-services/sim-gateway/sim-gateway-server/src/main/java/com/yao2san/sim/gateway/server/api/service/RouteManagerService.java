package com.yao2san.sim.gateway.server.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simgateway.request.RouteAddReq;
import com.yao2san.simservice.bean.simgateway.request.RouteListReq;
import com.yao2san.simservice.bean.simgateway.request.RouteUpdateReq;
import com.yao2san.simservice.bean.simgateway.response.RouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteListRes;
import com.yao2san.simservice.entity.simgateway.Route;

public interface RouteManagerService extends IService<Route> {

    RouteDetailRes detail(Long routeId);

    /**
     * 新增路由
     * @param req
     */
    void add(RouteAddReq req);

    void delete(Long routeId);

    void update(RouteUpdateReq req);

    void online(Long routeId);

    void offline(Long routeId);

    ResponsePageData<RouteListRes> list(RouteListReq req);
}
