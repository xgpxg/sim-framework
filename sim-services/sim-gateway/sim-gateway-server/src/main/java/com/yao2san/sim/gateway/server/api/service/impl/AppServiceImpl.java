package com.yao2san.sim.gateway.server.api.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.request.AppReq;
import com.yao2san.sim.gateway.server.api.service.AppService;
import com.yao2san.simservice.bean.simgateway.request.AppAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.AppListReq;
import com.yao2san.simservice.bean.simgateway.response.AppListRes;
import com.yao2san.simservice.entity.simgateway.Route;
import com.yao2san.simservice.entity.simgateway.RouteApp;
import com.yao2san.simservice.enums.common.YesOrNo;
import com.yao2san.simservice.mapper.simgateway.RouteAppMapper;
import com.yao2san.simservice.mapper.simgateway.RouteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AppServiceImpl extends ServiceBaseImpl<RouteAppMapper, RouteApp> implements AppService {
    @Autowired
    private RouteAppMapper routeAppMapper;
    @Autowired
    private RouteMapper routeMapper;

    @Override
    @Transactional
    public void add(AppAddOrUpdateReq req) {
        req.setAppName(req.getAppName().trim());
        req.setAppCode(req.getAppCode().trim());
        req.setStatus(YesOrNo.YES);
        checkName(req);
        checkCode(req);
        this.save(req);
    }

    @Override
    @Transactional
    public void update(AppAddOrUpdateReq req) {
        req.setAppName(req.getAppName().trim());
        req.setAppCode(req.getAppCode().trim());
        checkName(req);
        this.updateById(req);
    }

    @Override
    public ResponsePageData<AppListRes> list(AppListReq req) {
        Page<AppListRes> page = convertPage(req);
        List<AppListRes> list = routeAppMapper.list(page, req);
        return buildPageResult(page, list);
    }

    @Override
    public void remove(Long routeAppId) {
        AppReq req = new AppReq();
        req.setRouteAppId(routeAppId);
        Long count = routeMapper.selectCount(Wrappers.lambdaQuery(Route.class).eq(Route::getRouteAppId, req.getRouteAppId()));
        if (count > 0) {
            throw new BusiException("请先删除路由后再删除应用");
        }
        this.removeById(routeAppId);
    }

    private void checkName(AppAddOrUpdateReq req) {
        long count = this.count(Wrappers.lambdaQuery(RouteApp.class)
                .eq(RouteApp::getAppName, req.getAppName())
                .ne(RouteApp::getId, req.getId()));
        if (count > 0) {
            throw new BusiException("应用名称[" + req.getAppName() + "]已存在");
        }
    }

    private void checkCode(AppAddOrUpdateReq req) {
        long count = this.count(Wrappers.lambdaQuery(RouteApp.class)
                .eq(RouteApp::getAppName, req.getAppName())
                .ne(RouteApp::getId, req.getId()));
        if (count > 0) {
            throw new BusiException("应用编码[" + req.getAppCode() + "]已存在");
        }
    }
}
