package com.yao2san.sim.gateway.server.route.rule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.netflix.zuul.context.RequestContext;
import com.yao2san.sim.gateway.server.config.GatewayProperties;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Loadbalancer rule for header
 */
@Slf4j
public class HeaderMatchRule extends AbstractMatcher {
    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }

    @Override
    public Server choose(Object key) {
        ILoadBalancer lb = getLoadBalancer();

        Server server;
        if (!this.isMatch()) {
            List<Server> notGrayServers = new ArrayList<>(lb.getReachableServers());
            notGrayServers.removeAll(GrayRouteHelper.getGrayServer(lb));
            server = choose(lb, key, notGrayServers);
            log.debug("Not hit gray service, current instance:{}", server);
        } else {
            server = choose(lb, key, GrayRouteHelper.getGrayServer(lb));
            log.debug("Hit gray service,use rule: HeaderMatchRule, current instance:{}", server);
        }
        return server;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public boolean isMatch() {
        Map<String, GatewayProperties.GrayRoute> all = GrayRouteHelper.getAll();
        GatewayProperties.GrayRoute grayRoute = all.get(getName());
        if (grayRoute == null || grayRoute.getServiceInstances().size() == 0) {
            return false;
        }
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();
        Map<String, Object> headers = grayRoute.getHeaders();
        Set<String> headerSet = headers.keySet();
        int count = 0;
        while (headerNames.hasMoreElements()) {
            String s = headerNames.nextElement();
            if (headerSet.contains(s)) {
                if (StringUtils.equals(request.getHeader(s), String.valueOf(headers.get(s)))) {
                    count++;
                }
            }
        }
        return count == headerSet.size();
    }
}
