package com.yao2san.sim.gateway.server.api.service.impl;

import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.netflix.loadbalancer.IRule;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.GrayRouteManagerService;
import com.yao2san.sim.gateway.server.api.service.RouteManagerService;
import com.yao2san.sim.gateway.server.route.core.RouteHelper;
import com.yao2san.sim.gateway.server.route.gray.GrayRoute;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteListReq;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteListRes;
import com.yao2san.simservice.bean.simgateway.response.RouteDetailRes;
import com.yao2san.simservice.entity.simgateway.RouteGray;
import com.yao2san.simservice.mapper.simgateway.RouteGrayMapper;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.SetUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Slf4j
public class GrayRouteManagerServiceImpl extends ServiceBaseImpl<RouteGrayMapper, RouteGray> implements GrayRouteManagerService {
    @Autowired
    private RouteManagerService routeManagerService;


    @Autowired
    private NacosServiceManager nacosServiceManager;

    @Autowired
    private RouteGrayMapper routeGrayMapper;

    @Override
    public GrayRouteDetailRes detail(Long routeGrayId) {
        GrayRouteDetailRes res = routeGrayMapper.detail(routeGrayId);
        //GrayRouteRes res = this.sqlSession.selectOne("grayRoute.detail", routeGrayId);
        res.setHeaders(JSONObject.parseObject(res.getHeadersString(), new TypeReference<Map<String, Object>>() {
        }));
        res.setCookies(JSONObject.parseObject(res.getCookiesString(), new TypeReference<Map<String, Object>>() {
        }));
        res.setParams(JSONObject.parseObject(res.getParamsString(), new TypeReference<Map<String, Object>>() {
        }));
        return res;
    }

    @Override
    public ResponsePageData<GrayRouteListRes> list(GrayRouteListReq req) {
        Page<GrayRouteListRes> page = convertPage(req);
        List<GrayRouteListRes> list = routeGrayMapper.list(page, req);
        return buildPageResult(page, list);
    }

    @Override
    @Transactional
    public void add(GrayRouteAddOrUpdateReq req) {
        req.setStatus("1200");
        this.convertMapToString(req);
        //this.sqlSession.insert("grayRoute.add", grayRouteReq);
        this.save(req);
    }

    @Override
    public void delete(Long routeGrayId) {
        this.removeById(routeGrayId);
        //this.sqlSession.delete("grayRoute.delete", routeGrayId);
    }

    @Override
    @Transactional
    public void update(GrayRouteAddOrUpdateReq req) {
        this.convertMapToString(req);
        String instances = req.getServiceInstances();
        if (instances != null) {
            Set<String> serviceInstances = SetUtils.hashSet(instances.split(","));
            for (String instance : serviceInstances) {
                try {
                    //nacosApiService.heartbeat(grayRouteReq.getServiceId(), instance);
                } catch (FeignException.NotFound e) {
                    throw new BusiException("Service instance " + instance + " not found.");
                }
            }
        }
        //this.sqlSession.update("grayRoute.update", grayRouteReq);
        this.updateById(req);
    }

    private void convertMapToString(GrayRouteAddOrUpdateReq grayRouteReq) {
        /*if (grayRouteReq.getHeaders() != null) {
            grayRouteReq.setHeadersString(JSONObject.toJSONString(grayRouteReq.getHeaders(), SerializerFeature.WriteMapNullValue));
        }
        if (grayRouteReq.getCookies() != null) {
            grayRouteReq.setCookiesString(JSONObject.toJSONString(grayRouteReq.getCookies(), SerializerFeature.WriteMapNullValue));
        }
        if (grayRouteReq.getParams() != null) {
            grayRouteReq.setParamsString(JSONObject.toJSONString(grayRouteReq.getParams(), SerializerFeature.WriteMapNullValue));
        }*/
    }

    @Override
    @Transactional
    public void online(Long routeGrayId) {
        //update service instances metadata
        GrayRouteDetailRes grayRouteRes = this.detail(routeGrayId);
        Set<String> serviceInstances = grayRouteRes.getServiceInstances();

        //update service instance status
        //serviceInstances.forEach(instance -> nacosApiService.updateStatus(grayRouteRes.getServiceId(), instance, InstanceInfo.InstanceStatus.UP.name()));

        //update loadbalancer rule
        String ruleTypeName = grayRouteRes.getRuleType();
        GrayRoute.RuleType ruleType = GrayRoute.RuleType.valueOf(ruleTypeName);
        String serviceId = grayRouteRes.getServiceId();
        IRule rule = RouteHelper.getLbRuleWithRuleType(ruleType);
        RouteHelper.changeLbRule(serviceId, rule);
        //update status
        RouteGray req = new RouteGray();
        req.setId(routeGrayId);
        req.setStatus("1000");

        GrayRouteHelper.refresh();

        this.updateById(req);
    }

    @Override
    @Transactional
    public void offline(Long routeGrayId) {
        GrayRouteDetailRes grayRouteRes = this.detail(routeGrayId);
        RouteDetailRes routeRes = routeManagerService.detail(grayRouteRes.getRouteId());
        String serviceId = routeRes.getServiceId();
        Set<String> serviceInstances = grayRouteRes.getServiceInstances();

        //update service instance status from eureka
        try {
            //serviceInstances.forEach(instance -> nacosApiService.updateStatus(serviceId, instance, InstanceInfo.InstanceStatus.OUT_OF_SERVICE.name()));
        } catch (Exception e) {
            log.warn("Service offline warn: update service instance status fial");
        }

        //change loadbalancer rule to default rule
        RouteHelper.changeLbRule(serviceId, RouteHelper.getDefaultLbRule(serviceId));

        //update status
        RouteGray req = new RouteGray();
        req.setId(routeGrayId);
        req.setStatus("1300");
        this.updateById(req);

        GrayRouteHelper.refresh();
    }

    @Override
    @Transactional
    public void formal(Long routeGrayId) {
        RouteGray routeGray = new RouteGray();
        routeGray.setId(routeGrayId);
        GrayRouteDetailRes grayRouteRes = this.detail(routeGrayId);
        String serviceId = grayRouteRes.getServiceId();

        //update service instance
        Set<String> serviceInstances = grayRouteRes.getServiceInstances();

        serviceInstances.forEach(instance -> {
            //check service instance status(send heartbeat)
            //note: maybe throw exception if instance not found
            //nacosApiService.heartbeat(serviceId, instance);
            //update service instance status from eureka
            //nacosApiService.updateStatus(serviceId, instance, InstanceInfo.InstanceStatus.UP.name());
        });

        //change loadbalancer rule to default rule
        RouteHelper.changeLbRule(serviceId, RouteHelper.getDefaultLbRule(serviceId));

        //update gray route to formal
        routeGray.setStatus("1400");
        this.updateById(routeGray);

        //refresh gray routes
        GrayRouteHelper.refresh();
    }

    @Override
    @Transactional
    public void copy(Long routeGrayId) {
        GrayRouteDetailRes detail = this.detail(routeGrayId);
        RouteGray copy = new RouteGray();
        BeanUtils.copyProperties(detail, copy);
        this.save(copy);
    }
}
