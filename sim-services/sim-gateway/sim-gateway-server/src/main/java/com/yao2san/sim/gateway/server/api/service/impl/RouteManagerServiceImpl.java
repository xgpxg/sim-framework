package com.yao2san.sim.gateway.server.api.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.RouteManagerService;
import com.yao2san.sim.gateway.server.route.core.RouteHelper;
import com.yao2san.simservice.bean.simgateway.request.RouteAddReq;
import com.yao2san.simservice.bean.simgateway.request.RouteListReq;
import com.yao2san.simservice.bean.simgateway.request.RouteUpdateReq;
import com.yao2san.simservice.bean.simgateway.response.RouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteListRes;
import com.yao2san.simservice.entity.simgateway.Route;
import com.yao2san.simservice.mapper.simgateway.RouteMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@Service
public class RouteManagerServiceImpl extends ServiceBaseImpl<RouteMapper, Route> implements RouteManagerService {

    @Autowired
    private RouteMapper routeMapper;

    @Override
    @Transactional
    public void add(RouteAddReq req) {
        if (StringUtils.isEmpty(req.getUrl()) && StringUtils.isEmpty(req.getServiceId())) {
            throw new BusiException("url or serviceId cannot be empty at the same time");
        }

        if (exist(req.getPath())) {
            throw new BusiException("path [" + req.getPath() + "] already matches another route, please change then path value");
        }
        this.save(req);
        //this.sqlSession.insert("route.add", routeReq);
        log.info("Add route:{}", req);
    }

    private boolean exist(String path) {
        //Integer count = this.sqlSession.selectOne("route.existByPath", path);
        long count = this.count(Wrappers.lambdaQuery(Route.class).eq(Route::getPath, path));
        return count > 0;
    }

    @Override
    @Transactional
    public void delete(Long routeId) {
        this.removeById(routeId);
    }

    @Override
    @Transactional
    public void update(RouteUpdateReq req) {

        //this.sqlSession.update("route.update", routeReq);

        //if has been online, refresh route
        Route routeRes = this.getById(req.getId());
        this.updateById(req);
        if (StringUtils.equals(routeRes.getStatus(), "1000")) {
            RouteHelper.refresh();
        }
    }

    @Override
    @Transactional
    public void online(Long routeId) {
        Route route = new Route();
        route.setId(routeId);
        route.setStatus("1000");
        this.updateById(route);
        RouteHelper.refresh();
    }

    @Override
    @Transactional
    public void offline(Long routeId) {
        Route route = new Route();
        route.setId(routeId);
        route.setStatus("1300");
        this.updateById(route);
        RouteHelper.refresh();
    }

    @Override
    public RouteDetailRes detail(@NotNull Long routeId) {
        RouteDetailRes res = new RouteDetailRes();
        Route route = this.getById(routeId);
        BeanUtils.copyProperties(route,res);
        return res;
    }

    @Override
    public ResponsePageData<RouteListRes> list(RouteListReq routeReq) {
        Page<RouteListRes> page = convertPage(routeReq);
        List<RouteListRes> list = routeMapper.listPage(page, routeReq);
        return buildPageResult(page, list);
    }
}
