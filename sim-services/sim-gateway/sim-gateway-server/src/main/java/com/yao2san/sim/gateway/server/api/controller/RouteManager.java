package com.yao2san.sim.gateway.server.api.controller;

import com.yao2san.sim.framework.log.annotation.PrintLog;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.RouteManagerService;
import com.yao2san.sim.gateway.server.route.core.AbstractRouteLocator;
import com.yao2san.sim.gateway.server.route.core.RouteHelper;
import com.yao2san.simservice.bean.simgateway.request.RouteAddReq;
import com.yao2san.simservice.bean.simgateway.request.RouteListReq;
import com.yao2san.simservice.bean.simgateway.request.RouteUpdateReq;
import com.yao2san.simservice.bean.simgateway.response.RouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteListRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * Route manage api
 *
 * @author wxg
 */
@RestController
@RequestMapping("route")
public class RouteManager {
    @SuppressWarnings("all")
    @Autowired
    private AbstractRouteLocator abstractRouteLocator;

    @SuppressWarnings("all")
    @Autowired
    private RouteManagerService routeManagerService;

    @PostMapping("refresh")
    public ResponseData<Void> refresh() {
        RouteHelper.refresh();
        return ResponseData.success(null, "success");
    }

    @PatchMapping("/online/{routeId}")
    public ResponseData<Void> online(@PathVariable Long routeId) {
        routeManagerService.online(routeId);
        return ResponseData.success(null, "success");
    }

    @PatchMapping("/offline/{routeId}")
    public ResponseData<Void> offline(@PathVariable Long routeId) {
        routeManagerService.offline(routeId);
        return ResponseData.success(null, "success");
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody @Validated RouteAddReq req) {
        routeManagerService.add(req);
        return ResponseData.success(null, "success");
    }

    @DeleteMapping("{routeId}")
    public ResponseData<Void> delete(@PathVariable @NotNull Long routeId) {
        routeManagerService.delete(routeId);
        return ResponseData.success(null, "success");
    }

    @PatchMapping
    public ResponseData<Void> update(@RequestBody @Validated RouteUpdateReq req) {
        routeManagerService.update(req);
        return ResponseData.success(null, "success");
    }

    @GetMapping
    @PrintLog(value = "查询路由列表", useTimer = true)
    public ResponseData<ResponsePageData<RouteListRes> > list(RouteListReq req) {
        ResponsePageData<RouteListRes> list = routeManagerService.list(req);
        return ResponseData.success(list);
    }

    @GetMapping("{routeId}")
    public ResponseData<RouteDetailRes> detail(@PathVariable Long routeId) {
        RouteDetailRes detail = routeManagerService.detail(routeId);
        return ResponseData.success(detail);
    }
}
