package com.yao2san.sim.gateway.server.api.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.common.auth.util.UserUtil;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.request.ServiceReq;
import com.yao2san.sim.gateway.server.api.service.ServiceManagerService;
import com.yao2san.simservice.bean.simgateway.request.ServiceAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.ServiceGroupListReq;
import com.yao2san.simservice.bean.simgateway.request.ServiceListReq;
import com.yao2san.simservice.bean.simgateway.response.ServiceDetailRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceGroupListRes;
import com.yao2san.simservice.bean.simgateway.response.ServiceListRes;
import com.yao2san.simservice.mapper.simgateway.ServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Service
public class ServiceManagerServiceImpl extends ServiceBaseImpl<ServiceMapper, com.yao2san.simservice.entity.simgateway.Service> implements ServiceManagerService {
    @Autowired
    private ServiceMapper serviceMapper;

    public ResponsePageData<ServiceListRes> list(ServiceListReq req) {
        //PageInfo<ServiceRes> list = this.qryList("service.list", req);
        Page<ServiceListRes> page = convertPage(req);
        req.setCreateUser(UserUtil.getCurrUserId());
        List<ServiceListRes> list = serviceMapper.list(page, req);
        return buildPageResult(page, list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> add(ServiceAddOrUpdateReq req) {
        //this.sqlSession.insert("service.add", req);
        this.save(req);
        return ResponseData.success();
    }

    @Override
    public ResponseData<ServiceDetailRes> detail(Long serviceId) {
        ServiceReq req = new ServiceReq();
        req.setServiceId(serviceId);
        req.setCreateUser(UserUtil.getCurrUserId());

        ServiceDetailRes detail = serviceMapper.detail(serviceId);
        //ServiceRes res = this.sqlSession.selectOne("service.detail", req);
        return ResponseData.success(detail);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> update(@Validated ServiceAddOrUpdateReq req) {
        //this.sqlSession.insert("service.update", req);
        this.updateById(req);
        return ResponseData.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> delete(Long serviceId) {
        //ServiceReq req = new ServiceReq();
        //req.setServiceId(serviceId);
        //this.sqlSession.insert("service.delete", req);
        this.removeById(serviceId);
        return ResponseData.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> online(Long serviceId) {
        //ServiceReq req = new ServiceReq();
        //req.setServiceId(serviceId);
        //req.setStatus("1000");
        //this.sqlSession.update("service.update", req);
        updateStatus(serviceId, "1200");
        return ResponseData.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> offline(Long serviceId) {
        //ServiceReq param = new ServiceReq();
        //param.setServiceId(serviceId);
        //param.setStatus("1200");
        //this.sqlSession.update("service.update", param);
        updateStatus(serviceId, "1200");
        return ResponseData.success();
    }

    private void updateStatus(Long serviceId, String status) {
        this.updateById(new com.yao2san.simservice.entity.simgateway.Service().setId(serviceId).setStatus(status));
    }

    @Override
    public ResponseData<List<ServiceGroupListRes>> group() {
//        Map<String, Object> param = new HashMap<>();
//        param.put("createUser", UserUtil.getCurrUserId());
        ServiceGroupListReq req = new ServiceGroupListReq();
        req.setCreateUser(UserUtil.getCurrUserId());
        List<ServiceGroupListRes> list = serviceMapper.group(req);
        //List<ServiceGroupRes> list = this.sqlSession.selectList("service.group", param);
        return ResponseData.success(list);
    }
}
