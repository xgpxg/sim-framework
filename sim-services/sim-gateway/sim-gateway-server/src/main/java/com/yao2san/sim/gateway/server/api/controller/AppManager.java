package com.yao2san.sim.gateway.server.api.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.AppService;
import com.yao2san.simservice.bean.simgateway.request.AppAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.AppListReq;
import com.yao2san.simservice.bean.simgateway.response.AppListRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("appManage")
public class AppManager {

    @Autowired
    private AppService appService;

    /**
     * 新增应用
     *
     * @param req 参数
     * @return result
     */
    @PostMapping
    public ResponseData<Void> add(@RequestBody AppAddOrUpdateReq req) {
        appService.add(req);
        return ResponseData.success();
    }

    /**
     * 查询应用列表
     *
     * @param req 参数
     * @return 应用列表
     */
    @GetMapping
    public ResponseData<ResponsePageData<AppListRes>> list(AppListReq req) {
        return ResponseData.success(appService.list(req));
    }

    /**
     * 修改应用
     *
     * @param req 参数
     * @return result
     */
    @PatchMapping
    public ResponseData<Void> update(@RequestBody AppAddOrUpdateReq req) {
        appService.update(req);
        return ResponseData.success();
    }

    /**
     * 删除应用
     *
     * @param routeAppId 应用ID
     * @return result
     */
    @DeleteMapping("{routeAppId}")
    public ResponseData<Void> remove(@PathVariable("routeAppId") Long routeAppId) {
        appService.remove(routeAppId);
        return ResponseData.success();
    }
}
