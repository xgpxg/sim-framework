package com.yao2san;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.yao2san.sim.gateway.server.rate.core.RateLimitHelper;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableZuulProxy
@SpringBootApplication
@EnableScheduling
@EnableFeignClients
@Slf4j
@RestController
@MapperScan("com.yao2san.simservice.mapper.simgateway")
public class SimGatewayApplication {
    @Autowired
    private NamingService namingService;
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    public static void main(String[] args) {
        SpringApplication.run(SimGatewayApplication.class, args);

        log.info("sim-gateway-server start success!");

        RateLimitHelper.refresh();

    }
    @GetMapping("test")
    public void a() throws NacosException {

    }
}
