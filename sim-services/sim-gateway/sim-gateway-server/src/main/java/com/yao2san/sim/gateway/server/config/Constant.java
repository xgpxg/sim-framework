package com.yao2san.sim.gateway.server.config;

public class Constant {
    public static final String VERSION_KEY = "version";
    public static final String GRAY_STATUS_KEY = "gray.status";

    public static final String OPEN_SERVICE_PREFIX = "/service/";
    public static final String OPEN_SERVICE_SUFFIX = "/**";
}
