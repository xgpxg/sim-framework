package com.yao2san.sim.gateway.server.api.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.request.GrayRouteReq;
import com.yao2san.sim.gateway.server.api.service.GrayRouteManagerService;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.GrayRouteListReq;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteDetailRes;
import com.yao2san.simservice.bean.simgateway.response.GrayRouteListRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * Gray route manage api
 *
 * @author wxg
 */
@RestController
@RequestMapping("grayRoute")
public class GrayRouteManager {
 /*   @SuppressWarnings("all")
    @Autowired
    private AbstractGrayRouteRule grayRouteRule;*/

    @SuppressWarnings("all")
    @Autowired
    private GrayRouteManagerService grayRouteManagerService;

    @PostMapping("refresh")
    public ResponseData refresh() {
        GrayRouteHelper.refresh();
        return ResponseData.success(null, "success");
    }

    @PatchMapping("/formal/{routeGrayId}")
    public ResponseData formal(@PathVariable Long routeGrayId) {
        grayRouteManagerService.formal(routeGrayId);
        return ResponseData.success(null, "success");
    }


    @PostMapping
    public ResponseData add(@RequestBody @Validated GrayRouteAddOrUpdateReq req) {
        grayRouteManagerService.add(req);
        return ResponseData.success(null, "success");
    }

    @DeleteMapping("{grayRouteId}")
    public ResponseData delete(@PathVariable @NotNull Long grayRouteId) {
        grayRouteManagerService.delete(grayRouteId);
        return ResponseData.success(null, "success");
    }

    @PatchMapping
    public ResponseData update(@RequestBody @Validated GrayRouteAddOrUpdateReq req) {
        grayRouteManagerService.update(req);
        return ResponseData.success(null, "success");
    }

    @PatchMapping("/online/{routeGrayId}")
    public ResponseData online(@PathVariable @NotNull Long routeGrayId) {
        grayRouteManagerService.online(routeGrayId);
        return ResponseData.success(null, "success");
    }

    @PatchMapping("/offline/{routeGrayId}")
    public ResponseData offline(@PathVariable @NotNull Long routeGrayId) {
        grayRouteManagerService.offline(routeGrayId);
        return ResponseData.success(null, "success");
    }

    @GetMapping
    public ResponseData<ResponsePageData<GrayRouteListRes>> list(GrayRouteListReq req) {
        ResponsePageData<GrayRouteListRes> list = grayRouteManagerService.list(req);
        return ResponseData.success(list);
    }

    @GetMapping("{routeGrayId}")
    public ResponseData<GrayRouteDetailRes> detail(@PathVariable Long routeGrayId) {
        GrayRouteDetailRes detail = grayRouteManagerService.detail(routeGrayId);
        return ResponseData.success(detail);
    }

    @PostMapping("copy")
    public ResponseData copy(Long routeGrayId) {
        grayRouteManagerService.copy(routeGrayId);
        return ResponseData.success();
    }
}
