package com.yao2san.sim.gateway.server.api.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.common.auth.util.UserUtil;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.sim.gateway.server.api.service.RouteRateLimitService;
import com.yao2san.sim.gateway.server.rate.core.RateLimitHelper;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitAddOrUpdateReq;
import com.yao2san.simservice.bean.simgateway.request.RouteRateLimitListReq;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitDetailRes;
import com.yao2san.simservice.bean.simgateway.response.RouteRateLimitListRes;
import com.yao2san.simservice.entity.simgateway.RouteRateLimitPolicy;
import com.yao2san.simservice.mapper.simgateway.RouteRateLimitPolicyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RouteRouteRateLimitServiceImpl extends ServiceBaseImpl<RouteRateLimitPolicyMapper, RouteRateLimitPolicy> implements RouteRateLimitService {

    @Autowired
    private RouteRateLimitPolicyMapper routeRateLimitPolicyMapper;

    @Override
    public void refresh() {
        RateLimitHelper.refresh();
    }

    @Override
    public void refresh(Long routeId) {
        RateLimitHelper.refresh(routeId);
    }

    @Override
    public RouteRateLimitDetailRes detail(Long routeRateLimitPolicyId) {
        return routeRateLimitPolicyMapper.detail(routeRateLimitPolicyId);
        //return sqlSession.selectOne("rateLimit.detail", routeRateLimitPolicyId);
    }

    public ResponsePageData<RouteRateLimitListRes> list(RouteRateLimitListReq req) {
        Page<RouteRateLimitListRes> page = convertPage(req);
        List<RouteRateLimitListRes> list = routeRateLimitPolicyMapper.list(page, req);
        return buildPageResult(page, list);
        //return this.qryList("rateLimit.list", routeRateLimit);
    }

    @Override
    @Transactional
    public void add(RouteRateLimitAddOrUpdateReq req) {
        //sqlSession.insert("rateLimit.add", routeRateLimit);
        this.save(req);
    }

    @Override
    @Transactional
    public void delete(Long rateLimitPolicyId) {
        //sqlSession.delete("rateLimit.delete", rateLimitPolicyId);
        this.removeById(rateLimitPolicyId);
    }

    @Override
    @Transactional
    public void update(RouteRateLimitAddOrUpdateReq req) {
        //sqlSession.update("rateLimit.update", routeRateLimit);
        this.updateById(req);
    }

    @Override
    @Transactional
    public void updateStatus(RouteRateLimitAddOrUpdateReq req) {
        //sqlSession.update("rateLimit.updateStatus", routeRateLimit);
        this.updateById(new RouteRateLimitPolicy()
                .setId(req.getId())
                .setStatus(req.getStatus())
                .setUpdateUser(UserUtil.getCurrUserId()));
    }

}
