package com.yao2san.sim.gateway.server.route.rule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.netflix.zuul.context.RequestContext;
import com.yao2san.sim.gateway.server.config.GatewayProperties;
import com.yao2san.sim.gateway.server.route.gray.GrayRouteHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

/**
 * Loadbalancer rule for request param
 */
@Slf4j
public class ParamMatchRule extends AbstractMatcher {
    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }

    @Override
    public Server choose(Object key) {
        ILoadBalancer lb = getLoadBalancer();

        Server server;
        if (!this.isMatch()) {
            List<Server> notGrayServers = new ArrayList<>(lb.getReachableServers());
            notGrayServers.removeAll(GrayRouteHelper.getGrayServer(lb));
            server = choose(lb, key, notGrayServers);
            log.debug("Not hit gray service, current instance:{}", server);
        } else {
            server = choose(lb, key, GrayRouteHelper.getGrayServer(lb));
            log.debug("Hit gray service,use rule: ParamMatchRule, current instance:{}", server);
        }
        return server;
    }

    @Override
    public boolean isMatch() {
        Map<String, GatewayProperties.GrayRoute> all = GrayRouteHelper.getAll();
        GatewayProperties.GrayRoute grayRoute = all.get(getName());
        if (grayRoute == null || grayRoute.getServiceInstances().size() == 0) {
            return false;
        }
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        Enumeration<String> parameterNames = request.getParameterNames();

        int count = 0;
        while (parameterNames.hasMoreElements()) {
            String name = parameterNames.nextElement();
            if (StringUtils.equals(request.getParameter(name), String.valueOf(grayRoute.getParams().get(name)))) {
                count++;
            }
        }
        return count == grayRoute.getParams().size();
    }
}
