package com.yao2san.sim.gateway.server.route.core;

import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import com.netflix.loadbalancer.ZoneAwareLoadBalancer;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.gateway.server.route.gray.GrayRoute;
import com.yao2san.sim.gateway.server.route.rule.CookieMatchRule;
import com.yao2san.sim.gateway.server.route.rule.HeaderMatchRule;
import com.yao2san.sim.gateway.server.route.rule.ParamMatchRule;
import com.yao2san.sim.gateway.server.route.rule.WeightMatchRule;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.cloud.netflix.zuul.RoutesRefreshedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class RouteHelper {
    private static ApplicationEventPublisher eventPublisher;
    @Getter
    private static AbstractRouteLocator dynamicRouteLocator;

    private static SpringClientFactory clientFactory;

    @Autowired
    public void setEventPublisher(ApplicationEventPublisher eventPublisher) {
        RouteHelper.eventPublisher = eventPublisher;
    }

    @Autowired
    public void setDynamicRouteLocator(AbstractRouteLocator dynamicRouteLocator) {
        RouteHelper.dynamicRouteLocator = dynamicRouteLocator;
    }

    @Autowired
    public void setClientFactory(SpringClientFactory clientFactory) {
        RouteHelper.clientFactory = clientFactory;
    }

    public static void refresh() {
        RoutesRefreshedEvent routesRefreshedEvent = new RoutesRefreshedEvent(dynamicRouteLocator);
        RouteHelper.eventPublisher.publishEvent(routesRefreshedEvent);
    }


    @SuppressWarnings("all")
    public static void changeLbRule(String name, IRule rule) {
        ZoneAwareLoadBalancer loadBalancer = (ZoneAwareLoadBalancer) clientFactory.getLoadBalancer(name);
        rule.setLoadBalancer(loadBalancer);
        ((ZoneAwareLoadBalancer) clientFactory.getLoadBalancer(name)).setRule(rule);
    }

    public static IRule getDefaultLbRule(String name) {
        ILoadBalancer loadBalancer = clientFactory.getLoadBalancer(name);
        return new RoundRobinRule(loadBalancer);
    }

    public static IRule getLbRuleWithRuleType(GrayRoute.RuleType ruleType) {
        switch (ruleType) {
            case WEIGHT_MATCH:
                return new WeightMatchRule();
            case HEADER_MATCH:
                return new HeaderMatchRule();
            case COOKIE_MATCH:
                return new CookieMatchRule();
            case PARAM_MATCH:
                return new ParamMatchRule();
            default:
                throw new BusiException("Not supported loadbalancer rule:" + ruleType);
        }
    }

}
