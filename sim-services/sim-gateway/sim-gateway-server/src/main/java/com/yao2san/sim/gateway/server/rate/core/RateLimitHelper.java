package com.yao2san.sim.gateway.server.rate.core;

import com.alibaba.fastjson.JSONArray;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.properties.RateLimitProperties;
import com.yao2san.simservice.bean.simgateway.common.RouteRateLimitExtra;
import com.yao2san.simservice.mapper.simgateway.RouteRateLimitPolicyMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.properties.RateLimitProperties.PREFIX;

@Component
@Slf4j
@ConditionalOnProperty(prefix = PREFIX, name = "enabled", havingValue = "true")
public class RateLimitHelper {

    private static RateLimitProperties rateLimitProperties;

    private static RouteRateLimitPolicyMapper routeRateLimitPolicyMapper;


    @Autowired
    public void setRateLimitProperties(RateLimitProperties rateLimitProperties) {
        RateLimitHelper.rateLimitProperties = rateLimitProperties;
    }

    @Autowired
    public void setRouteRateLimitPolicyMapper(RouteRateLimitPolicyMapper routeRateLimitPolicyMapper) {
        RateLimitHelper.routeRateLimitPolicyMapper = routeRateLimitPolicyMapper;
    }

    /**
     * refresh all rate limit
     */
    public static void refresh() {
        refresh(null);
    }

    /**
     * refresh rate limit
     */
    public static void refresh(Long routeId) {
        List<RouteRateLimit> list = new ArrayList<>();
        List<RouteRateLimitExtra> routeRateLimitExtras;
        if (routeId != null) {
            routeRateLimitExtras = routeRateLimitPolicyMapper.qryRouteRateLimits(routeId);
            //list = sqlSession.selectList("rateLimit.qryRouteRateLimits", new RouteRateLimit().setRouteId(routeId));
        } else {
            routeRateLimitExtras = routeRateLimitPolicyMapper.qryRouteRateLimits(null);
            //list = sqlSession.selectList("rateLimit.qryRouteRateLimits");
        }
        for (RouteRateLimitExtra routeRateLimitExtra : routeRateLimitExtras) {
            RouteRateLimit routeRateLimit = new RouteRateLimit();
            BeanUtils.copyProperties(routeRateLimitExtra, routeRateLimit);
            list.add(routeRateLimit);
        }
        Map<String, List<RateLimitProperties.Policy>> policyMap = new HashMap<>();
        list.forEach(routeRateLimit -> {
            RateLimitProperties.Policy policy = new RateLimitProperties.Policy();
            policy.setLimit(routeRateLimit.getLimit());
            policy.setQuota(routeRateLimit.getQuota());
            policy.setRefreshInterval(routeRateLimit.getRefreshInterval());
            List<RateLimitProperties.Policy.MatchType> types = JSONArray.parseArray(routeRateLimit.getMatchType(), RateLimitProperties.Policy.MatchType.class);
            policy.setType(types);
            //NOTE: the key is Zuul.Route.id, not service id
            String key = routeRateLimit.getId();
            if (policyMap.get(key) == null) {
                List<RateLimitProperties.Policy> li = new LinkedList<>();
                li.add(policy);
                policyMap.put(key, li);
            } else {
                policyMap.get(key).add(policy);
            }
        });

        rateLimitProperties.setPolicyList(policyMap);

        log.info("Rate limit policy load success");
    }
}
