package com.yao2san.sim.gateway.server.nacos;


import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
@Data
public class NacosUtil {


    private static NamingService namingService;

    @Autowired
    public void setNamingService(NamingService namingService) {
        NacosUtil.namingService = namingService;
    }

    public static List<String> getAllSerices(String groupName) {
        try {
            return namingService.getServicesOfServer(1, 1000, groupName).getData();
        } catch (NacosException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public static List<Instance> getAllInstances(String serviceName, String groupName) {
        List<Instance> allInstances;
        try {
            allInstances = namingService.getAllInstances(serviceName, groupName);
            for (Instance instance : allInstances) {
                log.info("{}", instance);
            }
            return allInstances.stream().filter(v -> v.getServiceName().equals(serviceName)).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }
}
