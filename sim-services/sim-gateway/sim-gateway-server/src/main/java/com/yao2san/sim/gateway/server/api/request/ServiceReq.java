package com.yao2san.sim.gateway.server.api.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceReq extends Pagination {
    @NotNull(groups = Update.class)
    private Long serviceId;

    private String name;

    private String url;

    private String type;

    private Long routeId;

    private Long serviceGroupId;

    private String description;

    public interface Update{}
}
