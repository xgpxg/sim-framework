package com.yao2san.sim.gateway.client.annotations;

//import com.yao2san.sim.gateway.client.config.SimGatewayClientConfig;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
//@Import(SimGatewayClientConfig.class)
@EnableFeignClients(basePackages = "com.yao2san.**")
//@EnableZuulProxy
public @interface SimGatewayClient {
}
