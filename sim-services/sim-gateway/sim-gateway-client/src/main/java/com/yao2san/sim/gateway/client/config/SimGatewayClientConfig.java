package com.yao2san.sim.gateway.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yao2san.sim.gateway.client")
@ConfigurationProperties(prefix = "sim.gateway")
public class SimGatewayClientConfig {

}
