package com.yao2san.simservice.enums.user;

import com.yao2san.sim.framework.web.exception.BusiException;
import lombok.Getter;

/**
 * 登录方式
 *
 * @author wxg
 */
public enum LoginType {
    /**
     * 用户名密码登录
     */
    UN(0),

    /**
     * 短信验证码登录
     */
    VERIFY_CODE(1),

    /**
     * 微信登录
     */
    WX(2),

    /**
     * 微信小程序登录
     */
    WX_XCX(3);

    @Getter
    private final int type;


    LoginType(Integer type) {
        this.type = type;
    }

    public static LoginType of(int type) {
        for (LoginType loginType : LoginType.values()) {
            if (loginType.getType() == type) {
                return loginType;
            }
        }
        throw new BusiException("不支持的登录类型：" + type);
    }
}
