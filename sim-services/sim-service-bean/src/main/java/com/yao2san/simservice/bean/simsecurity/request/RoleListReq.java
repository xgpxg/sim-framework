package com.yao2san.simservice.bean.simsecurity.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleListReq extends Pagination {
    private String status;
}
