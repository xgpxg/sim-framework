package com.yao2san.simservice.bean.simgateway.response;

import lombok.Data;

@Data
public class ServiceGroupListRes {
    private Long id;
    private String groupName;
}
