package com.yao2san.simservice.bean.simsecurity.request.user;

import com.yao2san.simservice.enums.user.LoginType;
import lombok.Data;

/**
 * 用户登录请求参数
 *
 * @author wxg
 */
@Data
public class UserLoginReq {
    /**
     * 登录方式
     *
     * @see LoginType
     */
    private Integer loginType;

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    /**
     * 短信验证码
     */
    private String verifyCode;
}
