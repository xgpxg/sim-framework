package com.yao2san.simservice.bean.simsecurity.request;

import com.yao2san.simservice.entity.simsecurity.Purview;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PurviewAddOrUpdateReq extends Purview {
}
