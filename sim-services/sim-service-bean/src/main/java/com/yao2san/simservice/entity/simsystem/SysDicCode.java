package com.yao2san.simservice.entity.simsystem;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic_code")
public class SysDicCode extends Model<SysDicCode> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Long id;

    @TableField("dic_code")
    private String dicCode;

    @TableField("dic_code_name")
    private String dicCodeName;

    @TableField("description")
    private String description;

    @TableField("is_show")
    private Integer isShow;

    /**
     * 分组
     */
    @TableField("group_name")
    private String groupName;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;

    @Override
    public Serializable pkVal() {
        return this.dicCode;
    }

}
