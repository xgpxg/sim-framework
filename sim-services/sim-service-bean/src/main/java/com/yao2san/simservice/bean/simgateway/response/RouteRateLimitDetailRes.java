package com.yao2san.simservice.bean.simgateway.response;

import com.yao2san.simservice.entity.simgateway.RouteRateLimitPolicy;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RouteRateLimitDetailRes extends RouteRateLimitPolicy {
}
