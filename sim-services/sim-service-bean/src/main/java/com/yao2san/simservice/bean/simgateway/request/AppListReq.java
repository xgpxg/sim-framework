package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppListReq extends Pagination {
    private Long routeAppId;
    private String appName;
    private String appCode;
}
