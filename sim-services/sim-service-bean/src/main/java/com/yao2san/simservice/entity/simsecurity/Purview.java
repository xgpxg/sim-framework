package com.yao2san.simservice.entity.simsecurity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("purview")
public class Purview extends Model<Purview> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("purview_name")
    private String purviewName;

    /**
     * 1 菜单权限 2接口权限 3页面元素 4开放接口
     */
    @TableField("purview_type")
    private String purviewType;

    @TableField("purview_code")
    private String purviewCode;

    @TableField("url")
    private String url;

    @TableField("component")
    private String component;

    @TableField("icon")
    private String icon;

    @TableField("parent_id")
    private Long parentId;

    /**
     * 例如/1/3/4
     */
    @TableField("path")
    private String path;

    @TableField("order_num")
    private Integer orderNum;

    @TableField("hidden")
    private Boolean hidden;

    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
