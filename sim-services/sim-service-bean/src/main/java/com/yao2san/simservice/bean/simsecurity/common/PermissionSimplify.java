package com.yao2san.simservice.bean.simsecurity.common;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PermissionSimplify {
    /**
     * 权限ID
     */
    private Long id;
    /**
     * 权限名称
     */
    private String purviewName;
    /**
     * 权限编码
     */
    private String purviewCode;
    /**
     * 权限类型 1菜单权限 2接口权限 3页面元素
     */
    private String purviewType;
    private String url;
    /**
     * 权限图标
     */
    private String icon;
    /**
     * 页面组件路径
     */
    private String component;
    /**
     * 上级ID
     */
    private Long parentId;
    /**
     * 生效时间
     */
    private String effDate;
    /**
     * 失效时间
     */
    private String expDate;
    /**
     * 排序
     */
    private Integer orderNum;
    /**
     * 是否隐藏
     */
    private Boolean hidden;
    /**
     * 路径
     */
    private String path;

}
