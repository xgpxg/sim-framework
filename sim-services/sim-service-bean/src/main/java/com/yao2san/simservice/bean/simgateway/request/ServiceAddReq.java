package com.yao2san.simservice.bean.simgateway.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ServiceAddReq {
    @NotNull(groups = Update.class)
    private Long serviceId;

    private String name;

    private String url;

    private String type;

    private Long routeId;

    private Long serviceGroupId;

    private String description;

    public interface Update{}
}
