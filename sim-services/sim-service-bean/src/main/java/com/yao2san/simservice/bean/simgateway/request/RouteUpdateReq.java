package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.simservice.entity.simgateway.Route;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class RouteUpdateReq extends Route {
    @NotNull(message = "id can not be null")
    private Long id;
}
