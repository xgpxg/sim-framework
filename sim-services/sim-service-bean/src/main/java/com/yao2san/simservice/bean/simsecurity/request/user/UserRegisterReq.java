package com.yao2san.simservice.bean.simsecurity.request.user;

import com.yao2san.simservice.enums.user.LoginType;
import com.yao2san.simservice.enums.user.RegisterType;
import lombok.Data;

/**
 * 用户注册请求参数
 *
 * @author wxg
 */
@Data
public class UserRegisterReq {
    /**
     * 注册方式
     *
     * @see RegisterType
     */
    private Integer loginType;

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    /**
     * 短信验证码
     */
    private String verifyCode;
}
