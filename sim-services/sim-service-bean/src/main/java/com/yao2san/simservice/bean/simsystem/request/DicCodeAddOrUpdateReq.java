package com.yao2san.simservice.bean.simsystem.request;

import com.yao2san.simservice.entity.simsystem.SysDicCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DicCodeAddOrUpdateReq extends SysDicCode {

}
