package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import com.yao2san.simservice.entity.simgateway.Service;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceAddOrUpdateReq extends Service {

}
