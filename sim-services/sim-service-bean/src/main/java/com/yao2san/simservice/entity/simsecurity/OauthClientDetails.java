package com.yao2san.simservice.entity.simsecurity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("oauth_client_details")
public class OauthClientDetails extends Model<OauthClientDetails> {

    private static final long serialVersionUID = 1L;

    @TableId("CLIENT_ID")
    private String clientId;

    @TableField("RESOURCE_IDS")
    private String resourceIds;

    @TableField("CLIENT_SECRET")
    private String clientSecret;

    @TableField("SCOPE")
    private String scope;

    @TableField("AUTHORIZED_GRANT_TYPES")
    private String authorizedGrantTypes;

    @TableField("WEB_SERVER_REDIRECT_URI")
    private String webServerRedirectUri;

    @TableField("AUTHORITIES")
    private String authorities;

    @TableField("ACCESS_TOKEN_VALIDITY")
    private Integer accessTokenValidity;

    @TableField("REFRESH_TOKEN_VALIDITY")
    private Integer refreshTokenValidity;

    @TableField("ADDITIONAL_INFORMATION")
    private String additionalInformation;

    @TableField("AUTOAPPROVE")
    private String autoapprove;


    @Override
    public Serializable pkVal() {
        return this.clientId;
    }

}
