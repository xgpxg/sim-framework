package com.yao2san.simservice.bean.simopen.response;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wxg
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OpenAppServiceRes extends BaseBean {
    private Long id;
    private Long serviceId;
    private Long userId;
    private String serviceName;
    private String serviceType;
    private String isAuth;
    private String url;
    private String effDate;

}
