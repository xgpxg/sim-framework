package com.yao2san.simservice.enums.user;

import com.yao2san.sim.framework.web.exception.BusiException;
import lombok.Getter;

/**
 * 身份认证类型
 *
 * @author wxg
 */

public enum IdentityType {
    /**
     * 用户名
     */
    USERNAME(1),
    /**
     * 手机号
     */
    PHONE_NUMBER(2),
    /**
     * 微信openid
     */
    OPENID(2),
    ;


    @Getter
    private final int type;

    IdentityType(int type) {
        this.type = type;
    }

    public static IdentityType of(int type) {
        for (IdentityType value : IdentityType.values()) {
            if (value.type == type) {
                return value;
            }
        }
        throw new BusiException("不支持的身份类型");
    }
}
