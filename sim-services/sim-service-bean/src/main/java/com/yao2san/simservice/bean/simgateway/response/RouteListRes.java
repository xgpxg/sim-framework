package com.yao2san.simservice.bean.simgateway.response;

import com.yao2san.simservice.entity.simgateway.Route;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RouteListRes extends Route {
    /**
     * 所属应用名称
     */
    private String appName;
}
