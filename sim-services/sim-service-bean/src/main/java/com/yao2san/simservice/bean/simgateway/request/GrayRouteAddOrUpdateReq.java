package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.simservice.entity.simgateway.RouteGray;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class GrayRouteAddOrUpdateReq extends RouteGray {

}
