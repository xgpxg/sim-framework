package com.yao2san.simservice.bean.simopen.response;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class OpenAppDetailRes extends BaseBean {
    private Long appInstId;
    private String appName;
    private String openId;
    private String secretKey;
    private List<ServicePurviewRes> services;
}
