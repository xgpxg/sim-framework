package com.yao2san.simservice.entity.simsystem;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_config_his")
public class SysConfigHis extends Model<SysConfigHis> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("config_id")
    private Long configId;

    @TableField("key")
    private String key;

    @TableField("value")
    private String value;

    @TableField("application")
    private String application;

    @TableField("profile")
    private String profile;

    @TableField("label")
    private String label;

    @TableField("version")
    private String version;

    @TableField("sort")
    private Integer sort;

    @TableField("description")
    private String description;

    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
