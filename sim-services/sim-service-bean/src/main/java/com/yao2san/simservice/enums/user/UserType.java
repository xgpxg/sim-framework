package com.yao2san.simservice.enums.user;

import lombok.Getter;

/**
 * 用户类型
 *
 * @author wxg
 */
public enum UserType {
    /**
     * 超级管理员
     */
    SUPER_ADMIN(0),
    /**
     * 平台管理员
     */
    PLATFORM_ADMIN(1),
    /**
     * 平台用户
     */
    PLATFORM_USER(2),
    /**
     * 平台租户
     */
    PLATFORM_TENANT(3),
    /**
     * 外部用户
     */
    EXTERNAL_USER(4),
    ;

    @Getter
    private final int type;

    UserType(int type) {
        this.type = type;
    }
}
