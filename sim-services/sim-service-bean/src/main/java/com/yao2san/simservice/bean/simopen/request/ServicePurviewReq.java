package com.yao2san.simservice.bean.simopen.request;

import lombok.Data;

import java.util.List;

/**
 * 服务授权请求
 *
 * @author wxg
 */
@Data
public class ServicePurviewReq {
    /**
     * 服务授权ID
     */
    private Long id;
    /**
     * 授权对象类型
     */
    private String objectType;
    /**
     * 授权对象ID
     */
    private Long objectId;
    /**
     * 服务类型
     */
    private String serviceType;
    /**
     * 服务名称
     */
    private String serviceName;
    /**
     * 服务ID列表
     */
    private List<Long> services;
    /**
     * 是否已授权 0未授权 1已授权
     */
    private Integer isAuth;
    /**
     * 操作类型: 1新增授权 2取消授权
     */
    private Integer option;
}
