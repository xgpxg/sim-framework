package com.yao2san.simservice.entity.simsecurity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 组织机构属性定义
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("org_attr_code")
public class OrgAttrCode extends Model<OrgAttrCode> {

    private static final long serialVersionUID = 1L;

    /**
     * 机构标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 属性编码
     */
    @TableField("attr_code")
    private String attrCode;

    /**
     * 属性名称
     */
    @TableField("attr_name")
    private String attrName;

    /**
     * 数据类型，code=DATA_TYPE
     */
    @TableField("data_type")
    private String dataType;

    /**
     * 是否必须：code=YES_NO
     */
    @TableField("required")
    private String required;

    /**
     * 最小长度
     */
    @TableField("min_len")
    private Integer minLen;

    /**
     * 最大长度
     */
    @TableField("max_len")
    private Integer maxLen;

    @TableField("description")
    private String description;

    /**
     * 状态
     */
    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
