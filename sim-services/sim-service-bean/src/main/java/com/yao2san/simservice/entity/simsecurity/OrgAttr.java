package com.yao2san.simservice.entity.simsecurity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 组织机构属性
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("org_attr")
public class OrgAttr extends Model<OrgAttr> {

    private static final long serialVersionUID = 1L;

    /**
     * 机构属性标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 机构标识
     */
    @TableField("org_id")
    private Long orgId;

    /**
     * 机构编码定义标识
     */
    @TableField("org_attr_code_id")
    private Long orgAttrCodeId;

    /**
     * 数据值
     */
    @TableField("data_value")
    private String dataValue;

    /**
     * 状态
     */
    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
