package com.yao2san.simservice.bean.simsecurity.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserListReq extends Pagination {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户联系号码
     */
    private String phone;

    /**
     * 用户邮箱
     */
    private String email;

    @JsonIgnore
    private transient String password;

    private String userType;

    private String effDate;

    private String expDate;

    private Integer notUpdate;

    private String description;
    /**
     * 头像
     */
    private String avatar;

    private String status;

    @JsonIgnore
    private String filterText;
    @JsonIgnore
    private String isAuthorized;

    private String openId;

    private String secretKey;

    private Long orgId;

}
