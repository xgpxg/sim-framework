package com.yao2san.simservice.entity.simsecurity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("open_id")
    private String openId;

    @TableField("user_name")
    private String userName;

    @TableField("nick_name")
    private String nickName;

    /**
     * DIC_CODE=USER_TYPE
     */
    @TableField("user_type")
    private Integer userType;

    @TableField("phone")
    private String phone;

    @TableField("email")
    private String email;

    @TableField("device_id")
    private String deviceId;

    /**
     * 生效时间
     */
    @TableField(value = "eff_date", updateStrategy = FieldStrategy.IGNORED)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime effDate;

    /**
     * 过期时间
     */
    @TableField(value = "exp_date", updateStrategy = FieldStrategy.IGNORED)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime expDate;

    @TableField("not_update")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Integer notUpdate;

    @TableField("secret_key")
    private String secretKey;

    /**
     * 头像
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 用户来源 1后台创建 2平台注册 3外部系统接入
     */
    @TableField("source")
    private Integer source;

    @TableField("description")
    private String description;

    @TableField("status")
    private Integer status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
