package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.simservice.entity.simgateway.RouteApp;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppAddOrUpdateReq extends RouteApp {

}
