package com.yao2san.simservice.bean.simsecurity.response.user;

import com.yao2san.simservice.entity.simsecurity.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleListRes extends Role {
    /**
     * 状态名称
     */
    private String statusName;
    /**
     * 创建人名称
     */
    private String createUserName;
    /**
     * 角色下用户数量
     */
    private Long userCount;
}
