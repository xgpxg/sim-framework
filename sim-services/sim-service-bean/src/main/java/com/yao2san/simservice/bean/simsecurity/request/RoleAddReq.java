package com.yao2san.simservice.bean.simsecurity.request;

import com.yao2san.simservice.entity.simsecurity.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class RoleAddReq extends Role {
}
