package com.yao2san.simservice.entity.simgateway;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("route_metrics")
public class RouteMetrics extends Model<RouteMetrics> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("route_id")
    private Long routeId;

    @TableField("metrics_name")
    private String metricsName;

    @TableField("all_count")
    private String allCount;

    @TableField("success_count")
    private String successCount;

    @TableField("error_count")
    private Double errorCount;

    @TableField("mean_rate")
    private Double meanRate;

    @TableField("mean_time")
    private Double meanTime;

    @TableField("min_time")
    private Double minTime;

    @TableField("max_time")
    private Double maxTime;

    @TableField("m1_rate")
    private Double m1Rate;

    @TableField("m5_rate")
    private Double m5Rate;

    @TableField("m15_rate")
    private Double m15Rate;

    @TableField("p75_less_time")
    private Double p75LessTime;

    @TableField("p95_less_time")
    private Double p95LessTime;

    @TableField("p99_less_time")
    private Double p99LessTime;

    @TableField("p999_less_time")
    private Double p999LessTime;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;



    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
