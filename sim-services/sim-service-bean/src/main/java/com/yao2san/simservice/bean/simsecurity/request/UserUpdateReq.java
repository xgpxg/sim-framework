package com.yao2san.simservice.bean.simsecurity.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yao2san.sim.framework.web.bean.Pagination;
import com.yao2san.simservice.entity.simsecurity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserUpdateReq extends User {

    private Long orgId;

}
