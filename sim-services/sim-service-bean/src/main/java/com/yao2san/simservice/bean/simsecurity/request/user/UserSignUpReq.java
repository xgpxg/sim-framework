package com.yao2san.simservice.bean.simsecurity.request.user;

import lombok.Data;

/**
 * 用户注册请求参数
 *
 * @author wxg
 */
@Data
public class UserSignUpReq {
    /**
     * 身份类型：1用户名 2手机号 3微信openid
     *
     * @see com.yao2san.simservice.enums.user.IdentityType
     */
    private Integer type;
    /**
     * 身份标识
     */
    private String identity;
    /**
     * 密钥
     */
    private String secret;

}
