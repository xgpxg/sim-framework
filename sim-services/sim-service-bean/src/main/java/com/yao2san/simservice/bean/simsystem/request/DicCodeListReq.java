package com.yao2san.simservice.bean.simsystem.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DicCodeListReq extends Pagination {
    private String groupName;
    private String filterText;
}
