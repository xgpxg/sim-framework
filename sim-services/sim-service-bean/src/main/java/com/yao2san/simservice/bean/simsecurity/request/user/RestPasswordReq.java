package com.yao2san.simservice.bean.simsecurity.request.user;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 管理员重置用户和密码请求
 */
@Data
public class RestPasswordReq {
    /**
     * 用户ID
     */
    @NotNull(message = "用户ID不能为空")
    private Long id;
    /**
     * 新密码
     */
    @NotEmpty(message = "密码不能为空")
    private String password;
}
