package com.yao2san.simservice.entity.simgateway;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("service_purview")
public class ServicePurview extends Model<ServicePurview> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("service_id")
    private Long serviceId;

    /**
     * 授权对象类型:user用户，app应用
     */
    @TableField("object_type")
    private String objectType;

    /**
     * 授权对象ID
     */
    @TableField("object_id")
    private Long objectId;

    @TableField("eff_date")
    private LocalDateTime effDate;

    @TableField("exp_date")
    private LocalDateTime expDate;

    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
