package com.yao2san.simservice.bean.simopen.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 服务列表查询请求
 *
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ServiceListReq extends Pagination {
    /**
     * 授权对象类型
     */
    private String objectType;
    /**
     * 授权对象ID
     */
    private Long objectId;
    /**
     * 服务类型
     */
    private String serviceType;
    /**
     * 服务名称
     */
    private String serviceName;
    /**
     * 是否已授权 0未授权 1已授权
     */
    private Integer isAuth;
    /**
     * 操作类型: 1新增授权 2取消授权
     */
    private Integer option;
}
