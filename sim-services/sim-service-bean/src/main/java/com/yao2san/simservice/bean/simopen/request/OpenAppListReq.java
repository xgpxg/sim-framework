package com.yao2san.simservice.bean.simopen.request;

import lombok.Data;

/**
 * @author wxg
 */
@Data
public class OpenAppListReq {
    private String appName;
    private Long createUser;
}
