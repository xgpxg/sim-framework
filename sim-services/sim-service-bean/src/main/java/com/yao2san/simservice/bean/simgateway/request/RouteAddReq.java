package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.simservice.entity.simgateway.Route;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Data
public class RouteAddReq extends Route {
    @NotEmpty(message = "routeName can not be empty")
    private String routeName;

    @NotNull(message = "routeAppId can not be null")
    private Long routeAppId;

    private String version;

    private String status;

    private String type;

    private String prefix;

    @NotEmpty(message = "path can not be empty")
    private String path;

    private String serviceId;

    private String url;

    private Boolean stripPrefix = true;

    private Boolean retryable = false;

    private Boolean customSensitiveHeaders = false;

    private Boolean isPublic = false;

    private String publicPrefix;

    /**
     * query param
     */
    private String filterText;

}
