package com.yao2san.simservice.bean.simsecurity.response;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;
/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgDetailRes extends Pagination {
}
