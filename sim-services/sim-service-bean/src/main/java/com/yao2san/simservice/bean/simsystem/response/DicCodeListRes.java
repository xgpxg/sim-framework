package com.yao2san.simservice.bean.simsystem.response;

import com.yao2san.simservice.entity.simsystem.SysDicCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DicCodeListRes extends SysDicCode {
    /**
     * 字典项数量
     */
    private Integer dicItemCount;
}
