package com.yao2san.simservice.bean.simgateway.response;

import com.yao2san.simservice.entity.simgateway.RouteApp;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AppListRes extends RouteApp {
    private Long routeAppId;
    private String appName;
    private String appCode;
    private Integer routeCount;
}
