package com.yao2san.simservice.bean.simsecurity.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户角色
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleSimplify {
    private Long roleId;
    private String roleName;
    private String roleCode;
    private Integer notUpdate;
}
