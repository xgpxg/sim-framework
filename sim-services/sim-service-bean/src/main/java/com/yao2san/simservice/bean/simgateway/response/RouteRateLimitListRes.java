package com.yao2san.simservice.bean.simgateway.response;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RouteRateLimitListRes extends Pagination {
    private Long id;

    private Long routeId;

    private String routeName;

    private Long refreshInterval;

    private Long limit;

    private Long quota;

    private boolean breakOnMatch;

    private String matchType;

    private String matcher;

    private String path;

    private String limitScope;


}
