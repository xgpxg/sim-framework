package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
public class RouteListReq extends Pagination {
    private String routeName;

    private Integer routeAppId;

    private String version;

    private String status;

    private String type;

    private String prefix;

    private String path;

    private String serviceId;

    private String url;

    private boolean retryable = false;



    private Boolean isPublic = false;


    /**
     * query param
     */
    private String filterText;


}
