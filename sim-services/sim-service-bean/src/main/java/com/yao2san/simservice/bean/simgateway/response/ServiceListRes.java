package com.yao2san.simservice.bean.simgateway.response;

import com.yao2san.simservice.entity.simgateway.Service;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceListRes extends Service {
    private String groupName;

}
