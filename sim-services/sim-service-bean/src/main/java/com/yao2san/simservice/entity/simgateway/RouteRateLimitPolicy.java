package com.yao2san.simservice.entity.simgateway;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("route_rate_limit_policy")
public class RouteRateLimitPolicy extends Model<RouteRateLimitPolicy> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("route_id")
    private Long routeId;

    @TableField("`limit`")
    private Long limit;

    @TableField("quota")
    private Long quota;

    @TableField("refresh_interval")
    private Long refreshInterval;

    @TableField("match_type")
    private String matchType;

    @TableField("break_on_match")
    private Boolean breakOnMatch;

    /**
     * 限流作用范围,global全局 special指定的,默认special
     */
    @TableField("limit_scope")
    private String limitScope;

    /**
     * 匹配对象
     */
    @TableField("matcher")
    private String matcher;

    /**
     * '策略描述'
     */
    @TableField("description")
    private String description;

    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
