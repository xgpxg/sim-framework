package com.yao2san.simservice.bean.simsecurity.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yao2san.sim.framework.web.bean.Pagination;
import com.yao2san.simservice.bean.simsecurity.common.Permission;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户角色
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserRoleRes extends Pagination {
    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 是否禁止更新
     */
    private Integer notUpdate;
    /**
     * 生效时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime effDate;
    /**
     * 失效时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime expDate;
    //private List<Permission> permissions;

}
