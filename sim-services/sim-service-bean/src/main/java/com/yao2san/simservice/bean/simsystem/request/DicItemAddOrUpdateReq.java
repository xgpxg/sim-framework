package com.yao2san.simservice.bean.simsystem.request;

import com.yao2san.simservice.entity.simsystem.SysDicItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DicItemAddOrUpdateReq extends SysDicItem {

}
