package com.yao2san.simservice.enums.user;

import com.yao2san.sim.framework.web.exception.BusiException;
import lombok.Getter;

public enum RegisterSource {
    /**
     * 后台创建
     */
    SYSTEM_CREATE(1, "后台创建"),
    /**
     * 平台注册
     */
    SYSTEM_REGISTER(2, "平台注册"),
    /**
     * 外部系统注册
     */
    EXTERNAL_REGISTER(3, "外部系统注册"),
    ;

    @Getter
    private final int code;
    @Getter
    private final String name;


    RegisterSource(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static RegisterSource of(int code) {
        for (RegisterSource loginType : RegisterSource.values()) {
            if (loginType.getCode() == code) {
                return loginType;
            }
        }
        throw new BusiException("未知的用户注册来源：" + code);
    }
}
