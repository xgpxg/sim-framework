package com.yao2san.simservice.entity.simgateway;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("route_gray")
public class RouteGray extends Model<RouteGray> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("route_id")
    private Long routeId;

    @TableField("group_id")
    private Long groupId;

    @TableField("gray_route_name")
    private String grayRouteName;

    @TableField("service_id")
    private String serviceId;

    @TableField("service_instances")
    private String serviceInstances;

    @TableField("path")
    private String path;

    @TableField("main_version")
    private String mainVersion;

    @TableField("gray_version")
    private String grayVersion;

    /**
     * WEIGHT_RANDOM/PARAM/HEADER
     */
    @TableField("rule_type")
    private String ruleType;

    @TableField("weight")
    private String weight;

    /**
     * json对象
     */
    @TableField("headers")
    private String headers;

    /**
     * json对象
     */
    @TableField("params")
    private String params;

    /**
     * json对象
     */
    @TableField("cookies")
    private String cookies;

    /**
     * 1200 未生效 1000正常 1300停用 1100已删除
     */
    @TableField("status")
    private String status;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;
    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
