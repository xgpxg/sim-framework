package com.yao2san.simservice.enums.user;

import com.yao2san.sim.framework.web.exception.BusiException;
import lombok.Getter;

/**
 * 登录方式
 *
 * @author wxg
 */
public enum RegisterType {
    /**
     * 用户名密码
     */
    UN(0),

    /**
     * 手机号
     */
    PHONE(1),

    /**
     * 微信
     */
    WX(2),

    /**
     * 小程序
     */
    WX_XCX(3);

    @Getter
    private final int type;


    RegisterType(Integer type) {
        this.type = type;
    }

    public static RegisterType of(int type) {
        for (RegisterType loginType : RegisterType.values()) {
            if (loginType.getType() == type) {
                return loginType;
            }
        }
        throw new BusiException("不支持的注册类型：" + type);
    }
}
