package com.yao2san.simservice.bean.simsecurity.request.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户登录请求参数
 *
 * @author wxg
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSignInReq {
    /**
     * 身份类型：1用户名 2手机号 3微信openid
     *
     * @see com.yao2san.simservice.enums.user.IdentityType
     */
    private Integer type;
    /**
     * 身份标识
     */
    private String identity;
    /**
     * 密钥
     */
    private String secret;

}
