package com.yao2san.simservice.bean.simgateway.response;

import com.yao2san.simservice.entity.simgateway.Route;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class RouteDetailRes extends Route {

}
