package com.yao2san.simservice.bean.simopen.request;

import com.yao2san.simservice.entity.simopen.OpenServicePurview;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 服务授权请求
 *
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ServicePurviewAddOrUpdateReq extends OpenServicePurview {
    /**
     * 服务ID列表
     */
    private List<Long> services;
    /**
     * 是否已授权 0未授权 1已授权
     */
    private Integer isAuth;
    /**
     * 操作类型: 1新增授权 2取消授权
     */
    private Integer option;
}
