package com.yao2san.simservice.entity.simgateway;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("rate")
public class Rate extends Model<Rate> {

    private static final long serialVersionUID = 1L;

    @TableId("key")
    private String key;

    @TableField("remaining")
    private Long remaining;

    @TableField("remaining_quota")
    private Long remainingQuota;

    @TableField("reset")
    private Long reset;

    @TableField("expiration")
    private LocalDateTime expiration;


    @Override
    public Serializable pkVal() {
        return this.key;
    }

}
