package com.yao2san.simservice.enums.user;

import lombok.Getter;

/**
 * 用户状态
 *
 * @author wxg
 */
public enum UserStatus {
    /**
     * 未生效
     */
    NOT_EFFECTIVE(0, "未生效"),
    /**
     * 正常
     */
    OK(1, "正常"),
    /**
     * 已停用
     */
    DISABLED(7, "已停用"),
    /**
     * 已冻结
     */
    FREEZE(8, "已冻结"),
    /**
     * 已删除
     */
    DELETED(9, "已删除"),
    ;

    @Getter
    private final int code;

    @Getter
    private final String description;

    UserStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }
}
