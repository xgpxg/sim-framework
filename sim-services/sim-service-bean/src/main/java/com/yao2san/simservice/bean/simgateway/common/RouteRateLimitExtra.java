package com.yao2san.simservice.bean.simgateway.common;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RouteRateLimitExtra extends RouteExtra {
    private Long routeId;

    private Long routeRateLimitPolicyId;

    private Long refreshInterval;

    private Long limit;

    private Long quota;

    private boolean breakOnMatch;

    private String matchType;

    private String matcher;

    private String serviceId;
}
