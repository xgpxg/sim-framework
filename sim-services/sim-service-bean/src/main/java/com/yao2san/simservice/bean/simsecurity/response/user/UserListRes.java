package com.yao2san.simservice.bean.simsecurity.response.user;

import com.yao2san.simservice.entity.simsecurity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserListRes extends User {
    /**
     * 状态名称
     */
    private String statusName;

    /**
     * 用户类型名称
     */
    private String userTypeName;

    /**
     * 角色
     */
    private String roles;

}
