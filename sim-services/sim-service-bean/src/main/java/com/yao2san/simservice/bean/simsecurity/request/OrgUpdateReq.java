package com.yao2san.simservice.bean.simsecurity.request;

import com.yao2san.sim.framework.web.bean.BaseBean;
import com.yao2san.simservice.entity.simsecurity.Org;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgUpdateReq extends Org {
    @NotNull(message = "orgId不能为空")
    private Long orgId;
    private Long parentId;
    private String orgName;
    private String orgCode;
    private String orgType;
    private String description;
    private List<OrgAttr> attrs;
}
