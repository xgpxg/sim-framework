package com.yao2san.simservice.entity.simgateway;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("route")
public class Route extends Model<Route> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("route_name")
    private String routeName;

    @TableField("route_app_id")
    private Long routeAppId;

    @TableField("service_id")
    private String serviceId;

    @TableField("url")
    private String url;

    @TableField("type")
    private String type;

    @TableField("prefix")
    private String prefix;

    @TableField("path")
    private String path;

    @TableField("strip_prefix")
    private Boolean stripPrefix;

    @TableField("retryable")
    private Boolean retryable;

    @TableField("sensitive_headers")
    private String sensitiveHeaders;

    /**
     * 是否对外路由
     */
    @TableField("is_public")
    private Boolean isPublic;

    /**
     * 对外访问前缀
     */
    @TableField("public_prefix")
    private String publicPrefix;

    /**
     * 1200 未生效 1000正常 1300停用 1100已删除
     */
    @TableField("status")
    private String status;

    @TableField("version")
    private String version;

    @TableField("custom_sensitive_headers")
    private Boolean customSensitiveHeaders;

    @TableField("create_user")
    private Long createUser;

    @TableField("update_user")
    private Long updateUser;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;
    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
