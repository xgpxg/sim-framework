package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class GrayRouteListReq extends Pagination {
    private String status;

    private String path;

    private String filterText;

}
