package com.yao2san.simservice.bean.simopen.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yao2san.simservice.entity.simopen.OpenApp;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

/**
 * @author wxg
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OpenAppAddOrUpdateReq extends OpenApp {
    private Long id;
    @NotEmpty(message = "appName not be null")
    private String appName;
    private Set<Long> serviceIds;
    private String openId;
    @JsonIgnore
    private String secretKey;
}
