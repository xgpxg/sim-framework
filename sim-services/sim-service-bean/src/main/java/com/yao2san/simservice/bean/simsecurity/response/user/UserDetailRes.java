package com.yao2san.simservice.bean.simsecurity.response.user;

import com.yao2san.simservice.entity.simsecurity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserDetailRes extends User {
}
