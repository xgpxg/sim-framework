package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.simservice.entity.simgateway.RouteRateLimitPolicy;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class RouteRateLimitAddOrUpdateReq extends RouteRateLimitPolicy {

}
