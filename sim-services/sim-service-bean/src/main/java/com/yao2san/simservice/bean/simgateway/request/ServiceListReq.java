package com.yao2san.simservice.bean.simgateway.request;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServiceListReq extends Pagination {
    /**
     * 服务类型
     */
    private String type;
    /**
     * 服务名称
     */
    private String name;
    /**
     * 服务分组ID
     */
    private Long serviceGroupId;

}
