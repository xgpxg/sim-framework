package com.yao2san.simservice.bean.simsystem.response;

import com.yao2san.simservice.entity.simsystem.SysDicItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DicItemListRes extends SysDicItem {
    private String dicCodeName;
}
