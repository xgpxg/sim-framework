package com.yao2san.simservice.bean.simsecurity.request.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 获取AccessToken结果
 *
 * @author wxg
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenResponse {
    /**
     * AccessToken
     */
    private String accessToken;
    /**
     * RefreshToken
     */
    private String refreshToken;
    /**
     * 过期时间(秒)
     */
    private long expireIn;
}
