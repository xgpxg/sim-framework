package com.yao2san.simservice.entity.simsystem;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author wxg
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic_item")
public class SysDicItem extends Model<SysDicItem> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Long id;

    @TableField("sys_dic_code_id")
    private Long sysDicCodeId;


    /**
     * 字典项
     */
    @TableField("item_code")
    private String itemCode;

    /**
     * 字典值
     */
    @TableField("item_text")
    private String itemText;

    @TableField("item_value")
    private String itemValue;

    /**
     * 字典分组
     */
    @TableField("dic_code")
    private String dicCode;

    /**
     * 说明
     */
    @TableField("description")
    private String description;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 状态：1000正常 2000 删除
     */
    @TableField("status")
    private String status;

    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createDate;

    @TableField("update_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    @TableField("tenant_id")
    private Long tenantId;

    @TableField("is_delete")
    private Integer isDelete;

    @Override
    public Serializable pkVal() {
        return null;
    }

}
