package com.yao2san.sim.open.client.bean.response;

import lombok.Data;

/**
 * 获取AccessToken结果
 *
 * @author wxg
 */
@Data
public class AccessTokenResponse {
    /**
     * AccessToken
     */
    private String accessToken;
    /**
     * RefreshToken
     */
    private String refreshToken;
    /**
     * 过期时间(秒)
     */
    private long expireIn;
}
