package com.yao2san.sim.open.client.common;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class ResponsePageData<T> {
    private long pageNum = 1;
    private long pageSize = 10;
    private long size;
    private long total;
    private long pages;
    private List<T> list;
}
