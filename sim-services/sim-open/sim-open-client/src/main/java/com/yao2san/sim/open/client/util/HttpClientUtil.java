package com.yao2san.sim.open.client.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yao2san.sim.open.client.api.OpenApiClient;
import com.yao2san.sim.open.client.common.ResponseData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class HttpClientUtil {
    private static final CloseableHttpClient CLIENT = HttpClients.createMinimal();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static <T> ResponseData<T> get(String server, String path, Object urlParam) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(server);
            uriBuilder.setPath(path);
            if (urlParam != null) {
                Map<String, String> paramMap = toMap(urlParam);
                if (paramMap != null && paramMap.size() > 0) {
                    for (Map.Entry<String, String> entry : paramMap.entrySet()) {
                        uriBuilder.setParameter(entry.getKey(), entry.getValue());
                    }
                }
            }
            URI uri = uriBuilder.build();
            HttpGet httpget = new HttpGet(uri);
            httpget.setHeader("Content-Type", "UTF-8");

            CloseableHttpResponse response = CLIENT.execute(httpget);

            HttpEntity entity = response.getEntity();
            String s = EntityUtils.toString(entity, StandardCharsets.UTF_8);

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                return ResponseData.error(s);
            }

            return OBJECT_MAPPER.readValue(s, new TypeReference<ResponseData<T>>() {
            });
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

    }


    public static void main(String[] args) throws IOException {
        OpenApiClient apiClient = new OpenApiClient("http://localhost:9527/api/auth/userManage?filterText=&pageSize=101", "1", "2");

    }

    public static Map<String, String> toMap(Object object) {
        try {
            return OBJECT_MAPPER.readValue(OBJECT_MAPPER.writeValueAsString(object), new TypeReference<Map<String, String>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
