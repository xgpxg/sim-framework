package com.yao2san.sim.open.client.constants;

public class ApiAddr {
    public final static String BASE = "https://localhost:7000";
    public final static String LOGIN = "/login";
    public final static String APP_AUTH = "/oauth/appAuth";
}
