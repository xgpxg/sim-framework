package com.yao2san.sim.open.client.bean.enums;

import lombok.Getter;

/**
 * 认证对象
 *
 * @author wxg
 */

public enum AuthObject {
    /**
     * 应用
     */
    APP("app"),
    /**
     * 用户
     */
    USER("user"),
    ;
    @Getter
    private final String code;

    AuthObject(String code) {
        this.code = code;
    }
}
