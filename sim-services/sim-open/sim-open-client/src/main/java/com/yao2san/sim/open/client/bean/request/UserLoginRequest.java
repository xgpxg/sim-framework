package com.yao2san.sim.open.client.bean.request;

import lombok.Data;

/**
 * 用户登录请求参数
 *
 * @author wxg
 */
@Data
public class UserLoginRequest {
    private String username;
    private String password;
}
