package com.yao2san.sim.open.client.bean.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 认证请求参数
 *
 * @author wxg
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAccessTokenRequest {
    /**
     * 认证对象：app应用，user用户
     */
    private String authObject;
    private String appId;
    private String appSecret;
}
