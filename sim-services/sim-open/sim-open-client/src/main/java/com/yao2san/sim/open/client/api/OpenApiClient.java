package com.yao2san.sim.open.client.api;

import com.yao2san.sim.open.client.bean.enums.AuthObject;
import com.yao2san.sim.open.client.bean.request.GetAccessTokenRequest;
import com.yao2san.sim.open.client.bean.response.AccessTokenResponse;
import com.yao2san.sim.open.client.common.ResponseData;
import com.yao2san.sim.open.client.constants.ApiAddr;
import com.yao2san.sim.open.client.util.HttpClientUtil;
import lombok.Getter;
import org.apache.http.auth.AuthenticationException;

import java.io.IOException;
import java.rmi.RemoteException;

public class OpenApiClient {
    @Getter
    private final String endpoint;
    @Getter
    private final String appId;
    @Getter
    private final String appSecret;

    private AccessTokenResponse cachedAccessToken;

    public OpenApiClient(String endpoint, String appId, String appSecret) throws IOException {
        this.endpoint = endpoint;
        this.appId = appId;
        this.appSecret = appSecret;

        ResponseData<AccessTokenResponse> result = this.getAccessToken(appId, appSecret);
        if (result.getCode() == 0) {
            this.cachedAccessToken = result.getData();
        } else {
            throw new RemoteException("认证失败, code=" + result.getCodeDesc() + ", message:" + result.getMsg());
        }
    }

    public ResponseData<?> login() {
        return null;
    }

    /**
     * 创建信息用户
     *
     * @return result
     */
    public ResponseData<?> crateUser() {
        return null;
    }

    /**
     * 获取AccessToken
     *
     * @param appId     应用ID
     * @param appSecret 应用密钥
     * @return access token
     * @throws IOException IOException
     */
    public ResponseData<AccessTokenResponse> getAccessToken(String appId, String appSecret) throws IOException {
        return HttpClientUtil.get(endpoint, ApiAddr.APP_AUTH, new GetAccessTokenRequest(appId, appSecret, AuthObject.APP.getCode()));
    }


}
