package com.yao2san.sim.open.server.busi.app.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yao2san.sim.framework.common.auth.util.UserUtil;
import com.yao2san.sim.framework.utils.CommonUtil;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.simservice.bean.simopen.request.OpenAppListReq;
import com.yao2san.simservice.bean.simopen.request.ServiceListReq;
import com.yao2san.simservice.bean.simopen.response.OpenAppDetailRes;
import com.yao2san.simservice.bean.simopen.response.OpenAppListRes;
import com.yao2san.simservice.bean.simopen.response.ServicePurviewRes;
import com.yao2san.sim.open.server.busi.app.service.OpenAppService;
import com.yao2san.simservice.bean.simopen.request.OpenAppAddOrUpdateReq;
import com.yao2san.simservice.entity.simopen.OpenApp;
import com.yao2san.simservice.entity.simopen.OpenServicePurview;
import com.yao2san.simservice.mapper.simopen.OpenAppMapper;
import com.yao2san.simservice.mapper.simopen.OpenServicePurviewMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author wxg
 */
@Service
public class OpenAppServiceImpl extends ServiceBaseImpl<OpenAppMapper, OpenApp> implements OpenAppService {
    private final OpenAppMapper openAppMapper;
    private final OpenServicePurviewMapper openServicePurviewMapper;

    public OpenAppServiceImpl(OpenAppMapper openAppMapper, OpenServicePurviewMapper openServicePurviewMapper) {
        this.openAppMapper = openAppMapper;
        this.openServicePurviewMapper = openServicePurviewMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData<Void> add(OpenAppAddOrUpdateReq req) {
        req.setOpenId(makeKey());
        req.setSecretKey(makeKey());
        req.setCreateUser(UserUtil.getCurrUserId());
        save(req);
        return ResponseData.success();
    }

    @Override
    public ResponseData<List<OpenAppListRes>> list(OpenAppListReq req) {
        req.setCreateUser(UserUtil.getCurrUserId());
        List<OpenAppListRes> list = openAppMapper.list(req);
        return ResponseData.success(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(OpenAppAddOrUpdateReq req) {
        removeById(req.getId());
        openServicePurviewMapper.delete(Wrappers.lambdaQuery(OpenServicePurview.class)
                .eq(OpenServicePurview::getObjectType, "app")
                .eq(OpenServicePurview::getObjectId, req.getId())
                .eq(OpenServicePurview::getCreateUser, UserUtil.getCurrUser()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(OpenAppAddOrUpdateReq req) {
        if (req.getId() == null) {
            throw new BusiException("appInstId can not be null");
        }
        this.updateById(req);
    }

    @Override
    public ResponseData<OpenAppDetailRes> detail(Long appInstId) {
        OpenAppAddOrUpdateReq req = new OpenAppAddOrUpdateReq();
        req.setId(appInstId);
        if (req.getId() == null) {
            throw new BusiException("appInstId can not be null");
        }
        OpenAppDetailRes detail = openAppMapper.detail(appInstId);
        ServiceListReq serviceListReq = new ServiceListReq();
        serviceListReq.setCreateUser(UserUtil.getCurrUserId());
        List<ServicePurviewRes> purviewServices = openServicePurviewMapper.qryAuthServices(null, serviceListReq);
        detail.setServices(purviewServices);

        return ResponseData.success(detail);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void restSecretKey(OpenAppAddOrUpdateReq req) {
        req.setSecretKey(makeKey());
        this.updateById(new OpenApp().setId(req.getId()).setSecretKey(req.getSecretKey()));
    }


    private String makeKey() {
        return CommonUtil.md5(UUID.randomUUID().toString());
    }
}
