package com.yao2san.sim.open.server.busi.app.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simopen.request.ServiceListReq;
import com.yao2san.simservice.bean.simopen.request.ServicePurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simopen.response.ServicePurviewRes;
import com.yao2san.sim.open.server.busi.app.service.ServicePurviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author wxg
 */
@RestController
@RequestMapping("service-purview")
public class OpenServicePurviewController {

    @Autowired
    private ServicePurviewService servicePurviewService;

    /**
     * 查询应用已授权或可授权的服务
     */
    @GetMapping("/service")
    public ResponseData<ResponsePageData<ServicePurviewRes>> services(@Validated ServiceListReq req) {
        return ResponseData.success(servicePurviewService.services(req));
    }

    /**
     * 服务授权or解除授权
     */
    @PostMapping("/service")
    public ResponseData<Void> serviceAuthorization(@RequestBody ServicePurviewAddOrUpdateReq req) {
        servicePurviewService.serviceAuthorization(req);
        return ResponseData.success();
    }

}
