package com.yao2san.sim.open.server.entity;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wxg
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class AppInst extends BaseBean {
    private Long appInstId;
    private String appName;
    private String openId;
    private String secretKey;

}
