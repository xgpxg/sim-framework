package com.yao2san.sim.open.server.busi.app.service;

import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simopen.request.ServiceListReq;
import com.yao2san.simservice.bean.simopen.request.ServicePurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simopen.response.ServicePurviewRes;
import com.yao2san.simservice.bean.simopen.request.ServicePurviewReq;

/**
 * @author wxg
 */
public interface ServicePurviewService {
    /**
     * 服务授权/解除授权
     *
     * @param req 授权参数
     */
    void serviceAuthorization(ServicePurviewAddOrUpdateReq req);

    /**
     * 查询服务列表
     *
     * @param req 查询参数
     * @return 服务列表
     */
    ResponsePageData<ServicePurviewRes> services(ServiceListReq req);
}
