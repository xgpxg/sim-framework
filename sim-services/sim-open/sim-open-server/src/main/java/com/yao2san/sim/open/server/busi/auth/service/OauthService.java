package com.yao2san.sim.open.server.busi.auth.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.open.client.bean.request.GetAccessTokenRequest;
import com.yao2san.sim.open.client.bean.response.AccessTokenResponse;
import com.yao2san.sim.open.server.busi.auth.request.AuthenticationReq;
import com.yao2san.sim.open.server.busi.auth.response.AuthenticationRes;

public interface OauthService {
    /**
     * 租户信息认证
     */
    @Deprecated
    ResponseData<AuthenticationRes> authenticate(AuthenticationReq authenticationReq);
    ResponseData<AccessTokenResponse> getAccessToken(GetAccessTokenRequest req);
}
