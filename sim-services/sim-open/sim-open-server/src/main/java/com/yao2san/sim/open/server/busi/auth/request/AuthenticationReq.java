package com.yao2san.sim.open.server.busi.auth.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class AuthenticationReq {

    /**
     * 用户或应用的openId
     */
    @NotEmpty(message = "openId cannot be empty")
    private String openId;

    /**
     * 用户或应用的私钥
     */
    @NotEmpty(message = "secretKey cannot be empty")
    private String secretKey;

    /**
     * 认证类型:app|user
     */
    @Pattern(regexp = "app|user",message = "Unsupported auth scope,usage: app|user")
    @NotEmpty(message = "scope not be empty,usage: app|user")
    private String scope;
}
