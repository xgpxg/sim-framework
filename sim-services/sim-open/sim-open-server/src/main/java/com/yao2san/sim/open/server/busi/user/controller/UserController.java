package com.yao2san.sim.open.server.busi.user.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户对外接口。
 * <p>
 * 该接口供第三方系统调用，包含用户信息变更（新增、修改、删除）及查询
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 创建用户
     *
     * @return result
     */
    @PostMapping("createUser")
    public ResponseData<?> addUser() {
        return ResponseData.success();
    }

    /**
     * 修改用户
     *
     * @return result
     */
    @PostMapping("updateUser")
    public ResponseData<?> updateUser() {
        return ResponseData.success();
    }

    /**
     * 删除用户
     *
     * @return result
     */
    @PostMapping("deleteUser")
    public ResponseData<?> deleteUser() {
        return ResponseData.success();
    }
}
