package com.yao2san;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wxg
 */
@SpringBootApplication
@Slf4j
@MapperScan("com.yao2san.simservice.mapper.simopen")
public class SimOpenApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimOpenApplication.class, args);
        log.info("sim-open start success!");
    }
}
