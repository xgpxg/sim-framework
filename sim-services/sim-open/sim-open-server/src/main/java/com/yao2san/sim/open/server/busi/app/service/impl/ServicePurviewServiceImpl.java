package com.yao2san.sim.open.server.busi.app.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simopen.request.ServiceListReq;
import com.yao2san.simservice.bean.simopen.request.ServicePurviewAddOrUpdateReq;
import com.yao2san.simservice.bean.simopen.response.ServicePurviewRes;
import com.yao2san.sim.open.server.busi.app.service.ServicePurviewService;
import com.yao2san.simservice.entity.simopen.OpenServicePurview;
import com.yao2san.simservice.mapper.simopen.OpenServicePurviewMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @author wxg
 */
@Service
public class ServicePurviewServiceImpl extends ServiceBaseImpl<OpenServicePurviewMapper, OpenServicePurview> implements ServicePurviewService {
    private final OpenServicePurviewMapper openServicePurviewMapper;

    public ServicePurviewServiceImpl(OpenServicePurviewMapper openServicePurviewMapper) {
        this.openServicePurviewMapper = openServicePurviewMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void serviceAuthorization(ServicePurviewAddOrUpdateReq req) {
        //新增授权
        if (Objects.equals(req.getOption(), 1)) {
            openServicePurviewMapper.delServicePurview(req);
            openServicePurviewMapper.addServicePurview(req);
        }
        //解除授权
        else if (Objects.equals(req.getOption(), 2)) {
            openServicePurviewMapper.delServicePurview(req);
        }
    }

    @Override
    public ResponsePageData<ServicePurviewRes> services(ServiceListReq req) {
        Page<ServicePurviewRes> page = convertPage(req);
        if (Objects.equals(req.getIsAuth(), 1)) {
            List<ServicePurviewRes> list = openServicePurviewMapper.qryAuthServices(page, req);
            return buildPageResult(page, list);
        } else if (Objects.equals(req.getIsAuth(), 0)) {
            List<ServicePurviewRes> list = openServicePurviewMapper.qryNotAuthServices(page, req);
            return buildPageResult(page, list);
        } else {
            throw new BusiException("不支持的服务授权查询类型(期望值:0 or 1，实际值:" + req.getIsAuth() + ")");
        }
    }
}
