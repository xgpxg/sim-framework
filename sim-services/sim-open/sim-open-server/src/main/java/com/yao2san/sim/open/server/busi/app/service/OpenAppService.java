package com.yao2san.sim.open.server.busi.app.service;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.simservice.bean.simopen.request.OpenAppListReq;
import com.yao2san.simservice.bean.simopen.response.OpenAppDetailRes;
import com.yao2san.simservice.bean.simopen.response.OpenAppListRes;
import com.yao2san.simservice.bean.simopen.request.OpenAppAddOrUpdateReq;

import java.util.List;

public interface OpenAppService {
    /**
     * 新增应用
     */
    ResponseData<Void> add(OpenAppAddOrUpdateReq req);

    /**
     * 查询应用列表(不分页)
     */
    ResponseData<List<OpenAppListRes>> list(OpenAppListReq req);

    /**
     * 删除应用
     * @param req
     */
    void delete(OpenAppAddOrUpdateReq req);

    /**
     * 更新应用
     * @param req 更新参数
     */
    void update(OpenAppAddOrUpdateReq req);

    ResponseData<OpenAppDetailRes> detail(Long appInstId);

    void restSecretKey(OpenAppAddOrUpdateReq req);

}
