package com.yao2san.sim.open.server.busi.app.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.simservice.bean.simopen.request.OpenAppListReq;
import com.yao2san.simservice.bean.simopen.response.OpenAppDetailRes;
import com.yao2san.simservice.bean.simopen.response.OpenAppListRes;
import com.yao2san.sim.open.server.busi.app.service.OpenAppService;
import com.yao2san.simservice.bean.simopen.request.OpenAppAddOrUpdateReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("app-inst")
public class OpenAppController {
    @Autowired
    private OpenAppService openAppService;

    @GetMapping
    public ResponseData<List<OpenAppListRes>> list(OpenAppListReq req) {
        return openAppService.list(req);
    }

    @PostMapping
    public ResponseData<Void> add(@RequestBody OpenAppAddOrUpdateReq req) {
        return openAppService.add(req);
    }
    @GetMapping("{appInstId}")
    public ResponseData<OpenAppDetailRes> detail(@PathVariable("appInstId") Long appInstId) {
        return openAppService.detail(appInstId);
    }
    @PatchMapping
    public ResponseData<Void> update(@RequestBody OpenAppAddOrUpdateReq req) {
        openAppService.update(req);
        return ResponseData.success();
    }

    @DeleteMapping
    public ResponseData<Void> delete(OpenAppAddOrUpdateReq req) {
        openAppService.delete(req);
        return ResponseData.success();
    }

    @PatchMapping("restSecretKey")
    public ResponseData<Void> restSecretKey(@RequestBody OpenAppAddOrUpdateReq req) {
        openAppService.restSecretKey(req);
        return ResponseData.success();
    }

}
