package com.yao2san.sim.monitor;

import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

import java.lang.management.MemoryUsage;
import java.util.Arrays;

/**
 * @author wxg
 **/
public class TestA {
    public static void main(String[] args) {
        SystemInfo si = new SystemInfo();
        System.out.println(si.getOperatingSystem().getThreadCount());
        HardwareAbstractionLayer hardware = si.getHardware();
        System.out.println(hardware.getDiskStores().get(0));
        System.out.println(hardware.getComputerSystem());
        System.out.println(hardware.getProcessor());
        System.out.println(hardware.getDisplays());
        System.out.println(hardware.getMemory());
        System.out.println(hardware.getNetworkIFs());
        System.out.println(hardware.getPowerSources());
        System.out.println(hardware.getUsbDevices(true));

    }
}
