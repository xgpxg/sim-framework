package com.yao2san.sim.system.server.busi.config.bean.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SysConfigPublishReq {
    @NotEmpty(message = "application can not be empty")
    private String application;
    @NotEmpty(message = "profile can not be empty")
    private String profile;

    private String version;
    private String status;
}
