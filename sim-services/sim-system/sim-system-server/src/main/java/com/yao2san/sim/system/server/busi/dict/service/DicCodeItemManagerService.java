package com.yao2san.sim.system.server.busi.dict.service;

import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsystem.request.*;
import com.yao2san.simservice.bean.simsystem.response.DicCodeListRes;
import com.yao2san.simservice.bean.simsystem.response.DicItemListRes;

import java.util.List;

public interface DicCodeItemManagerService {
    /**
     * 查询数据字典列表
     *
     * @param req 查询参数
     * @return 列表
     */
    ResponsePageData<DicCodeListRes> qryCodes(DicCodeListReq req);

    /**
     * 新增字典
     *
     * @param req 参数
     */
    void addCode(DicCodeAddOrUpdateReq req);

    /**
     * 更新字典
     *
     * @param req 参数
     */
    void updateCode(DicCodeAddOrUpdateReq req);

    /**
     * 检查字典编码是否存在
     *
     * @param dicCode 字典编码
     */
    Boolean codeExist(String dicCode);

    /**
     * 查询字典项列表
     *
     * @param req 查询参数
     * @return 列表
     */
    ResponsePageData<DicItemListRes> qryItems(DicItemListReq req);

    /**
     * 新增字典项
     *
     * @param req 参数
     */
    void addItem(DicItemAddOrUpdateReq req);

    /**
     * 更新字典项
     *
     * @param req 参数
     */
    void updateItem(DicItemAddOrUpdateReq req);

    /**
     * 删除字典（逻辑删除）
     *
     * @param dicCode 字典编码
     */
    void deleteCode(String dicCode);

    /**
     * 删除字典项
     *
     * @param dicCode  字典编码
     * @param itemCode 字典项编码
     */
    void deleteItem(String dicCode, String itemCode);

    /**
     * 查询字典分组列表
     *
     * @return 分组列表
     */
    List<String> groups();
}
