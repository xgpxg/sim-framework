package com.yao2san.sim.system.server.enums;

import lombok.Getter;

public class ConfigEnum {
    public enum ConfigStatus {
        /**
         * 新增
         */
        ADDED("0"),
        /**
         * 修改
         */
        UPDATE("2"),
        /**
         * 删除
         */
        DELETED("3"),
        /**
         * 发布
         */
        PUBLISHED("1");

        @Getter
        private String value;

        ConfigStatus(String value) {
            this.value = value;
        }


    }

}
