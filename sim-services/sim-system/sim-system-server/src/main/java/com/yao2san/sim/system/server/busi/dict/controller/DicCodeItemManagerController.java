package com.yao2san.sim.system.server.busi.dict.controller;

import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsystem.request.*;
import com.yao2san.simservice.bean.simsystem.response.DicCodeListRes;
import com.yao2san.sim.system.server.busi.dict.service.DicCodeItemManagerService;
import com.yao2san.simservice.bean.simsystem.response.DicItemListRes;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典编码/字典项管理
 */
@RestController
@RequestMapping("dicCodeItem")
public class DicCodeItemManagerController {
    private final DicCodeItemManagerService service;

    public DicCodeItemManagerController(DicCodeItemManagerService service) {
        this.service = service;
    }

    /**
     * 查询字典编码
     *
     * @param req 查询参数
     * @return 字典列表
     */
    @GetMapping("codes")
    public ResponseData<ResponsePageData<DicCodeListRes>> qryCode(DicCodeListReq req) {
        return ResponseData.success(service.qryCodes(req));
    }

    /**
     * 查询字典项列表
     *
     * @param req 查询参数
     * @return 字典项列表
     */
    @GetMapping("items")
    public ResponseData<ResponsePageData<DicItemListRes>> qryItem(DicItemListReq req) {
        return ResponseData.success(service.qryItems(req));
    }

    /**
     * 新增字典
     *
     * @param req 参数
     * @return result
     */
    @PostMapping("addCode")
    public ResponseData<Void> addCode(@RequestBody DicCodeAddOrUpdateReq req) {
        service.addCode(req);
        return ResponseData.success();
    }

    /**
     * 修改字典
     *
     * @param req 参数
     * @return result
     */
    @PatchMapping("updateCode")
    ResponseData<Void> updateCode(@RequestBody DicCodeAddOrUpdateReq req) {
        service.updateCode(req);
        return ResponseData.success();
    }

    /**
     * 新增字典项
     *
     * @param req 参数
     * @return result
     */
    @PostMapping("addItem")
    public ResponseData<Void> addItem(@RequestBody DicItemAddOrUpdateReq req) {
        service.addItem(req);
        return ResponseData.success();
    }

    /**
     * 修改增字典项
     *
     * @param req 参数
     * @return result
     */
    @PatchMapping("updateItem")
    ResponseData<Void> updateItem(@RequestBody DicItemAddOrUpdateReq req) {
        service.updateItem(req);
        return ResponseData.success();
    }

    /**
     * 删除字典
     *
     * @param dicCode 字典编号
     * @return result
     */
    @DeleteMapping("deleteCode")
    public ResponseData<Void> deleteCode(String dicCode) {
        service.deleteCode(dicCode);
        return ResponseData.success();
    }

    /**
     * 删除字典项
     *
     * @param dicCode  字典编号
     * @param itemCode 字典项编号
     * @return result
     */
    @DeleteMapping("deleteItem")
    public ResponseData<Void> req(String dicCode, String itemCode) {
        service.deleteItem(dicCode, itemCode);
        return ResponseData.success();
    }

    /**
     * 查询字典分组列表
     *
     * @return 分组列表
     */
    @GetMapping("groups")
    public ResponseData<List<String>> groups() {
        return ResponseData.success(service.groups());
    }

    /**
     * 检查字典编码是否存在
     *
     * @param dicCode 字典编码
     * @return result
     */
    @GetMapping("codeExist")
    public ResponseData<Boolean> codeExist(String dicCode) {
        return ResponseData.success(service.codeExist(dicCode));
    }
}
