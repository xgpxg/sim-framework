package com.yao2san.sim.system.server.busi.dict.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yao2san.sim.framework.web.mybatisplus.ServiceBaseImpl;
import com.yao2san.sim.framework.web.response.ResponsePageData;
import com.yao2san.simservice.bean.simsystem.request.*;
import com.yao2san.simservice.bean.simsystem.response.DicCodeListRes;
import com.yao2san.sim.system.server.busi.dict.service.DicCodeItemManagerService;
import com.yao2san.simservice.bean.simsystem.response.DicItemListRes;
import com.yao2san.simservice.entity.simsystem.SysDicCode;
import com.yao2san.simservice.entity.simsystem.SysDicItem;
import com.yao2san.simservice.enums.common.YesOrNo;
import com.yao2san.simservice.mapper.simsystem.SysDicCodeMapper;
import com.yao2san.simservice.mapper.simsystem.SysDicItemMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DicCodeItemManagerServiceImpl extends ServiceBaseImpl<SysDicCodeMapper, SysDicCode> implements DicCodeItemManagerService {

    private final SysDicCodeMapper sysDicCodeMapper;
    private final SysDicItemMapper sysDicItemMapper;

    public DicCodeItemManagerServiceImpl(SysDicCodeMapper sysDicCodeMapper, SysDicItemMapper sysDicItemMapper) {
        this.sysDicCodeMapper = sysDicCodeMapper;
        this.sysDicItemMapper = sysDicItemMapper;
    }

    public ResponsePageData<DicCodeListRes> qryCodes(DicCodeListReq req) {
        Page<DicCodeListRes> page = convertPage(req);
        List<DicCodeListRes> list = sysDicCodeMapper.qryDicCode(page, req);
        return buildPageResult(page, list);
    }

    @Override
    @Transactional
    public void addCode(DicCodeAddOrUpdateReq req) {
        this.save(req);
    }

    @Override
    @Transactional
    public void updateCode(DicCodeAddOrUpdateReq req) {
        this.updateById(req);
    }

    @Override
    public Boolean codeExist(String dicCode) {
        long count = this.count(Wrappers.lambdaQuery(SysDicCode.class)
                .eq(SysDicCode::getDicCode, dicCode)
                .eq(SysDicCode::getIsShow, YesOrNo.NO));
        return count > 0;
    }

    @Override
    public ResponsePageData<DicItemListRes> qryItems(DicItemListReq req) {
        Page<DicItemListRes> page = convertPage(req);
        List<DicItemListRes> list = sysDicItemMapper.qryDicItem(page, req);
        return buildPageResult(page, list);
    }

    @Override
    @Transactional
    public void addItem(DicItemAddOrUpdateReq req) {
        sysDicItemMapper.insert(req);
    }

    @Override
    public void updateItem(DicItemAddOrUpdateReq req) {
        if (req.getSort() == null) {
            req.setSort(0);
        }
        sysDicItemMapper.updateById(req);
    }

    @Override
    @Transactional
    public void deleteCode(String dicCode) {
        this.update(Wrappers.lambdaUpdate(SysDicCode.class)
                .eq(SysDicCode::getDicCode, dicCode)
                .set(SysDicCode::getIsDelete, YesOrNo.YES));
        this.sysDicItemMapper.update(null, Wrappers.lambdaUpdate(SysDicItem.class)
                .eq(SysDicItem::getDicCode, dicCode)
                .set(SysDicItem::getIsDelete, YesOrNo.YES));
    }

    @Override
    @Transactional
    public void deleteItem(String dicCode, String itemCode) {
        this.sysDicItemMapper.update(null, Wrappers.lambdaUpdate(SysDicItem.class)
                .eq(SysDicItem::getDicCode, dicCode)
                .eq(SysDicItem::getItemCode, itemCode)
                .set(SysDicItem::getIsDelete, YesOrNo.YES));
    }

    @Override
    public List<String> groups() {
        return this.list(Wrappers.lambdaQuery(SysDicCode.class)
                        .eq(SysDicCode::getIsDelete, YesOrNo.NO)
                        .select(SysDicCode::getGroupName)
                        .groupBy(SysDicCode::getGroupName))
                .stream()
                .map(SysDicCode::getGroupName)
                .collect(Collectors.toList());
    }
}
