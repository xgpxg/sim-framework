package com.yao2san.sim.system.server.busi.config.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SysConfigApp extends BaseBean {
    private Long configAppId;

    private String appName;
    private String appCode;

    public interface Add {
    }
}
