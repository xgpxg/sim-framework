package com.yao2san.sim.api.server.busi.api.service;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiGroupReq;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoListReq;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoUpdateReq;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiAppDetailRes;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiDetailRes;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiVersionRes;
import com.yao2san.sim.api.server.busi.register.bean.ApiApp;
import com.yao2san.sim.api.server.busi.register.bean.ApiGroup;
import com.yao2san.sim.api.server.busi.register.bean.ApiInfo;
import com.yao2san.sim.framework.web.response.ResponseData;

import java.util.List;

/**
 * @author wxg
 **/
public interface ApiManagerService {
    ResponseData<List<ApiApp>> apps();
    ResponseData<ApiAppDetailRes> app(Long apiAppId);

    ResponseData<PageInfo<ApiInfo>> apis(ApiInfoListReq req);
    ResponseData<ApiDetailRes> apiDetail(Long apiId);

    ResponseData<List<ApiGroup>> groups(ApiGroupReq req);
    ResponseData<List<String>> apiAllVersions(Long apiAppId);
    ResponseData<List<ApiVersionRes>> apiVersions(Long apiAppId, String operationId);

    ResponseData<Void> update(ApiInfoUpdateReq req);
}
