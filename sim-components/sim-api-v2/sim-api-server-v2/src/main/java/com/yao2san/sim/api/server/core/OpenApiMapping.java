package com.yao2san.sim.api.server.core;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yao2san.sim.api.server.busi.register.bean.ApiSchema;
import com.yao2san.sim.api.server.busi.register.bean.*;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
/*@Deprecated*/
public class OpenApiMapping {

    private static final String DEFAULT_VERSION = "default";

    private static ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        OpenApiMapping.objectMapper = objectMapper;
    }

    public ApiApp buildApp(ApiApp apiApp) {
        return apiApp;

    }

    public List<ApiServer> buildServers(List<ApiServer> servers) {
        return servers;

    }

    public List<ApiGroup> buildGroups(JSONObject openApiJson) {
        List<Tag> tags = openApiJson.getObject("tags", new TypeReference<List<Tag>>() {
        });
        List<ApiGroup> apiGroups = new ArrayList<>();

        for (Tag tag : tags) {
            ApiGroup apiGroup = new ApiGroup();
            apiGroup.setName(tag.getName());
            apiGroup.setDescription(tag.getDescription());
            apiGroups.add(apiGroup);
        }
        return apiGroups;
    }

    public List<ApiInfo> buildInfo(JSONObject openApiJson) {
        List<ApiInfo> apiInfos = new ArrayList<>();
        Map<String, JSONObject> paths = openApiJson.getObject("paths", new TypeReference<Map<String, JSONObject>>() {
        });
        paths.forEach((k, v) -> {
            JSONObject get = v.getJSONObject("get");
            JSONObject post = v.getJSONObject("post");
            JSONObject put = v.getJSONObject("put");
            JSONObject patch = v.getJSONObject("patch");
            JSONObject delete = v.getJSONObject("delete");
            JSONObject trace = v.getJSONObject("trace");


            if (get != null) {
                ApiInfo apiInfo = new ApiInfo();
                apiInfo.setHttpMethod("GET");
                fillApiInfo(k, apiInfo, get);
                apiInfos.add(apiInfo);
            }
            if (post != null) {
                ApiInfo apiInfo = new ApiInfo();
                apiInfo.setHttpMethod("POST");
                fillApiInfo(k, apiInfo, post);
                apiInfos.add(apiInfo);
            }
            if (put != null) {
                ApiInfo apiInfo = new ApiInfo();
                apiInfo.setHttpMethod("PUT");
                fillApiInfo(k, apiInfo, put);
                apiInfos.add(apiInfo);
            }
            if (patch != null) {
                ApiInfo apiInfo = new ApiInfo();
                apiInfo.setHttpMethod("PATH");
                fillApiInfo(k, apiInfo, patch);
                apiInfos.add(apiInfo);
            }
            if (delete != null) {
                ApiInfo apiInfo = new ApiInfo();
                apiInfo.setHttpMethod("DELETE");
                fillApiInfo(k, apiInfo, delete);
                apiInfos.add(apiInfo);
            }
            if (trace != null) {
                ApiInfo apiInfo = new ApiInfo();
                apiInfo.setHttpMethod("TRACE");
                fillApiInfo(k, apiInfo, trace);
                apiInfos.add(apiInfo);
            }
        });
        return apiInfos;
    }

    private void fillApiInfo(String path, ApiInfo apiInfo, JSONObject operationJson) {
        apiInfo.setPath(path);
        apiInfo.setName(operationJson.getString("summary"));
        apiInfo.setDescription(operationJson.getString("description"));
        apiInfo.setOperationId(operationJson.getString("operationId"));
        apiInfo.setParameter(operationJson.getString("parameters"));
        apiInfo.setRequestBody(operationJson.getString("requestBody"));
        apiInfo.setResponse(operationJson.getString("responses"));
        apiInfo.setTag(String.valueOf(operationJson.getJSONArray("tags").get(0)));
        apiInfo.setVersion(DEFAULT_VERSION);

        String extensions = operationJson.getString("extensions");
        if (StringUtils.isNotEmpty(extensions)) {
            apiInfo.setExtensions(extensions);
            JSONObject extensionsJson = JSONObject.parseObject(extensions);
            String version = extensionsJson.getString("version");
            if (StringUtils.isNotEmpty(version)) {
                apiInfo.setVersion(version);
            }
        }
    }

    public List<ApiSchema> buildSchema(JSONObject openApiJson) {
        List<ApiSchema> apiSchemas = new ArrayList<>();

        JSONObject components = openApiJson.getJSONObject("components");
        Map<String, JSONObject> schemas = components.getObject("schemas", new TypeReference<Map<String, JSONObject>>() {
        });
        schemas.forEach((k, v) -> {
            ApiSchema apiSchema = new ApiSchema();
            apiSchema.setName(k);
            apiSchema.setTitle(v.getString("title"));
            apiSchema.setDescription(v.getString("description"));
            apiSchema.setContent(v.toJSONString());
            apiSchema.setPath("#/components/schemas/" + k);
            apiSchemas.add(apiSchema);
        });

        return apiSchemas;
    }

    public static String toJsonString(Object object) {
        if (object == null) {
            return null;
        }
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    public static String getCode(Info info) {
        Map<String, Object> extensions = info.getExtensions();
        return String.valueOf(extensions.get("x-group"));
    }
}
