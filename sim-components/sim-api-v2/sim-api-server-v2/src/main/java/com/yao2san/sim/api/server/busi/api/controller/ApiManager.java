package com.yao2san.sim.api.server.busi.api.controller;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiGroupReq;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoListReq;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoUpdateReq;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiAppDetailRes;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiDetailRes;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiVersionRes;
import com.yao2san.sim.api.server.busi.api.service.ApiManagerService;
import com.yao2san.sim.api.server.busi.register.bean.ApiApp;
import com.yao2san.sim.api.server.busi.register.bean.ApiGroup;
import com.yao2san.sim.api.server.busi.register.bean.ApiInfo;
import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wxg
 **/
@RestController
@RequestMapping("api-manage")
public class ApiManager {
    @Autowired
    private ApiManagerService apiManagerService;

    @GetMapping("apps")
    public ResponseData<List<ApiApp>> apps() {
        return apiManagerService.apps();
    }

    @GetMapping("apps/{apiAppId}")
    public ResponseData<ApiAppDetailRes> app(@PathVariable(value = "apiAppId") Long apiAppId) {
        return apiManagerService.app(apiAppId);
    }

    @GetMapping("apis")
    public ResponseData<PageInfo<ApiInfo>> apis(ApiInfoListReq req) {
        return apiManagerService.apis(req);
    }

    @GetMapping("apis/{apiId}")
    public ResponseData<ApiDetailRes> apiDetail(@PathVariable("apiId") Long apiId) {
        return apiManagerService.apiDetail(apiId);
    }

    @PatchMapping("api")
    public ResponseData<Void> update(@RequestBody ApiInfoUpdateReq req) {
        return apiManagerService.update(req);
    }

    @GetMapping("groups")
    public ResponseData<List<ApiGroup>> groups(ApiGroupReq req) {
        return apiManagerService.groups(req);
    }

    @GetMapping("all-versions")
    ResponseData<List<String>> apiAllVersions(Long apiAppId) {
        return apiManagerService.apiAllVersions(apiAppId);
    }

    @GetMapping("versions")
    ResponseData<List<ApiVersionRes>> apiVersions(Long apiAppId, String operationId) {
        return apiManagerService.apiVersions(apiAppId,operationId);
    }
}
