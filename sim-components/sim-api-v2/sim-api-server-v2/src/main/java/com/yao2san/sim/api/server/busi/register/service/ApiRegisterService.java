package com.yao2san.sim.api.server.busi.register.service;

import com.alibaba.fastjson.JSONObject;
import com.yao2san.sim.api.server.busi.register.bean.ApiRegisterInfo;
import com.yao2san.sim.api.server.busi.register.bean.request.ApiRegisterReq;
import com.yao2san.sim.api.server.busi.register.controller.ApiRegisterController;
import com.yao2san.sim.framework.web.response.ResponseData;
import io.swagger.v3.oas.models.OpenAPI;
import springfox.documentation.service.Documentation;

import java.util.Map;

/**
 * @author wxg
 **/
public interface ApiRegisterService {
    ResponseData<String> registry(ApiRegisterInfo apiRegisterInfo);
}
