package com.yao2san.sim.api.server.busi.api.bean.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ApiInfoUpdateReq {
    @NotNull(message = "apiId不能为空")
    private Long apiId;
    private String status;
}
