package com.yao2san.sim.api.server.busi.export.service.impl;

import com.github.pagehelper.PageInfo;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoListReq;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiAppDetailRes;
import com.yao2san.sim.api.server.busi.api.service.ApiManagerService;
import com.yao2san.sim.api.server.busi.export.service.ExportService;
import com.yao2san.sim.api.server.busi.register.bean.ApiApp;
import com.yao2san.sim.api.server.busi.register.bean.ApiInfo;
import com.yao2san.sim.framework.web.exception.BusiException;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import com.yao2san.sim.framework.web.uploader.WebDownloader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author wxg
 **/
@Service
public class ExportServiceImpl extends BaseServiceImpl implements ExportService {
    private static final String EXPORT_API_TEMPLATE_FILE = "/template.docx";

    @Autowired
    private ApiManagerService apiManagerService;

    @Override
    public void export(Long apiAppId, String exportType) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        if (response == null) {
            throw new RuntimeException("error");
        }
        if ("word".equalsIgnoreCase(exportType)) {
            exportWord(apiAppId, response);
        } else if ("pdf".equalsIgnoreCase(exportType)) {
            exportPdf(apiAppId, response);
        } else {
            throw new BusiException("Unsupported export type,use word or pdf");
        }
    }

    private void exportWord(Long apiAppId, HttpServletResponse response) {
        ApiAppDetailRes app = this.getApiAppDetail(apiAppId);
        List<ApiInfo> apis = this.getApis(apiAppId);
        WebDownloader.fillDownloadResponse(app.getName() + ".docx", response, true);
        try (InputStream in = this.getClass().getResourceAsStream(EXPORT_API_TEMPLATE_FILE)) {
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void exportPdf(Long apiAppId, HttpServletResponse response) {

    }

    private ApiAppDetailRes getApiAppDetail(Long apiAppId) {
        ResponseData<ApiAppDetailRes> result = apiManagerService.app(apiAppId);
        return result.getData();
    }

    private List<ApiInfo> getApis(Long apiAppId) {
        ApiInfoListReq param = new ApiInfoListReq();
        param.setApiAppId(apiAppId);
        return sqlSession.selectList("apiManage.qryApis", param);
    }

}
