package com.yao2san.sim.api.server.busi.api.bean.response;

import com.yao2san.sim.api.server.busi.register.bean.ApiApp;
import com.yao2san.sim.api.server.busi.register.bean.ApiServer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiAppDetailRes extends ApiApp {
    private int apiCount;
    private List<MethodCount> methodCounts;
    private List<ApiServer> apiServers;
    @Data
    public static class MethodCount {
        private int count;
        private String httpMethod;
    }
}
