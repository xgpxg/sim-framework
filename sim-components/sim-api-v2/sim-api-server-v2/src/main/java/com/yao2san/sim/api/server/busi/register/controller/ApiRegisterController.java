package com.yao2san.sim.api.server.busi.register.controller;

import com.alibaba.fastjson.JSONObject;
import com.yao2san.sim.api.server.busi.register.bean.ApiRegisterInfo;
import com.yao2san.sim.api.server.busi.register.bean.request.ApiRegisterReq;
import com.yao2san.sim.api.server.busi.register.service.ApiRegisterService;
import com.yao2san.sim.framework.web.response.ResponseData;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.service.Documentation;

import java.util.Map;


/**
 * @author wxg
 **/
@RestController
@RequestMapping("registry")
public class ApiRegisterController {
    @Autowired
    private ApiRegisterService registerService;

    @PostMapping
    public ResponseData<String> registry(@RequestBody ApiRegisterInfo apiRegisterInfo) {
        return   registerService.registry(apiRegisterInfo);
    }
}
