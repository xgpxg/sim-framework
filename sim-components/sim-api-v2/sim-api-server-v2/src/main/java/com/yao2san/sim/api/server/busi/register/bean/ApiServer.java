package com.yao2san.sim.api.server.busi.register.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApiServer extends BaseBean {
    private Long apiServerId;
    private Long apiAppId;

    private String url;

    private String description;

    private Boolean isDefault = false;

    private Boolean isActive = false;

}
