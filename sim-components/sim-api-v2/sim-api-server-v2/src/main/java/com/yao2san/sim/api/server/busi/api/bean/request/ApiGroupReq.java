package com.yao2san.sim.api.server.busi.api.bean.request;

import lombok.Data;

/**
 * @author wxg
 **/
@Data
public class ApiGroupReq {
    private Long apiAppId;
}
