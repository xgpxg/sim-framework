package com.yao2san.sim.api.server.busi.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;
import com.github.pagehelper.PageInfo;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiGroupReq;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoListReq;
import com.yao2san.sim.api.server.busi.api.bean.request.ApiInfoUpdateReq;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiAppDetailRes;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiDetailRes;
import com.yao2san.sim.api.server.busi.api.bean.response.ApiVersionRes;
import com.yao2san.sim.api.server.busi.api.service.ApiManagerService;
import com.yao2san.sim.api.server.busi.register.bean.*;
import com.yao2san.sim.framework.utils.RequestUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wxg
 **/
@Service
public class ApiManagerServiceImpl extends BaseServiceImpl implements ApiManagerService {
    @Override
    public ResponseData<List<ApiApp>> apps() {
        List<ApiApp> list = sqlSession.selectList("apiManage.qryApps");
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<ApiAppDetailRes> app(Long apiAppId) {
        int all = sqlSession.selectOne("apiManage.countApi", apiAppId);
        List<ApiAppDetailRes.MethodCount> list = sqlSession.selectList("apiManage.countApiByHttpMethod", apiAppId);
        ApiApp apiApp = sqlSession.selectOne("apiManage.qryApiApp", apiAppId);
        List<ApiServer> apiServers = sqlSession.selectList("apiManage.qryAppServers", apiAppId);

        ApiAppDetailRes res = new ApiAppDetailRes();
        BeanUtils.copyProperties(apiApp, res);
        res.setApiCount(all);
        res.setMethodCounts(list);
        res.setApiServers(apiServers);
        return ResponseData.success(res);
    }

    @Override
    public ResponseData<PageInfo<ApiInfo>> apis(ApiInfoListReq req) {
        PageInfo<ApiInfo> list = this.qryList("apiManage.qryApis", req);
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<ApiDetailRes> apiDetail(Long apiId) {
        ApiDetailRes res = new ApiDetailRes();
        ApiInfo apiInfo = this.sqlSession.selectOne("apiManage.qryApiInfo", apiId);
        ApiApp apiApp = this.sqlSession.selectOne("apiManage.qryApiApp", apiInfo.getApiAppId());
        List<ApiServer> apiServers = this.sqlSession.selectList("apiManage.qryAppServers", apiInfo.getApiAppId());
        List<ApiSchema> apiSchemas = this.sqlSession.selectList("apiManage.qryAppSchemas", apiInfo.getApiAppId());

        res.setApiInfo(apiInfo);
        res.setApiApp(apiApp);
        res.setApiServers(apiServers);
        res.setApiSchemas(apiSchemas);


        ApiDetailRes.Original original = new ApiDetailRes.Original();
        List<JSONObject> parameters = JSONObject.parseObject(apiInfo.getParameter(), new TypeReference<List<JSONObject>>() {
        }, Feature.DisableCircularReferenceDetect);
        JSONObject requestBody = JSONObject.parseObject(apiInfo.getRequestBody(), Feature.DisableCircularReferenceDetect);
        JSONObject responses = JSONObject.parseObject(apiInfo.getResponse(), JSONObject.class, Feature.DisableCircularReferenceDetect);
        List<JSONObject> schemas = apiSchemas.stream().map(v -> JSONObject.parseObject(v.getContent(), JSONObject.class, Feature.DisableCircularReferenceDetect)).collect(Collectors.toList());

        original.setParameters(parameters);
        original.setRequestBody(requestBody);
        original.setResponses(responses);
        original.setSchemas(schemas);
        res.setOriginal(original);
        return ResponseData.success(res);
    }

    @Override
    public ResponseData<List<ApiGroup>> groups(ApiGroupReq req) {
        List<ApiGroup> list = sqlSession.selectList("apiManage.qryApiGroups", req.getApiAppId());
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<List<String>> apiAllVersions(Long apiAppId) {
        List<String> list = sqlSession.selectList("apiManage.qryApiAllVersions", apiAppId);
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<List<ApiVersionRes>> apiVersions(Long apiAppId, String operationId) {
        Map<String, Object> param = new HashMap<>(2);
        param.put("operationId", operationId);
        param.put("apiAppId", apiAppId);
        List<ApiVersionRes> list = sqlSession.selectList("apiManage.qryApiVersions", param);
        return ResponseData.success(list);
    }

    @Override
    public ResponseData<Void> update(ApiInfoUpdateReq req) {
        this.sqlSession.update("apiManage.updateApi", req);
        return ResponseData.success();
    }
}
