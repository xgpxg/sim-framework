package com.yao2san.sim.api.server.busi.register.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.yao2san.sim.api.server.busi.register.bean.ApiSchema;
import com.yao2san.sim.api.server.busi.register.bean.*;
import com.yao2san.sim.api.server.busi.register.service.ApiRegisterService;
import com.yao2san.sim.api.server.core.OpenApiMapping;
import com.yao2san.sim.framework.utils.BeanContextUtil;
import com.yao2san.sim.framework.web.response.ResponseData;
import com.yao2san.sim.framework.web.service.impl.BaseServiceImpl;
import io.swagger.v3.oas.models.OpenAPI;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wxg
 **/
@Service
@Slf4j
public class ApiRegisterServiceImpl extends BaseServiceImpl implements ApiRegisterService {
    @Autowired
    private OpenApiMapping mapping;

    @Override
    @Transactional
    public ResponseData<String> registry(ApiRegisterInfo apiRegisterInfo) {

       /* OpenAPI openAPI = null;

        if (!this.shouldUpdate(openAPI)) {
            log.info("Api has not changed and does not need to be updated");
            return ResponseData.success();
        }*/

        JSONObject openApiJson = JSONObject.parseObject(apiRegisterInfo.getApiApp().getOpenapiJson(), Feature.DisableCircularReferenceDetect);
        ApiApp apiApp = mapping.buildApp(apiRegisterInfo.getApiApp());
        List<ApiServer> apiServers = mapping.buildServers(apiRegisterInfo.getApiServers());
        List<ApiGroup> apiGroups = mapping.buildGroups(openApiJson);
        List<ApiSchema> apiSchemas = mapping.buildSchema(openApiJson);
        List<ApiInfo> apiInfos = mapping.buildInfo(openApiJson);

        this.saveOrUpdate(apiApp, apiGroups, apiServers, apiInfos, apiSchemas);
        return ResponseData.success("api register success");
    }
    private void saveOrUpdate(ApiApp apiApp,List<ApiServer> apiServers) {
        if (!this.exist(apiApp.getCode())) {
            save(apiApp,apiServers);
        }else{
            update(apiApp,apiServers);
        }
    }
    private void save(ApiApp apiApp,List<ApiServer> apiServers) {
        this.sqlSession.insert("api.addApiApp", apiApp);
        Long apiAppId = apiApp.getApiAppId();

        fillApiAppId(apiAppId,apiServers);

        this.sqlSession.insert("api.addApiServer", apiServers);

    }
    private void update(ApiApp apiApp,List<ApiServer> apiServers) {

    }

    private void fillApiAppId(Long apiAppId,List<ApiServer> apiServers){
        for (ApiServer apiServer : apiServers) {
            apiServer.setApiAppId(apiAppId);
        }
    }

    private void saveOrUpdate(ApiApp apiApp, List<ApiGroup> apiGroups, List<ApiServer> apiServers, List<ApiInfo> apiInfos, List<ApiSchema> apiSchemas) {
        if (!this.exist(apiApp.getCode())) {
            this.save(apiApp, apiGroups, apiServers, apiInfos, apiSchemas);
        } else {
            this.update(apiApp, apiGroups, apiServers, apiInfos, apiSchemas);
        }
    }

    private void save(ApiApp apiApp, List<ApiGroup> apiGroups, List<ApiServer> apiServers, List<ApiInfo> apiInfos, List<ApiSchema> apiSchemas) {
        this.sqlSession.insert("api.addApiApp", apiApp);
        Long apiAppId = apiApp.getApiAppId();

        fillApiAppId(apiAppId, apiApp, apiGroups, apiServers, apiInfos, apiSchemas);

        this.sqlSession.insert("api.addApiGroup", apiGroups);
        this.sqlSession.insert("api.addApiServer", apiServers);
        this.sqlSession.insert("api.addApiInfo", apiInfos);
        this.sqlSession.insert("api.addApiSchema", apiSchemas);
    }

    private void update(ApiApp apiApp, List<ApiGroup> apiGroups, List<ApiServer> apiServers, List<ApiInfo> apiInfos, List<ApiSchema> apiSchemas) {
        Long apiAppId = this.sqlSession.selectOne("api.qryApiAppIdByCode", apiApp.getCode());
        fillApiAppId(apiAppId, apiApp, apiGroups, apiServers, apiInfos, apiSchemas);


        List<ApiInfo> apis = this.sqlSession.selectList("api.qryAllApis", apiAppId);
        List<ApiSchema> schemas = this.sqlSession.selectList("api.qryAllSchemas", apiAppId);

        Diff<ApiInfo> apiInfoDif = getDifApiInfo(apiAppId, apis, apiInfos);
        Diff<ApiSchema> schemaDif = getDifSchema(apiAppId, schemas, apiSchemas);

        if (apiInfoDif.getAdded().size() > 0) {
            this.sqlSession.insert("api.addApiInfo", apiInfoDif.getAdded());
        }
        if (apiInfoDif.getRemoved().size() > 0) {
            this.sqlSession.delete("api.deleteApiInfo", apiInfoDif.getRemoved().stream().map(ApiInfo::getApiId).collect(Collectors.toList()));
        }
        if (apiInfoDif.getUpdated().size() > 0) {
            for (ApiInfo apiInfo : apiInfoDif.getUpdated()) {
                this.sqlSession.update("api.updateApiInfo", apiInfo);
            }
        }

        if (schemaDif.getAdded().size() > 0) {
            this.sqlSession.insert("api.addApiSchema", schemaDif.getAdded());
        }
        if (schemaDif.getRemoved().size() > 0) {
            this.sqlSession.delete("api.deleteApiSchema", schemaDif.getRemoved().stream().map(ApiSchema::getApiSchemaId).collect(Collectors.toList()));
        }
        if (schemaDif.getUpdated().size() > 0) {
            for (ApiSchema apiSchema : schemaDif.getUpdated()) {
                this.sqlSession.update("api.updateApiSchema", apiSchema);
            }
        }


        this.sqlSession.delete("api.cleanApiGroup", apiAppId);
        this.sqlSession.insert("api.addApiGroup", apiGroups);
        this.sqlSession.update("api.updateApiApp", apiApp);
        //TODO 记录变化

    }

    private void fillApiAppId(Long apiAppId, ApiApp apiApp, List<ApiGroup> apiGroups, List<ApiServer> apiServers, List<ApiInfo> apiInfos, List<ApiSchema> apiSchemas) {
        for (ApiGroup apiGroup : apiGroups) {
            apiGroup.setApiAppId(apiAppId);
        }
        for (ApiServer apiServer : apiServers) {
            apiServer.setApiAppId(apiAppId);
        }
        for (ApiInfo apiInfo : apiInfos) {
            apiInfo.setApiAppId(apiAppId);
        }
        for (ApiSchema apiSchema : apiSchemas) {
            apiSchema.setApiAppId(apiAppId);
        }
    }

    private Diff<ApiInfo> getDifApiInfo(Long apiAppId, List<ApiInfo> oldApiInfos, List<ApiInfo> newApiInfos) {
        Map<String, ApiInfo> map = new HashMap<>();
        oldApiInfos.forEach(v -> map.put(v.getOperationId(), v));

        Diff<ApiInfo> diff = new Diff<>();
        for (ApiInfo apiInfo : newApiInfos) {
            String newOperationId = apiInfo.getOperationId();
            //for add new
            if (!map.containsKey(newOperationId)) {
                apiInfo.setApiId(apiAppId);
                diff.getAdded().add(apiInfo);
            }else if(map.get(newOperationId).getVersion()!=null && !map.get(newOperationId).getVersion().equals(apiInfo.getVersion())){
                //for different version
                apiInfo.setApiId(apiAppId);
                diff.getAdded().add(apiInfo);
            }else {
                //for update old
                ApiInfo old = map.get(newOperationId);
                if (!apiInfo.equalsValue(old)) {
                    apiInfo.setApiId(old.getApiId());
                    apiInfo.setApiAppId(old.getApiAppId());
                    diff.getUpdated().add(apiInfo);
                }
            }
        }

        map.clear();
        newApiInfos.forEach(v -> map.put(v.getOperationId(), v));
        for (ApiInfo apiInfo : oldApiInfos) {
            String oldOperationId = apiInfo.getOperationId();
            //for delete not exist
            if (!map.containsKey(oldOperationId)) {
                diff.getRemoved().add(apiInfo);
            }
        }

        return diff;
    }

    private Diff<ApiSchema> getDifSchema(Long apiAppId, List<ApiSchema> oldApiSchemas, List<ApiSchema> newApiSchemas) {
        Map<String, ApiSchema> map = new HashMap<>();
        oldApiSchemas.forEach(v -> map.put(v.getName(), v));

        Diff<ApiSchema> diff = new Diff();
        for (ApiSchema apiSchema : newApiSchemas) {
            String name = apiSchema.getName();
            //for add new
            if (!map.containsKey(name)) {
                apiSchema.setApiAppId(apiAppId);
                diff.getAdded().add(apiSchema);
            } else {
                //for update old
                ApiSchema old = map.get(name);
                if (!apiSchema.equalsValue(old)) {
                    apiSchema.setApiSchemaId(old.getApiSchemaId());
                    apiSchema.setApiAppId(old.getApiAppId());
                    diff.getUpdated().add(apiSchema);
                }
            }
        }

        map.clear();
        newApiSchemas.forEach(v -> map.put(v.getName(), v));
        for (ApiSchema apiSchema : oldApiSchemas) {
            String name = apiSchema.getName();
            //for delete not exist
            if (!map.containsKey(name)) {
                diff.getRemoved().add(apiSchema);
            }
        }

        return diff;
    }


    private boolean shouldUpdate(OpenAPI openAPI) {

        String code = OpenApiMapping.getCode(openAPI.getInfo());
        String json = this.sqlSession.selectOne("api.qryOpenApiJson", code);

        if (json == null) {
            return true;
        }
        int newHash = OpenApiMapping.toJsonString(openAPI).hashCode();
        int oldHash = json.hashCode();

        return oldHash != newHash;
    }

    private boolean exist(String code) {
        Long apiAppId = this.sqlSession.selectOne("api.qryApiAppIdByCode", code);
        return apiAppId != null;
    }


    @Data
    static class Diff<T> {
        private List<T> added = new ArrayList<>();
        private List<T> updated = new ArrayList<>();
        private List<T> removed = new ArrayList<>();
    }
}
