package com.yao2san.sim.api.server.busi.api.bean.response;

import lombok.Data;

@Data
public class ApiVersionRes {
    private Long apiId;
    private String version;
}
