package com.yao2san.sim.api.client.config;

import com.yao2san.sim.api.client.annotation.EnabledSimApiClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(SimApiClientProperties.class)
@ComponentScan("com.yao2san")
@ConditionalOnClass(EnabledSimApiClient.class)
public class SimApiClientAutoConfiguration {
}
