package com.yao2san.sim.api.client.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ApiOperation {
    String value() ;
    String description() default "";

    String group() default "";

    String consumer() default "application/json";

    String provider() default "application/json";

    String version() default "0.0.1";

    String author() default "";

    String protocols() default "";

    Class<?> response() default Object.class;

    int sort() default 0;

    boolean hidden() default false;

    boolean editable() default false;

}
