package com.yao2san.sim.api.client.config;

import com.yao2san.sim.api.client.annotation.EnabledSimApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Server;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * @author wxg
 */
@Configuration
@ConditionalOnClass(EnabledSimApiClient.class)
@EnableSwagger2
public class SwaggerConfig {
    @Autowired
    private SimApiClientProperties client;

    @Bean
    public Docket createRestApi() {
        //TODO default value
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName(client.getGroup())
                .select()
                .apis(RequestHandlerSelectors.basePackage(client.getBasePackage()))
                .paths(PathSelectors.any())
                .build();

    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(client.getName())
                .description(client.getDescription())
                .version(client.getVersion())
                .build();
    }
}