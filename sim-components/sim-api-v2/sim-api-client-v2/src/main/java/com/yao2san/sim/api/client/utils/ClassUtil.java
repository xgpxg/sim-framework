package com.yao2san.sim.api.client.utils;

public class ClassUtil {
    public static String getMainClassName() {
        StackTraceElement[] stackTraceElements = new RuntimeException().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            if ("main".equals(stackTraceElement.getMethodName())) {
                return stackTraceElement.getClassName();
            }
        }
        return "";
    }
    public static Class<?> getMainClass() {
        Class<?> mainClass;
        try {
            mainClass = Class.forName(getMainClassName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return mainClass;
    }
}
