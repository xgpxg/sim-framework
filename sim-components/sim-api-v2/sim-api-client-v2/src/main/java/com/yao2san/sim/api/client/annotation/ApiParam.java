package com.yao2san.sim.api.client.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ApiParam {
    /**
     * 参数释义
     */
    String value();
    /**
     * 参数名称(别名,默认取字段名)
     */
    String name() default "";
    /**
     * 是否必须
     */
    boolean required() default false;

    /**
     * 是否隐藏
     */
    boolean hidden() default false;

    String example() default "";
}
