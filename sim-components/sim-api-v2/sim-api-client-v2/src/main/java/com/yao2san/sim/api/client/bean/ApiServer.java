package com.yao2san.sim.api.client.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class ApiServer{

    private String url;

    private String description;

    private Boolean isDefault;

    private Boolean isActive = false;

}
