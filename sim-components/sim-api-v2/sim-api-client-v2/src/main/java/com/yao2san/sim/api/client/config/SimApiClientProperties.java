package com.yao2san.sim.api.client.config;

import com.yao2san.sim.api.client.bean.ApiServer;
import io.swagger.v3.oas.models.servers.Server;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.Contact;

import java.util.List;

/**
 * @author wxg
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "sim.api.client")
public class SimApiClientProperties {
    private String name;
    private String group;
    private String url;
    private String description;
    private String version;
    private String basePackage;
    private List<ApiServer> servers;
}
