package com.yao2san.sim.api.client.bean;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiApp {

    private String name;

    private String code;

    private String description;

    private String openapiJson;
}
