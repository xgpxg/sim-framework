package com.yao2san.sim.api.client.utils;

import com.yao2san.sim.framework.utils.BeanContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import springfox.documentation.service.Documentation;
import springfox.documentation.spring.web.DocumentationCache;

import java.util.Map;

/**
 * @author wxg
 **/
public class SpringfoxUtil {

    public static Map<String, Documentation> getAllCachedApi() {
        DocumentationCache documentationCache = BeanContextUtil.getBean(DocumentationCache.class);
        Map<String, Documentation> all = documentationCache.all();

        return all;
    }


}
