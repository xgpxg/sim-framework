package com.yao2san.sim.api.client.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;
import java.util.Set;

@Data
@Accessors(chain = true)
@Deprecated
public class ApiInfo {
    private String name;
    private Set<RequestMethod> methods;
    private Set<String> paths;
    private Set<ApiParamInfo> params;
    private Map<String, String> headers;
    private String group;
    private String consumer;
    private String provider;
    private String description;
    private String version;
    private String author;
    private boolean hidden;
    private String protocols;
    private ApiParamInfo response;
    private int sort;

}
