package com.yao2san.sim.api.client.controller;

import com.yao2san.sim.api.client.service.ApiRegister;
import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("endpoint")
public class ClientController {
    @Autowired
    private ApiRegister apiRegister;
    @GetMapping("refresh")
    public ResponseData<Void> refresh(){
        apiRegister.init();
        return ResponseData.success();
    }
}
