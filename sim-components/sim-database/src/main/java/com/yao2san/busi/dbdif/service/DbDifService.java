package com.yao2san.busi.dbdif.service;

import com.yao2san.sim.framework.web.response.ResponseData;

import java.util.List;
import java.util.Map;

public interface DbDifService {
    List<Map<String, Object>> qryTables(Map<String, Object> params);

    List<Map<String, Object>> qryFields(Map<String, Object> params);

    ResponseData<Void> saveResult(Map<String, Object> params);

    ResponseData<List<Map<String, Object>>> qryHistory();
}
