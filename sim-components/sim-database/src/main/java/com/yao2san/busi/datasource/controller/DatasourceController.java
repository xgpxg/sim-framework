package com.yao2san.busi.datasource.controller;

import com.yao2san.busi.datasource.bean.request.DatasourceReq;
import com.yao2san.busi.datasource.service.DatasourceService;
import com.yao2san.sim.framework.web.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("datasource")
public class DatasourceController {
    @Autowired
    private DatasourceService datasourceService;

    @GetMapping
    public ResponseData list(DatasourceReq req) {
        return datasourceService.list(req);
    }

    @GetMapping("{datasourceId}")
    public ResponseData detail(@PathVariable Long datasourceId) {
        return ResponseData.success();
    }

    @PostMapping
    public ResponseData add(@RequestBody DatasourceReq req) {
        return this.datasourceService.add(req);
    }

    @GetMapping("datasourceAttrCode")
    public ResponseData qryDatasourceAttrCode(String datasourceType) {
        return datasourceService.qryDatasourceAttrCode(datasourceType);
    }
}
