package com.yao2san.busi.datasource.bean.entity;

import com.yao2san.sim.framework.web.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class Datasource extends Pagination {
    private Long datasourceId;
    private String name;
    private String type;
}
