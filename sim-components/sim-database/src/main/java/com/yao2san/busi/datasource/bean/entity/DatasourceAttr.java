package com.yao2san.busi.datasource.bean.entity;

import com.yao2san.sim.framework.web.bean.BaseBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class DatasourceAttr extends BaseBean {
    private Long datasourceAttrId;
    private Long datasourceId;
    private String attrCode;
    private String attrValue;

}
