package com.yao2san.busi.datasource.bean.response;

import com.yao2san.busi.datasource.bean.entity.Datasource;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DatasourceRes extends Datasource {
    private String typeName;
    private String statusName;
    private String createUserName;
}
