package com.yao2san.sim.im.client;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.websocket.*;
import java.util.Objects;
import java.util.Set;

@ClientEndpoint
@Slf4j
@Component
public class WSHandler {
    @Autowired
    RedisTemplate<String,String> redisTemplate;
    private static RedisTemplate<String,String> redisTemplateService;

    @PostConstruct
    public void init() {
        redisTemplateService=redisTemplate;
    }

    @OnOpen
    public void onOpen(Session session) {
        WSClient.session = session;
    }

    @OnMessage
    public void processMessage(String message) {
        System.out.println(message);
    }

    @OnError
    public void processError(Throwable t) {
       log.error(t.getMessage());
    }

    @OnClose
    public void processClose(Session session, CloseReason closeReason) {
        log.error(session.getId() + closeReason.toString());
    }

    public void send(String sessionId, String message) {


    }
}