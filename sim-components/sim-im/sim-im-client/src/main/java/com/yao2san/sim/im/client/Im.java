package com.yao2san.sim.im.client;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@AllArgsConstructor
@Data
public class Im {
    public static final Map<String,Im> SESSION = new ConcurrentHashMap<>();
    private String clientId;
    private Session session;

    public String getKey(){
        return clientId;
    }
}
