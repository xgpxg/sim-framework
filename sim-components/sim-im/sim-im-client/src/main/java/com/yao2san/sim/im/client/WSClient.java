package com.yao2san.sim.im.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author ghh
 * @date 2019-08-16 16:02
 */
@Component
@Slf4j
public class WSClient {
    public static Session session;


    public static void main(String[] args) throws IOException, DeploymentException, InterruptedException {
        if (WSClient.session != null) {
            WSClient.session.close();
        }
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();

        List<Session> sessions = new ArrayList<>();
        for (int i = 0; i < 5000; i++) {
            String uri = "ws://127.0.0.1:8080/chat/" + i;
            WSHandler wsHandler = new WSHandler();
            Session session = container.connectToServer(wsHandler, URI.create(uri));
            sessions.add(session);
        }


        long ss = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                for (Session s : sessions) {
                    synchronized (s) {
                        s.getAsyncRemote().sendText("hello");
                    }
                }
            }).start();
        }
        long e = System.currentTimeMillis();
        System.out.println(e - ss);
        TimeUnit.SECONDS.sleep(1000);
    }
}