package com.yao2san.sim.im.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@Component
@ServerEndpoint("/chat/{clientId}")
@Slf4j
public class Endpoint {


    private static StringRedisTemplate redisTemplate;

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        Endpoint.redisTemplate = redisTemplate;
    }

    @OnOpen
    public void open(Session session, @PathParam("clientId") String clientId) throws IOException {
        Im im = new Im(clientId, session);
        Im.SESSION.put(im.getKey(), im);
        RemoteEndpoint.Basic endpoint = session.getBasicRemote();
        endpoint.sendText("客户端:" + clientId + "连接");
    }

    @OnClose
    public void close(Session session, @PathParam("clientId") String clientId) {
        log.info("客户端:" + clientId + "断开");
        Im.SESSION.remove(clientId);
    }

    @OnMessage
    public void message(String message, Session session, @PathParam("clientId") String clientId) throws IOException {
        //写队列
        ListOperations<String, String> opsForList = redisTemplate.opsForList();
        opsForList.leftPush("messages:his", message);

        //发布
        redisTemplate.convertAndSend("messages:pub",message);



        RemoteEndpoint.Basic endpoint = session.getBasicRemote();
        endpoint.sendText("转发消息("+clientId+"):" + message);
    }
}
