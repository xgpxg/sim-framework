package com.yao2san.sim.im.client;

import org.springframework.stereotype.Component;

@Component
public class MySubscriber implements Subscriber{

    @Override
    public void handleMessage(String message) {
        System.out.println(message);
    }

    @Override
    public String getTopic() {
        return "messages";
    }
}
