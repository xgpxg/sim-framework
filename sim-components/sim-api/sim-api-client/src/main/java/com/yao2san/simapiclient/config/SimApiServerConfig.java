package com.yao2san.simapiclient.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "sim-api.server")
@Configuration
@Data
public class SimApiServerConfig {
    private String addr;
}
