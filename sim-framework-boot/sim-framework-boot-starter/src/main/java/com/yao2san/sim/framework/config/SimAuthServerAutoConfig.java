
package com.yao2san.sim.framework.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.yao2san.sim.auth.server","com.yao2san.sim.auth.common"})
public class SimAuthServerAutoConfig {
}
