package com.yao2san.sim.framework.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author wxg
 * @since 2023-11-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic_code")
public class SysDicCode extends Model<SysDicCode> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Long id;

    @TableField("dic_code")
    private String dicCode;

    @TableField("dic_code_name")
    private String dicCodeName;

    @TableField("description")
    private String description;

    @TableField("is_show")
    private Integer isShow;

    /**
     * 分组
     */
    @TableField("group_name")
    private String groupName;

    /**
     * 状态：1正常 0停用
     */
    @TableField("status")
    private Integer status;

    @TableField("create_date")
    private LocalDateTime createDate;

    @TableField("update_date")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    /**
     * 租户ID
     */
    @TableField("tenant_id")
    private Long tenantId;

    /**
     * 是否删除：0否 1是
     */
    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
