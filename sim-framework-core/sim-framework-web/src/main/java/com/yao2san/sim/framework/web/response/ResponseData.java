package com.yao2san.sim.framework.web.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yao2san.sim.framework.web.exception.BusiException;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"success", "objectMapper"})
public class ResponseData<T> implements Serializable {
    private int code;
    private T data;
    private String msg;
    private String requestId;
    //private final long timestamp = System.currentTimeMillis();

    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ResponseData() {
    }

    private ResponseData(int status, String msg, T data) {
        this.code = status;
        this.msg = msg;
        this.data = data;
    }

    private ResponseData(int status, String msg) {
        this.code = status;
        this.msg = msg;
    }

    private ResponseData(ResponseCode status, String msg) {
        this.code = status.getCode();
        this.msg = msg;
    }

    public boolean isSuccess() {
        return this.code == ResponseCode.SUCCESS.getCode();
    }

    public String toJsonString() {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> ResponseData<T> success() {
        return new ResponseData<>(ResponseCode.SUCCESS.getCode(), "", null);
    }

    public static <T> ResponseData<T> success(T data) {
        return new ResponseData<>(ResponseCode.SUCCESS.getCode(), "", data);
    }

    public static <T> ResponseData<T> success(T data, String msg) {
        return new ResponseData<>(ResponseCode.SUCCESS.getCode(), msg, data);
    }

    public static <T> ResponseData<T> error() {
        return new ResponseData<>(ResponseCode.ERROR, "");
    }

    public static <T> ResponseData<T> error(String msg) {
        return new ResponseData<>(ResponseCode.ERROR, msg);
    }

    public static <T> ResponseData<T> businessError(String msg) {
        return new ResponseData<>(ResponseCode.BUSINESS_EXCEPTION, msg);
    }

    public static <T> ResponseData<T> error(int status, String msg) {
        return new ResponseData<>(status, msg);
    }

    public static <T> ResponseData<T> error(ResponseCode status) {
        return new ResponseData<>(status, "");
    }

    public static <T> ResponseData<T> error(ResponseCode status, String msg) {
        return new ResponseData<>(status, msg);
    }

    public static <T> ResponseData<T> accessRestricted(int status, String msg) {
        return new ResponseData<>(ResponseCode.ACCESS_RESTRICTED.getCode(), msg);
    }

    public static boolean isOk(ResponseData<?> responseData) {
        return responseData != null && responseData.getCode() == ResponseCode.SUCCESS.getCode();
    }

    public static BusiException exception(ResponseData<?> responseData) {
        return new BusiException(responseData.getCode(), responseData.getMsg());
    }

    /**
     * 响应是否成功，不成功则抛出异常（用于服务之间调用时，传递异常）
     */
    public ResponseData<T> okOrElseThrow() {
        if (this.isSuccess()) {
            return this;
        }
        throw new BusiException(this.code, this.msg);
    }

    /**
     * 响应是否成功，不成功则抛出异常并隐藏异常详细信息（用于服务之间调用时，传递异常）
     */
    public ResponseData<T> okOrElseThrowDefault() {
        if (this.isSuccess()) {
            return this;
        }
        throw new BusiException(this.code, "服务器开小差了，请稍后再试~");
    }

    public ResponseData<T> okOrElse(ResponseDataHandler<T> handler) {
        handler.handle(this);
        return this;
    }

    public interface ResponseDataHandler<T> {
        void handle(ResponseData<T> responseData);
    }
}
