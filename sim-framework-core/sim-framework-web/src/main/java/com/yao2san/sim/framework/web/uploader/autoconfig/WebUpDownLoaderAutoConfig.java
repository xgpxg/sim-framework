package com.yao2san.sim.framework.web.uploader.autoconfig;

import com.yao2san.sim.framework.web.uploader.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnProperty(value = "sim.web.up-down-loader.enabled", havingValue = "true")
@Import(WebUpDownloadProperties.class)
public class WebUpDownLoaderAutoConfig {
    @Bean
    public DefaultWebUploader defaultWebUploader(WebUpDownloadProperties properties, FolderPolicy folderPolicy, FileNameRewriter fileNameRewriter) {
        DefaultWebUploader uploader = new DefaultWebUploader();
        uploader.setProperties(properties);
        uploader.setPolicy(folderPolicy);
        uploader.setFileNameRewriter(fileNameRewriter);
        return uploader;
    }
    @Bean
    public DefaultWebDownloader defaultWebDownloader(WebUpDownloadProperties properties) {
        DefaultWebDownloader downloader = new DefaultWebDownloader();
        downloader.setProperties(properties);
        return downloader;
    }

    @ConditionalOnMissingBean(FolderPolicy.class)
    @Bean
    public DefaultFolderPolicy dateFolderPolicy(WebUpDownloadProperties properties) {
        DefaultFolderPolicy defaultFolderPolicy = new DefaultFolderPolicy();
        defaultFolderPolicy.setProperties(properties);
        return defaultFolderPolicy;
    }

    @ConditionalOnMissingBean(FileNameRewriter.class)
    @Bean
    public DefaultFileNameRewriter defaultFileNameRewriter() {
        return new DefaultFileNameRewriter();
    }
}
