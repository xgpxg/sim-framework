package com.yao2san.sim.framework.web.mybatis.sharding;

public interface ShardingStrategy {
    String getTableName(String tableName);
}
