package com.yao2san.sim.framework.web.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
public class ResponsePageData<T> {
    private long pageNum = 1;
    private long pageSize = 10;
    private long total;
    private List<T> list;
    private Map<Object, Object> extra;

    public ResponsePageData<T> appendExtra(Object k, Object v) {
        if (extra == null) {
            extra = new HashMap<>();
            extra.put(k, v);
        } else {
            extra.put(k, v);
        }
        return this;
    }
}
