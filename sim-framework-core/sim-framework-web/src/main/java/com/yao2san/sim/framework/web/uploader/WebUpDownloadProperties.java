package com.yao2san.sim.framework.web.uploader;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wxg
 **/
@Configuration
@ConfigurationProperties(prefix = "sim.web.up-down-loader")
@Data
@Deprecated
public class WebUpDownloadProperties {
    private Boolean enabled;
    private String rootPath;
    private StoreType storeType;
    private Boolean overwrite = false;
    private Boolean hiddenRealPath = false;
    private RemoteProperties remote;
    private FtpProperties ftp;
    private SftpProperties sftp;



    @Configuration
    @ConfigurationProperties(prefix = "sim.web.up-down-loader.remote")
    @Data
    public static class RemoteProperties {
        private String host;
        private String port;
        private String username;
        private String password;
    }

    @Configuration
    @ConfigurationProperties(prefix = "sim.web.up-down-loader.ftp")
    @Data
    public static class FtpProperties {
        private String host;
        private String port;
        private String username;
        private String password;
    }

    @Configuration
    @ConfigurationProperties(prefix = "sim.web.up-down-loader.sftp")
    @Data
    public static class SftpProperties {
        private String host;
        private String port;
        private String username;
        private String password;
    }


    public enum StoreType {
        LOCAL, REMOTE, FTP, SFTP
    }
}
