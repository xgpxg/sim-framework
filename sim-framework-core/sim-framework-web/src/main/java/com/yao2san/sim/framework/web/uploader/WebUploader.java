package com.yao2san.sim.framework.web.uploader;

import com.yao2san.sim.framework.utils.BeanContextUtil;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Deprecated
@SuperBuilder
@AllArgsConstructor
public class WebUploader extends WebUploaderConfig {

    public UploadResult upload(MultipartFile file) {
        try {
            WebUploaderConfig config = WebUploaderConfig.builder()
                    .fileName(fileName)
                    .rewriteFileName(rewriteFileName)
                    .overwrite(overwrite)
                    .build();
            DefaultWebUploader uploader = BeanContextUtil.getBean(DefaultWebUploader.class);
            return uploader.upload(file, config);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
