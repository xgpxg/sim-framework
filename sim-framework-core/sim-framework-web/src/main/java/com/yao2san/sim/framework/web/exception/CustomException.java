package com.yao2san.sim.framework.web.exception;

/**
 * 自定义异常
 */
public interface CustomException {
    int getCode();

    String getMessage();
}
