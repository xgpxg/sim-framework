package com.yao2san.sim.framework.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author wxg
 * @since 2023-11-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic_item")
public class SysDicItem extends Model<SysDicItem> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Long id;

    @TableField("sys_dic_code_id")
    private Long sysDicCodeId;

    /**
     * 字典项
     */
    @TableField("item_code")
    private String itemCode;

    /**
     * 字典值
     */
    @TableField("item_text")
    private String itemText;

    @TableField("item_value")
    private String itemValue;

    /**
     * 字典编码
     */
    @TableField("dic_code")
    private String dicCode;

    /**
     * 说明
     */
    @TableField("description")
    private String description;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 状态：1正常 0停用
     */
    @TableField("status")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField("create_date")
    private LocalDateTime createDate;

    @TableField("update_date")
    private LocalDateTime updateDate;

    @TableField("remark")
    private String remark;

    /**
     * 租户ID
     */
    @TableField("tenant_id")
    private Long tenantId;

    /**
     * 是否删除：0否 1是
     */
    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
