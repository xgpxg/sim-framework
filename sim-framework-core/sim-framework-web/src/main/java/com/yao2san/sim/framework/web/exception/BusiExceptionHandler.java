package com.yao2san.sim.framework.web.exception;

import com.alibaba.fastjson.JSONObject;
import com.netflix.client.ClientException;
import com.yao2san.sim.framework.web.response.ResponseCode;
import com.yao2san.sim.framework.web.response.ResponseData;
import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务全局异常处理
 * 抛出异常时记录请求参数，服务名，traceId，异常原因，请求IP，sessionId，userId等等信息
 */
@ControllerAdvice
@SuppressWarnings("all")
@Slf4j
public class BusiExceptionHandler {
    //@Autowired(required = false)
    //protected Tracer tracer;

    @Value("${spring.application.name}")
    public String SERVICE_NAME;

/*
    @Override
    public String getServiceName() {
        return this.SERVICE_NAME;
    }
    abstract public String getServiceName();*/

    /**
     * 处理业务异常
     *
     * @param e       业务异常
     * @param request 请求内容
     * @return 异常信息
     */
    @ResponseBody
    @ExceptionHandler(BusiException.class)
    public ResponseData handlerBusiException(BusiException e, HttpServletRequest request) {
        log.error("", e);
        //TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        String params = JSONObject.toJSONString(request.getParameterMap());
        //返回错误信息(不包含详细信息)
        ResponseData responseData = ResponseData.error(e.getCode(), e.getMessage());
        //请求方法
        String method = request.getMethod();
        //服务名称
        String serviceName = this.SERVICE_NAME;
        //traceId
        //String traceId = tracer.currentSpan().context().traceIdString();

        //log.error("SERVICE:{};ERROR:{};TRACER-ID:{};METHOD:{};PARAMS:{}",serviceName,e.getMessage(),traceId,method,params);
        log.error("SERVICE:{};ERROR:{};METHOD:{}", serviceName, e.getMessage(), method);
        return responseData;
    }

    /**
     * 处理未知异常
     *
     * @param e       异常
     * @param request 请求
     * @return
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseData handlerException(Exception e, HttpServletRequest request) throws IOException {
        log.error("", e);
        //TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        if (e instanceof BindException) {
            return handleBindException((BindException) e);
        }
        if (e instanceof MethodArgumentNotValidException) {
            return handleMethodArgumentNotValidException((MethodArgumentNotValidException) e);
        }
        if (e instanceof BadSqlGrammarException) {
            if (e.getMessage().contains("command denied to user")) {
                return handleNotUpdateDeletePermissionException((BadSqlGrammarException) e);
            }
        }
        if (e.getCause() instanceof ClientException) {
            if (e.getMessage().contains("Load balancer does not have available server for client")) {
                return handleNotAvailableServerException((ClientException) e.getCause());
            }
        }
        //请求方法
        String method = request.getMethod();
        //服务名称
        String serviceName = this.SERVICE_NAME;
        //traceId
        //String traceId = tracer.currentSpan().context().traceIdString();

        //log.error("SERVICE:{};ERROR:{};TRACER-ID:{};METHOD:{};PARAMS:{}",serviceName,e.getMessage(),traceId,method,params);
        log.error("SERVICE:{};ERROR:{};METHOD:{}", serviceName, e.getMessage(), method);
        //返回错误信息(不包含详细信息)
        return ResponseData.error(ResponseCode.SERVER_ERROR, e.getMessage());
    }

    /**
     * Handle validation exception
     */
    private ResponseData handleBindException(BindException e) {
        log.error("Rest service exception", e);
        List<FieldError> fieldErrorList = e.getBindingResult().getFieldErrors();
        String message = getMessage(fieldErrorList);
        return ResponseData.error(ResponseCode.ILLEGAL_ARGUMENT, message);
    }

    /**
     * Handle validation exception
     */
    private ResponseData handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error("Rest service exception", e);
        List<FieldError> fieldErrorList = e.getBindingResult().getFieldErrors();
        String message = getMessage(fieldErrorList);
        return ResponseData.error(ResponseCode.ILLEGAL_ARGUMENT, message);
    }

    private ResponseData handleNotUpdateDeletePermissionException(BadSqlGrammarException e) {
        log.error("Rest service exception", e);
        return ResponseData.businessError("current user is view only");
    }

    private ResponseData handleNotAvailableServerException(ClientException e) {
        log.error("Service invoke exception", e);
        return ResponseData.error(ResponseCode.SERVICE_UNAVAILABLE, ResponseCode.SERVICE_UNAVAILABLE.getDesc());
    }

    @ExceptionHandler(RetryableException.class)
    @ResponseBody
    private ResponseData handleRetryableException(RetryableException e) {
        log.error("Service invoke error", e);
        return ResponseData.error(ResponseCode.READ_TIMEOUT, ResponseCode.READ_TIMEOUT.getDesc());
    }

    @ExceptionHandler(MessageException.class)
    @ResponseBody
    private ResponseData handleMessageException(MessageException e) {
        return ResponseData.error(ResponseCode.BUSINESS_EXCEPTION, e.getMessage());
    }

    private String getMessage(List<FieldError> fieldErrorList) {
        List<String> messages = new ArrayList<>(8);
        if (!CollectionUtils.isEmpty(fieldErrorList)) {
            for (FieldError fieldError : fieldErrorList) {
                if (fieldError != null && fieldError.getDefaultMessage() != null) {
                    messages.add(fieldError.getDefaultMessage());
                }
            }
        }
        return String.join(",", messages);
    }

}
