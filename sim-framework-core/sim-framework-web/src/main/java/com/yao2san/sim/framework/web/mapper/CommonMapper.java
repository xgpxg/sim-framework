package com.yao2san.sim.framework.web.mapper;

import com.yao2san.sim.framework.web.entity.SysDicItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CommonMapper {
    /**
     * 查询数据字典列表
     *
     * @param code 字典code
     * @return result
     */
    List<SysDicItem> getDicList(@Param("dicCode") String code);

    /**
     * 查询字典项
     *
     * @param dicCode  字典code
     * @param itemCode 字典项code
     * @return result
     */
    SysDicItem getDicItem(@Param("dicCode") String dicCode, @Param("itemCode") String itemCode);

    /**
     * 查询单个字典值(仅有一个字典项)
     *
     * @param dicCode 字典code
     * @return result
     */
    String getSignalDicCodeValue(@Param("dicCode") String dicCode);
}
