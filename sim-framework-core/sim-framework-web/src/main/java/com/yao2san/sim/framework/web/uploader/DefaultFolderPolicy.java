package com.yao2san.sim.framework.web.uploader;

import com.yao2san.sim.framework.utils.CommonUtil;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateFormatUtils;

import java.io.File;
import java.util.Date;

/**
 * @author wxg
 **/
@Slf4j
public class DefaultFolderPolicy implements FolderPolicy {
    private static final String YYYYMMDD = "yyyyMMdd";
    @Setter
    private WebUpDownloadProperties properties;


    @Override
    public String getRootPath() {
        return properties.getRootPath();
    }

    @Override
    public String create() {
        String rootPath = getRootPath();
        String path = rootPath + "/" + DateFormatUtils.format(new Date(), YYYYMMDD);
        path = path.endsWith("/") ? path.substring(0, path.length() - 1) : path;
        path = CommonUtil.formatPath(path);
        File f = new File(path);
        if (!f.exists()) {
            boolean mkdir = f.mkdir();
            if (!mkdir) {
                log.error("Folder create fail");
            }
        }
        return path;
    }

    @Override
    public FileNameRewriter fileNameRewriter() {
        return new DefaultFileNameRewriter();
    }
}
