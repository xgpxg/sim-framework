package com.yao2san.sim.framework.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yao2san.sim.framework.web")
public class SimFrameworkWebAutoConfig {
}
