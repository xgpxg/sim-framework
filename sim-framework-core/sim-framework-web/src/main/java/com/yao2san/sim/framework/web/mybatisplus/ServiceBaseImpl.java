package com.yao2san.sim.framework.web.mybatisplus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yao2san.sim.framework.web.bean.Pagination;
import com.yao2san.sim.framework.web.response.ResponsePageData;

import java.util.List;


public abstract class ServiceBaseImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements IService<T> {
    protected <R> Page<R> convertPage(Pagination pagination) {
        return new Page<>(pagination.getPageNum(), pagination.getPageSize());
    }

    protected <R> ResponsePageData<R> buildPageResult(Page<R> page, List<R> list) {
        ResponsePageData<R> pageData = new ResponsePageData<>();
        pageData.setPageNum(page.getCurrent())
                .setPageSize(page.getSize())
                .setTotal(page.getTotal())
                .setList(list);
        return pageData;
    }
}
