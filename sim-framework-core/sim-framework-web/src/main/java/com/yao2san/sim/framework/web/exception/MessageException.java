package com.yao2san.sim.framework.web.exception;

import com.yao2san.sim.framework.web.response.ResponseCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MessageException extends BusiException {
    private int code;
    private String message;
    private CustomException customException;
    public MessageException() {
        super();
    }

    public MessageException(String message) {
        this.code = ResponseCode.ERROR.getCode();
        this.message = message;
    }

    public MessageException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public MessageException(CustomException customException) {
        super(customException.getMessage());
        this.code = customException.getCode();
        this.message = customException.getMessage();
    }
}
