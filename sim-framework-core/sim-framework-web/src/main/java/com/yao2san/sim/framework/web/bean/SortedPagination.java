package com.yao2san.sim.framework.web.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wxg
 * 分页
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SortedPagination extends Pagination {
    /**
     * 排序字段
     */
    private String sortBy;
    /**
     * 排序方式：asc、desc
     */
    private String sortType = "asc";
}
