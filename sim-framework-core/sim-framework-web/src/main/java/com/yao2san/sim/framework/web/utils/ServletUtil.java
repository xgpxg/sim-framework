package com.yao2san.sim.framework.web.utils;

import com.yao2san.sim.framework.utils.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ServletUtil {
    public static final String TOKEN = "token";
    public static final String ACCESS_TOKEN_HEADER = "AccessToken";
    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static HttpServletRequest getHttpServletRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            return servletRequestAttributes.getRequest();
        } else {
            throw new RuntimeException("Current request is not http request");
        }

    }

    public static HttpServletResponse getHttpServletResponse() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            return servletRequestAttributes.getResponse();
        } else {
            throw new RuntimeException("Current request is not http request");
        }

    }

    /**
     * 获取请求头
     *
     * @param headerName 请求头名称
     * @return 请求头值
     */
    public static String getHeader(String headerName) {
        return getHeader(headerName, null);
    }

    /**
     * 获取请求头
     *
     * @param headerName   请求头名称
     * @param defaultValue 默认值
     * @return 请求头值
     */
    public static String getHeader(String headerName, String defaultValue) {
        String header = ServletUtil.getHttpServletRequest().getHeader(headerName);
        return header == null ? defaultValue : header;
    }

    public static String getToken() {
        String token;
        HttpServletRequest request = getHttpServletRequest();
        token = request.getHeader(TOKEN);
        token = StringUtils.isEmpty(token) ? request.getParameter(TOKEN) : token;
        return token;
    }

    public static String getAuthorization() {
        HttpServletRequest request = getHttpServletRequest();
        String header = request.getHeader(AUTHORIZATION_HEADER);
        String[] s = header.split(" ");
        if (s.length < 2) {
            return null;
        }
        return s[1];
    }

    public static String getAccessToken() {
        String token;
        HttpServletRequest request = getHttpServletRequest();
        token = request.getHeader(ACCESS_TOKEN_HEADER);
        token = StringUtils.isEmpty(token) ? request.getParameter(ACCESS_TOKEN_HEADER) : token;
        return token;
    }

    public static String getRequestIp() {
        try {
            HttpServletRequest request = getHttpServletRequest();
            return IpUtil.getClientIp(request);
        } catch (Exception e) {
            log.info("{}", e.getMessage());
            return null;
        }
    }

    public static String getRequestBody(HttpServletRequest request) {
        StringBuilder data = new StringBuilder();
        String line;
        BufferedReader reader;
        try {
            reader = request.getReader();
            while (null != (line = reader.readLine())) {
                data.append(line);
            }
            return data.toString();
        } catch (IOException e) {
            log.error("", e);
            return null;
        }
    }

    public static Map<String, String> getQueryParams(HttpServletRequest request) {
        Enumeration<String> parameterNames = request.getParameterNames();
        if (parameterNames != null && parameterNames.hasMoreElements()) {
            Map<String, String> uriVariables = new HashMap<>();
            while (parameterNames.hasMoreElements()) {
                String name = parameterNames.nextElement();
                uriVariables.put(name, request.getParameter(name));
            }
            return uriVariables;
        }
        return Collections.emptyMap();
    }

    public static Map<String, String> getQueryParams(String queryString) {
        Map<String, String> map = new HashMap<>();
        String[] param = queryString.split("&");
        for (String kv : param) {
            String[] pair = kv.split("=");
            if (pair.length == 2) {
                map.put(pair[0], pair[1]);
            }
        }
        return map;
    }
}
