package com.yao2san.sim.framework.web.uploader;

/**
 * @author wxg
 **/
public interface FolderPolicy {
    /**
     * get upload or download root path.
     *
     * @return root path.
     */
    String getRootPath();

    /**
     * create folder.
     *
     * @return path
     */
    String create();

    FileNameRewriter fileNameRewriter();
}
