package com.yao2san.sim.framework.cloud.config;

import com.alibaba.cloud.nacos.NacosConfigAutoConfiguration;
import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.yao2san.sim.framework.cloud.exception.NacosConfigNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * @author wxg
 * Nacos configuration acquisition tool
 */
@Component
@ConditionalOnClass(NacosConfigAutoConfiguration.class)
@ConditionalOnProperty(name = "spring.cloud.nacos.config.enabled", havingValue = "true", matchIfMissing = true)
@ImportAutoConfiguration(NacosConfigProperties.class)
@Slf4j
public class CloudConfig {
    private static NacosConfigProperties nacosConfigProperties;
    private static final Properties ALL_CONFIG = new Properties();
    private static final YamlPropertiesFactoryBean YAML_PROCESSOR = new YamlPropertiesFactoryBean();


    @Autowired
    public void setNacosConfigProperties(NacosConfigProperties nacosConfigProperties) {
        CloudConfig.nacosConfigProperties = nacosConfigProperties;
    }


    /**
     * Get a config value with key
     *
     * @param key key
     * @return value
     */
    public static String getValue(String key) {
        return getValueOrDefault(key, null);
    }

    /**
     * Get a config value with key,if value is null, return default value
     *
     * @param key          key
     * @param defaultValue default value
     * @return value
     */
    public static String getValueOrDefault(String key, String defaultValue) {
        return String.valueOf(ALL_CONFIG.getOrDefault(key, defaultValue));
    }


    @PostConstruct
    private static void sync() {
        Properties properties = new Properties();
        properties.put("serverAddr", nacosConfigProperties.getServerAddr());
        properties.put("namespace", nacosConfigProperties.getNamespace());
        try {
            ConfigService configService = NacosFactory.createConfigService(properties);
            if (nacosConfigProperties.getGroup() != null) {
                log.info("Loading group config:{}", String.join(",", nacosConfigProperties.getGroup()));
                String dataId = nacosConfigProperties.getName() + "." + nacosConfigProperties.getFileExtension();
                String config = configService.getConfig(dataId, nacosConfigProperties.getGroup(), 3000);
                if (config == null) {
                    String expMsg = String.format("No configuration file found.group:%s,dataId:%s", nacosConfigProperties.getGroup(), dataId);
                    log.warn(expMsg);
                    return;
                }
                YAML_PROCESSOR.setResources(new InputStreamResource(new ByteArrayInputStream(config.getBytes())));
                ALL_CONFIG.putAll(YAML_PROCESSOR.getObject());
            }
        } catch (NacosException e) {
            log.error("create nacos config service error", e);
        }
    }
    @PostConstruct
    private static void setListener() {
        Properties properties = new Properties();
        properties.put("serverAddr", nacosConfigProperties.getServerAddr());
        properties.put("namespace", nacosConfigProperties.getNamespace());
        ConfigService configService;
        try {
            configService = NacosFactory.createConfigService(properties);
            if (nacosConfigProperties.getGroup() != null) {
                log.info("add config listener,group:{}", nacosConfigProperties.getGroup());
                String dataId = nacosConfigProperties.getName() + "." + nacosConfigProperties.getFileExtension();
                configService.addListener(dataId, nacosConfigProperties.getGroup(), new Listener() {
                    @Override
                    public Executor getExecutor() {
                        return null;
                    }

                    @Override
                    public void receiveConfigInfo(String s) {
                        log.info("receive config change,reload all config");
                        sync();
                    }
                });
            }
        } catch (NacosException e) {
            log.error("create nacos config service error.", e);
        }
    }
}
