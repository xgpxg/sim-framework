package com.yao2san.sim.framework.cache.reatelimit;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RateLimitConfig {

    @Bean
    public KeyPolicy defaultKeyPolicy() {
        return new DefaultKeyPolicy();
    }

    @Bean
    public KeyPolicy ipKeyPolicy() {
        return new IpKeyPolicy();
    }

    @Bean
    @ConditionalOnMissingBean(FallbackPolicy.class)
    public FallbackPolicy defaultRejectPolicy() {
        return new DefaultFallbackPolicy();
    }
}
