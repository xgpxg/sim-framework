package com.yao2san.sim.framework.cache.utils;

import org.redisson.api.RRateLimiter;
import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Component
public class RateLimitUtil {
    private static RedissonClient redissonClient;

    private static final Map<String, RRateLimiter> RATE_LIMITER_MAP = new ConcurrentHashMap<>();

    @Autowired
    public void setRedissonClient(RedissonClient redissonClient) {
        RateLimitUtil.redissonClient = redissonClient;
    }

    public static RRateLimiter addRateLimiter(String key, long rate, long rateInterval, RateIntervalUnit rateIntervalUnit) {
        RRateLimiter rateLimiter = redissonClient.getRateLimiter(key);
        rateLimiter.setRate(RateType.OVERALL, rate, rateInterval, rateIntervalUnit);
        RATE_LIMITER_MAP.put(key, rateLimiter);
        return rateLimiter;
    }

    public static RRateLimiter addRateLimiterIfAbsent(String key, long rate, long rateInterval, RateIntervalUnit rateIntervalUnit) {
        RRateLimiter limiter = RATE_LIMITER_MAP.get(key);
        if (limiter != null) {
            return limiter;
        } else {
            return addRateLimiter(key, rate, rateInterval, rateIntervalUnit);
        }
    }

    public static void acquire(String key) {
        RRateLimiter limiter = RATE_LIMITER_MAP.get(key);
        if (limiter != null) {
            limiter.acquire();
        } else {
            throw new IllegalArgumentException("限流器未初始化, key=" + key);
        }
    }

    public static boolean tryAcquire(String key) {
        RRateLimiter limiter = RATE_LIMITER_MAP.get(key);
        if (limiter != null) {
            return limiter.tryAcquire();
        } else {
            throw new IllegalArgumentException("限流器未初始化, key=" + key);
        }
    }
}
