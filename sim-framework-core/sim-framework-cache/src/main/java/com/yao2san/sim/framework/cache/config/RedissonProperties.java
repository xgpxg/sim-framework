package com.yao2san.sim.framework.cache.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@ConfigurationProperties(prefix = "spring.redis.redisson")
@Configuration
@Data
public class RedissonProperties {
    private Boolean enable;
    private Mode mode;
    private List<String> nodes;

    public enum Mode {
        SINGLE,
        CLUSTER
    }
}
