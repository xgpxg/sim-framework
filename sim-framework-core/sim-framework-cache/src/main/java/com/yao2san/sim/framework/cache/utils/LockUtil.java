package com.yao2san.sim.framework.cache.utils;


import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class LockUtil {

    private final static int DEFAULT_LEASE_TIME = 30;
    private static RedissonClient redissonClient;

    @Autowired
    public void setRedissonClient(RedissonClient redissonClient) {
        LockUtil.redissonClient = redissonClient;
    }

    /**
     * 获取锁
     *
     * @param key      锁的key
     * @param waitTime 等待时间，单位：秒
     * @return 是否获取到锁
     */
    public static boolean tryLock(String key, long waitTime) {
        RLock lock = redissonClient.getLock(key);
        try {
            return lock.tryLock(waitTime, DEFAULT_LEASE_TIME, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error("Redis锁获取异常", e);
        }
        return false;
    }

    /**
     * 释放锁
     *
     * @param key 锁的key
     */
    public static void unlock(String key) {
        RLock lock = redissonClient.getLock(key);
        if (lock.isLocked()) {
            lock.unlock();
        }
    }
}
