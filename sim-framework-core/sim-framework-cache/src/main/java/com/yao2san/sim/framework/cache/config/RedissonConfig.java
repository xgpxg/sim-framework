package com.yao2san.sim.framework.cache.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.stream.Collectors;

@Configuration
@Import(RedissonProperties.class)
@ConditionalOnProperty(prefix = "spring.redis.redisson", name = "enable", havingValue = "true")
public class RedissonConfig {
    private final RedissonProperties properties;
    private final RedisProperties redisProperties;

    public RedissonConfig(RedissonProperties properties, RedisProperties redisProperties) {
        this.properties = properties;
        this.redisProperties = redisProperties;
    }

    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        if (properties.getMode() == RedissonProperties.Mode.SINGLE) {
            config.useSingleServer().setAddress("redis://" + redisProperties.getHost() + ":" + redisProperties.getPort());
        }
        if (properties.getMode() == RedissonProperties.Mode.CLUSTER) {
            config.useClusterServers().setNodeAddresses(
                    redisProperties.getCluster().getNodes()
                            .stream()
                            .map(v -> "redis://" + v).collect(Collectors.toList())
            );
        }
        return Redisson.create(config);

    }
}
