package com.yao2san.sim.framework.cache.reatelimit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RateLimit {
    int limit();

    int interval() default 1;

    String key() default "";

    int waitTime() default 0;

    String[] keyPolicy() default "default";

}
