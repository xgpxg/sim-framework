package com.yao2san.sim.framework.cache.reatelimit;

import com.google.common.util.concurrent.RateLimiter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
@SuppressWarnings("all")
public class RateLimitAspect {
    private static final Map<String, RateLimiter> LIMITER = new ConcurrentHashMap<>();

    private List<KeyPolicy> keyPolicies;

    private FallbackPolicy fallbackPolicy;

    public RateLimitAspect(List<KeyPolicy> keyPolicies, FallbackPolicy fallbackPolicy) {
        this.keyPolicies = keyPolicies;
        this.fallbackPolicy = fallbackPolicy;
    }


    @Around("@annotation(rateLimit)")
    public Object before(ProceedingJoinPoint joinPoint, RateLimit rateLimit) throws Throwable {
        double interval = rateLimit.interval();
        int limit = rateLimit.limit();
        int waitTime = rateLimit.waitTime();
        String[] policyNames = rateLimit.keyPolicy();
        double permitsPerSecond = limit / interval;

        String key = rateLimit.key();
        if (key.isEmpty()) {
            //Key policies with spring bean
            for (KeyPolicy keyPolicy : keyPolicies) {
                //key policy name
                String policyName = keyPolicy.getName();
                if (policyName == null || "".equals(policyName)) {
                    throw new NullPointerException("The key policy name not be empty");
                }
                for (String name : policyNames) {
                    if (name.equalsIgnoreCase(policyName)) {
                        key = key + ":" + keyPolicy.getKey(joinPoint);
                    }
                }
            }
            key = key.substring(1);
        }

        if (LIMITER.get(key) == null) {
            LIMITER.put(key, RateLimiter.create(permitsPerSecond));
        } else {
            boolean b = LIMITER.get(key).tryAcquire(1, waitTime, TimeUnit.MILLISECONDS);
            if (!b) {
                return fallbackPolicy.fallback(key);
            }
        }
        return joinPoint.proceed();
    }
}
