package com.yao2san.sim.framework.cache.reatelimit;

import java.io.IOException;

public interface FallbackPolicy {
    Object fallback(String key) throws IOException;
}
