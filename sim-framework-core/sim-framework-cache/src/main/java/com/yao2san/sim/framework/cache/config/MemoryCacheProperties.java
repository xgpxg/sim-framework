package com.yao2san.sim.framework.cache.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "spring.cache.memory")
@Configuration
@Data
public class MemoryCacheProperties {
    private int capacity;
    private int cleanThreshold = 100000;
}
