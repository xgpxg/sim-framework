package com.yao2san.sim.framework.cache.config;

import com.yao2san.sim.framework.cache.cache.MemoryCache;
import com.yao2san.sim.framework.cache.cache.RedisCache;
import com.yao2san.sim.framework.cache.cache.SimCache;
import com.yao2san.sim.framework.cache.exception.NoSimCacheBeanConfiguredException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@Import({MemoryCacheProperties.class,RedisCacheConfig.class})
public class CacheAutoConfig {

    @Bean
    @ConditionalOnProperty(prefix = "spring.cache", name = "type", havingValue = "redis",matchIfMissing = true)
    public SimCache redisCache(RedisTemplate<String, Object> redisTemplate) {
        return new RedisCache(redisTemplate);
    }

    @Bean
    @ConditionalOnProperty(prefix = "spring.cache", name = "type", havingValue = "simple")
    public SimCache memoryCache(MemoryCacheProperties properties) {
        return new MemoryCache(properties);
    }

    @Bean
    @ConditionalOnMissingBean(SimCache.class)
    public SimCache missingCache(){
        throw new NoSimCacheBeanConfiguredException();
    }
}
