package com.yao2san.sim.framework.utils.idutil;

public class IdUtil {
    private final static Sequence SEQUENCE = new Sequence(null);

    public static long nextId() {
        return SEQUENCE.nextId();
    }
}
