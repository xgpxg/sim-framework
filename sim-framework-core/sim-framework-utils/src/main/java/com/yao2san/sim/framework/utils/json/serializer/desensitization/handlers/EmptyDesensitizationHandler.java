package com.yao2san.sim.framework.utils.json.serializer.desensitization.handlers;

import com.yao2san.sim.framework.utils.json.serializer.desensitization.DesensitizationHandler;

public class EmptyDesensitizationHandler implements DesensitizationHandler {

    @Override
    public String desensitize(String value) {
        return value;
    }

}