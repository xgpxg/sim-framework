package com.yao2san.sim.framework.utils.json.serializer.desensitization;

public interface DesensitizationHandler {
    String desensitize(String value);
}
