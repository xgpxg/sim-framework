package com.yao2san.sim.framework.utils.json.serializer.desensitization;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yao2san.sim.framework.utils.json.serializer.desensitization.handlers.EmptyDesensitizationHandler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = DefaultDesensitizationSerializer.class)
public @interface DesensitizationSerializer {
    /**
     * 脱敏类型
     *
     * @see DesensitizationType
     */
    DesensitizationType type();

    /**
     * 自定义脱敏处理器
     */
    Class<? extends DesensitizationHandler> handler() default EmptyDesensitizationHandler.class;

}