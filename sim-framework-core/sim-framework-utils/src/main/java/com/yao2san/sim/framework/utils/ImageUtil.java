package com.yao2san.sim.framework.utils;

import com.idrsolutions.image.png.PngCompressor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import sun.misc.BASE64Encoder;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * 图片工具
 *
 * @author wxg
 */
@Slf4j
public class ImageUtil {

    private final static Set<String> IMAGE_TYPE_SET = new HashSet<>();

    static {
        IMAGE_TYPE_SET.add("jpg");
        IMAGE_TYPE_SET.add("jpeg");
        IMAGE_TYPE_SET.add("png");
        IMAGE_TYPE_SET.add("webp");
        IMAGE_TYPE_SET.add("tiff");
        IMAGE_TYPE_SET.add("bmp");
        IMAGE_TYPE_SET.add("svg");
        IMAGE_TYPE_SET.add("raw");
        IMAGE_TYPE_SET.add("gif");
    }

    /**
     * 图片文件转base64
     *
     * @param file
     * @return
     */
    @SuppressWarnings("all")
    public static String Image2Bse64(File file) {
        long size = file.length();
        byte[] imageByte = new byte[(int) size];
        FileInputStream fs = null;
        BufferedInputStream bis = null;
        try {
            fs = new FileInputStream(file);
            bis = new BufferedInputStream(fs);
            bis.read(imageByte);
        } catch (IOException e) {
            log.error("error", e);
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    log.error("error", e);
                }
            }
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException e) {
                    log.error("error", e);

                }
            }
        }
        return (new BASE64Encoder()).encode(imageByte);
    }

    /**
     * 压缩图片，尺寸保持不变，输出jpg格式
     *
     * @param input         input
     * @param outputQuality 图片质量，0~1
     * @param output        output
     */
    public static void compress(InputStream input, double outputQuality, OutputStream output) {
        try {
            Thumbnails.of(input).imageType(BufferedImage.TYPE_INT_ARGB).scale(1).outputQuality(outputQuality).outputFormat("jpg").toOutputStream(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 压缩图片，尺寸保持不变，输出jpg格式
     *
     * @param url           url
     * @param outputQuality 图片质量，0~1
     * @param output        output
     */
    public static void compress(URL url, double outputQuality, OutputStream output) {
        try {
            Thumbnails.of(url).imageType(BufferedImage.TYPE_INT_ARGB).scale(1).outputQuality(outputQuality).outputFormat("jpg").toOutputStream(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 压缩PNG图片，尺寸保持不变，输出PNG格式
     *
     * @param input  input
     * @param output output
     */
    public static void compressPng(InputStream input, OutputStream output) {
        try {
            PngCompressor.compress(input, output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 压缩PNG图片，尺寸保持不变，输出PNG格式
     *
     * @param url    url
     * @param output output
     */
    public static void compressPng(URL url, OutputStream output) {
        try (InputStream inputStream = url.openConnection().getInputStream()) {
            PngCompressor.compress(inputStream, output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 生成缩略图。保持原图格式
     *
     * @param input  input
     * @param scale  缩放比例，0~1
     * @param output output
     */
    public static void thumbnail(InputStream input, double scale, OutputStream output) {
        try {
            Thumbnails.of(input).scale(scale).toOutputStream(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 生成缩略图。保持原图格式
     *
     * @param url    input
     * @param scale  缩放比例，0~1
     * @param output output
     */
    public static void thumbnail(URL url, double scale, OutputStream output) {
        try {
            Thumbnails.of(url).scale(scale).toOutputStream(output);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 是否是图片
     *
     * @param suffix 文件后缀
     * @return true or false
     */
    public static boolean isImage(String suffix) {
        if (suffix == null || "".equals(suffix)) {
            return false;
        }
        return IMAGE_TYPE_SET.contains(suffix.toLowerCase());
    }
}
