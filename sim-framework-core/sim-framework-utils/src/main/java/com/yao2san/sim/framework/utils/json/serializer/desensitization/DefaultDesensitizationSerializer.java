package com.yao2san.sim.framework.utils.json.serializer.desensitization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

import java.io.IOException;

public class DefaultDesensitizationSerializer extends StdScalarSerializer<String> implements ContextualSerializer {

    private DesensitizationHandler handler;

    public DefaultDesensitizationSerializer() {
        super(String.class, false);
    }

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(handler.desensitize(value));
    }


    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) {
        DesensitizationSerializer annotation = property.getAnnotation(DesensitizationSerializer.class);
        if (annotation != null) {
            DesensitizationType type = annotation.type();
            if (type == DesensitizationType.CUSTOM) {
                Class<? extends DesensitizationHandler> clazz = annotation.handler();
                this.handler = DesensitizationHandlerSupport.registerHandler(clazz);
            } else {
                this.handler = DesensitizationHandlerSupport.DefaultHandler.ofHandler(type);
            }
            return this;
        }
        return null;
    }
}
