package com.yao2san.sim.framework.utils.json.serializer.desensitization.handlers;

import cn.hutool.core.util.ReUtil;
import com.yao2san.sim.framework.utils.json.serializer.desensitization.DesensitizationHandler;

import java.util.List;

public class MobileDesensitizationHandler implements DesensitizationHandler {
    private static final String REPLACE = "****";
    private static final String REG = "((1[3-9])\\d{9})+";

    @Override
    public String desensitize(String value) {
        List<String> all = ReUtil.findAll(REG, value, 0);
        for (String s : all) {
            value = value.replace(s, String.join("", s.substring(0, 3), REPLACE, s.substring(7)));
        }
        return value;
    }
}
