package com.yao2san.sim.framework.utils.json.serializer.desensitization;

import com.yao2san.sim.framework.utils.json.serializer.desensitization.handlers.MobileDesensitizationHandler;
import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DesensitizationHandlerSupport {
    private final static Map<Class<? extends DesensitizationHandler>, DesensitizationHandler> CUSTOM_HANDLERS = new ConcurrentHashMap<>();

    public static DesensitizationHandler registerHandler(Class<? extends DesensitizationHandler> clazz) {
        try {
            DesensitizationHandler instance = CUSTOM_HANDLERS.get(clazz);
            if (instance == null) {
                instance = clazz.newInstance();
                instance = CUSTOM_HANDLERS.put(clazz, instance);
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public enum DefaultHandler {
        MOBILE_PHONE(DesensitizationType.MOBILE_PHONE, new MobileDesensitizationHandler()),
        ;

        @Getter
        private final DesensitizationType type;
        @Getter
        private final DesensitizationHandler handler;

        DefaultHandler(DesensitizationType type, DesensitizationHandler handler) {
            this.type = type;
            this.handler = handler;
        }

        public static DesensitizationHandler ofHandler(DesensitizationType type) {
            for (DefaultHandler value : DefaultHandler.values()) {
                if (value.type == type) {
                    return value.handler;
                }
            }
            throw new IllegalArgumentException("Not supported handler type:" + type);
        }
    }
}
