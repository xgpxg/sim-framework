package com.yao2san.sim.framework.common.auth.enums;

public enum AuthType {
    /**
     * session认证
     */
    SESSION,
    /**
     * jwt认证
     */
    JWT,
    /**
     * AccessToken认证
     */
    AccessToken,
    ;
}