package com.yao2san.sim.framework.common.auth;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * 用户主体
 *
 * @author wxg
 */
@Data
public class UserPrincipal implements Serializable {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户ID
     */
    //private Long userId;
    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 用户类型：1用户
     */
    private Integer type;
    /**
     * 角色
     */
    private Set<String> roles;
    /**
     * 权限
     */
    private Set<String> permissions;
    /**
     * 用户扩展信息
     */
    private Map<String, Object> extra;
}
