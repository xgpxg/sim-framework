package com.yao2san.sim.framework.common.auth;

import lombok.Data;

/**
 * 用户权限
 *
 * @author wxg
 */
@Data
public class Permission {
    /**
     * 权限名称
     */
    private String purviewName;
    /**
     * 权限编码
     */
    private String purviewCode;
    /**
     * 权限绑定的url
     */
    private String resource;
}
