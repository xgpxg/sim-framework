package com.yao2san.sim.framework.common.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

@Aspect
@Component
public class InheritableRequestContextAspect {
    @Before(value = "@annotation(com.yao2san.sim.framework.common.annotation.InheritableRequestContext)")
    @SuppressWarnings("all")
    public void before(JoinPoint joinPoint) {
        RequestContextHolder.setRequestAttributes(RequestContextHolder.getRequestAttributes(), true);
    }
}
