package com.yao2san.sim.framework.common.auth.util;

import com.yao2san.sim.framework.common.auth.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;

@Slf4j
public class UserUtil {
    public static Long getCurrUserId() {
        return getCurrUser().getId();
    }

    public static UserPrincipal getCurrUser() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
        if (userPrincipal != null) {
            return userPrincipal;
        }
        return (UserPrincipal) SecurityUtils.getSubject().getPrincipal();
    }
}
